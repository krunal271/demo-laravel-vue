import Vue from 'vue'
import VueRouter from 'vue-router'
import router from './router.js'
import App from './views/App'
import store from './store'
import BootstrapVue from 'bootstrap-vue'
import Axios from 'axios';
import VeeValidate from 'vee-validate';
import moment from 'moment-timezone';

window.axios = require('axios');

Vue.use(VueRouter)
Vue.use(BootstrapVue)
Vue.use(VeeValidate, {
  events: ''
});
moment.tz.setDefault('Australia/Brisbane');
require('./bootstrap');

const app = new Vue({
    el: '#app',
    components: { App },
    router,
    store,
});

// if(window.location.protocol != 'https:') {
//   location.href = location.href.replace("http://", "https://");
//  }