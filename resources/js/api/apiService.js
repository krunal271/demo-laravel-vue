import api from './siteconfig'

export default { 
 
  getUserListPagination(data){
     return api.post(data.pageUrl,data);
  },
  getUserDetails(userData) {
    return api.post('user/getDetails',{'userId':userData})
  },
  getUserTypeSelectList() {
    return api.post('userType/getUserTypeSelectList')
  },
  getUserCategorySelectListByUserTypeId(userTypeId) {
    return api.post('userCategory/getUserCategorySelectListByUserTypeId',{'userTypeId': userTypeId})
  },
  getDepartmentSelectListByUserTypeId(userTypeId) {
    return api.post('department/getDepartmentSelectListByUserTypeId',{'userTypeId': userTypeId})
  },
  getHiringManagerSelectList() {
    return api.post('user/getHiringManagerSelectList')
  },
  getUserDetailsByUserId(userId) {
    return api.post('user/getUserDetailsByUserId',{'userId': userId})
  },
  createUser(userData) {
    return api.post('user/createUser',{'userData': userData})
  },
  editUser(userData) {
    return api.post('user/editUser',{'userData': userData})
  },
  deleteUserByUserId(userId) {
    return api.post('user/deleteUserByUserId',{'userId': userId})
  },
  getUserTypeList(){
    return api.post('/user/typeList');
  },
  deleteMultipleUserByUserId(userId) {
    return api.post('user/deleteMultipleUserByUserId',{'userId': userId})
  },
  updateMultipleUserStatusByUserId(userData) {
    return api.post('user/updateMultipleUserStatusByUserId',{userData})
  },
  inactiveMultipleUserByUserId(userId) {
    return api.post('user/inactiveMultipleUserByUserId',{'userId': userId})
  },
  /*User type services*/
  getUserTypes(pageUrl,noofRecord,searchData,sortData){
    return api.post(pageUrl,{'noofRecord':noofRecord,'searchData':searchData,'sortData': sortData});
  },
  addUserType(userTypeData){
    return api.post('/usertype/addUserType', userTypeData);
  },
  editUserType(userTypeData){
    return api.post('/usertype/editUserType', userTypeData);
  },
  getUserTypeById(userTypeId){
    return api.post('/usertype/getUserTypeById', {'userTypeId': userTypeId});
  },
  getSearchUserType(searchData){
    return api.post('/usertype/getSearchUserType', searchData);
  },
  deleteMultipleUserTypeById(userTypeId){
    return api.post('usertype/deleteMultipleUserTypeById',{'userTypeId': userTypeId});
  },
  updateMultipleUserTypeStatusById(userTypeId){
    return api.post('usertype/updateMultipleUserTypeStatusById',{'userTypeId': userTypeId});
  },
  removeUserType(id){
    return api.post('/usertype/removeUserType',{'userTypeId': id});
  },  
  /*User type services end*/

  /*Department services*/
  getDepartmentListPagination(pageUrl,noofRecord,searchData,sortData){
    return api.post(pageUrl,{'noofRecord':noofRecord,'searchData':searchData,'sortData': sortData});
  },
  addDepartment(departmentData){
    return api.post('department/addDepartment', departmentData);
  },
  deleteDepartmentById(departmentId){
    return api.post('department/deleteDepartment', {'departmentId': departmentId });
  },
  deleteMultipleDepartmentById(departmentId){
    return api.post('department/deleteMultipleDepartmentById', {'departmentId': departmentId });
  },
  getDepartmentById(departmentId){
    return api.post('department/getDepartmentById', {'departmentId': departmentId });
  },
  editDepartmentById(departmentData){
    return api.post('department/editDepartmentById', {'departmentData': departmentData });
  },
  updateMultipleDepartmentStatusByDepartmentId (departmentId){
    return api.post('department/updateMultipleDepartmentStatusByDepartmentId', {'departmentId': departmentId });
  },
  
  /*Department services end*/

  /* Module services */
  getModuleListPagination(pageUrl,noofRecord,searchData,sortData){
    return api.post(pageUrl,{'noofRecord':noofRecord,'searchData':searchData,'sortData': sortData});
  },
  addModule(moduleData){
    return api.post('module/addModule', moduleData);
  },
  deleteModuleById(moduleId){
    return api.post('module/deleteModule', {'moduleId': moduleId });
  },
  deleteMultipleModuleById(moduleId){
    return api.post('module/deleteMultipleModuleById', {'moduleId': moduleId });
  },
  getModuleById(moduleId){
    return api.post('module/getModuleById', {'moduleId': moduleId });
  },
  editModuleById(moduleData){
    return api.post('module/editModuleById', {'moduleData': moduleData });
  },
  updateMultipleModuleStatusByModuleId (moduleId){
    return api.post('module/updateMultipleModuleStatusByModuleId', {'moduleId': moduleId });
  },
  /* Modules services end */

  /* UserCategory services */
  getUserCategoryListPagination(pageUrl,data){
    return api.post(pageUrl,data);
  },
  addUserCategory(userCategoryData){
    return api.post('usercategory/addUserCategory', userCategoryData);
  },
  deleteUserCategoryById(userCategoryId){
    return api.post('usercategory/deleteUserCategory', {'userCategoryId': userCategoryId });
  },
  deleteMultipleUserCategoryById(userCategoryId){
    return api.post('usercategory/deleteMultipleUserCategoryById', {'userCategoryId': userCategoryId });
  },
  getUserCategoryById(userCategoryId){
    return api.post('usercategory/getUserCategoryById', {'userCategoryId': userCategoryId });
  },
  editUserCategoryById(userCategoryData){
    return api.post('usercategory/editUserCategoryById', {'userCategoryData': userCategoryData });
  },
  updateMultipleUserCategoryStatusByUserCategoryId(userCategoryData){
    return api.post('usercategory/updateMultipleUserCategoryStatusByUserCategoryId', {'userCategoryId': userCategoryData });
  },
  
  /* UserCategory services end */

  /* Template services */
  getTemplateListPagination(pageUrl,noofRecord,searchData){
    return api.post(pageUrl,{'noofRecord':noofRecord,'searchData':searchData});
  },
  addTemplate(templateData){
    return api.post('template/addTemplate', templateData);
  },
  deleteTemplateById(templateId){
    return api.post('template/deleteTemplate', {'templateId': templateId });
  },
  deleteMultipleTemplateById(templateId){
    return api.post('template/deleteMultipleTemplateById', {'templateId': templateId });
  },
  getTemplateById(templateId){
    return api.post('template/getTemplateById', {'templateId': templateId });
  },
  editTemplateById(templateData){
    return api.post('template/editTemplateById', {'templateData': templateData });
  },
  updateMultipleTemplateStatusByTemplateId (templateId){
    return api.post('template/updateMultipleTemplateStatusByTemplateId', {'templateId': templateId });
  },
  /* Template services end*/

  /* Admin User services */
  getAdminListPagination(pageUrl,data){
    return api.post(pageUrl,data);
  },
  addAdmin(adminData){
    return api.post('admin/addAdmin', adminData);
  },
  deleteAdminById(adminId){
    return api.post('admin/deleteAdmin', {'adminId': adminId });
  },
  deleteMultipleAdminById(adminId){
    return api.post('admin/deleteMultipleAdminById', {'adminId': adminId });
  },
  getAdminById(adminId){
    return api.post('admin/getAdminById', {'adminId': adminId });
  },
  editAdminById(adminData){
    return api.post('admin/editAdminById', {'adminData': adminData });
  },
  updateMultipleAdminStatusByAdminId (adminId){
    return api.post('admin/updateMultipleAdminStatusByAdminId', {'adminId': adminId });
  },
  /* Admin services end*/

  /*getBookingListPagination(noOfPage,searchData)
  {
      return api.post('booking/getBookingListPagination',{'noOfPage':noOfPage,'searchData':searchData});
  },    */
  getBookingListPagination(data)
  {
      return api.post(data.pageUrl,data);
  },
  getParticipantSelectList(data)
  {
      return api.post('user/getParticipantSelectList', {'data': data});
  },
  createBooking(data)
  {
      return api.post('booking/createBooking',data);
  },
  sendResetLinkInMail(requestData) {  
    return api.post('password/reset/link',{'email':requestData})
  },
  sendFrontResetLinkInMail(requestData) {  
    return api.post('front/password/sendreset/link',{'email':requestData})
  },
  
  checkPasswordReset(token,email) {  
    return api.post('admin/password/reset/'+token,{'email':email})
  },
    checkFrontPasswordReset(token,email) {  
    return api.post('front/password/reset/'+token,{'email':email})
  },
  editPasswordReset(data) {  
    return api.post('admin/password/update',{'data':data})
  },
  editFrontPasswordReset(data) {  
    return api.post('front/password/update',{'data':data})
  },
  deleteBookingByBookingId(data)
  {
      return api.post('booking/deleteBookingByBookingId',data);
  },
  getBookingDetailsByBookingId(data)
  {
      return api.post('booking/getBookingDetailsByBookingId',data);
  },
  addConfirmAttendance(data)
  {
      return api.post('booking/addConfirmAttendance',data);
  },
  changeBookingAttend(data)
  {
      return api.post('booking/changeBookingAttend',data);
  },

  getFrontUserDetailsByUserId(data) {
    return api.post('user/getFrontUserDetailsByUserId',data);
  },
  participantPersonalDetails(data){
      return api.post('user/participantPersonalDetails',data);
  },
  getDocumentsListByparticipantId(data)
  {
      return api.post(data.pageUrl,data);
  },
  addDocumentsUpload(data)
  {
      return api.post('documents/addDocumentsUpload',data,{headers: {
       'Content-Type': 'multipart/form-data'
      }
   });
  },
  multipleDocumentUpload(data)
  {
    return api.post('documents/upload-documents',data,{headers: {
        'Content-Type': 'multipart/form-data'
       }
    });
  },
  deleteDocument(userDocumentId)
  {
    return api.post('document/remove',{'userDocumentId':userDocumentId});
  },
  updateExpiryDate(expiryDocumentId,expiryDocumentDate)
  {
    return api.post('document/update',{'expiryDocumentId':expiryDocumentId,'expiryDocumentDate':expiryDocumentDate});
  },
  adminUpdateExpiryDate(expiryDocumentId,expiryDocumentDate)
  {
    return api.post('document/admin-update-document',{'expiryDocumentId':expiryDocumentId,'expiryDocumentDate':expiryDocumentDate});
  },
  changeExpiryDateStatus(docStatus,expiryDocumentId,participnatId)
  {
    return api.post('document/update-status',{'docStatus':docStatus,'expiryDocumentId':expiryDocumentId,'participantId':participnatId});
  },
  createNewParticipant(data){

    return api.post('participant/create',{'data':data});

  },
  userListByType(data){
 
    return api.post('user/list',{'data':data});    
  },
  userCategoryListByType(data){
 
    return api.post('userCategory/list',{'data':data});    
  },
  userDepartmentListByType(data){

    return api.post('userDepartment/list',{'data':data});    
  },
  participantListWithPagination(url,data){

    return api.post(url,{'data':data});
  },
  getDocumentsDetailsByDocumentId(data)
  {
    return api.post('documents/getDocumentsDetailsByDocumentId',data);
  },
  
  /* Front: elearning module services*/
  // [get active elearning modules]
  getFrontModuleListPagination(pageUrl,noofRecord,searchData)
  {
    return api.post(pageUrl,{'noofRecord':noofRecord,'searchData':searchData});
  },
  // [start elearning module by id]
  startModule(moduleId)
  {
    return api.post('/elearning/startModule', {'moduleId': moduleId});
  },
  checkAttempt(moduleId)
  {
    return api.post('/elearning/check-attempt', {'moduleId': moduleId});
  },
  /* elearning module services end */
  
  changeConfirmPolicy(data)
  {
      return api.post('user/changeConfirmPolicy',data);
  },
  getPolicyDetailByUserId(data)
  {
      return api.post('user/getPolicyDetailByUserId',data);
  },
  checkConfirmAccountRegistrationKey(data)
  {
      return api.post('user/checkConfirmAccountRegistrationKey',data);
  },
  getDetailsOfConfirmAccount(data)
  {
      return api.post('user/getDetailsOfConfirmAccount',data);
  },
  addConfirmAccount(data)
  {
      return api.post('user/addConfirmAccount',data);
  },
  editAdminPasswordReset(data) {  
    return api.post('admin/password/update',{'data':data})
  },
  deleteParticipant(data){

    return api.post('participant/delete',{'data':data});

  },
  uploadVisaAttachments(data)
  {
      return api.post('user/uploadVisaAttachments',data,{headers: {
       'Content-Type': 'multipart/form-data'
      }
   })
  },
  removeVisaAttachments(data)
  {
        return api.post('user/removeVisaAttachments',data);
  },
  
  changeExpiryDate(data)
  {
      return api.post('documents/changeExpiryDate',data);
  },
  checkExistEmailId(data){

     return api.post('user/alreadyExistEmail',{'data':data});
  },
  inviteParticipant(data)
  {
       return api.post('participant/inviteParticipant',data);
  },
  searchQueryDataWithPagination(url,data){
    return api.post(url,{'data':data});
  },
  //get interview quetion for interview and referances
  getQuestionList()
  {
     return api.post('interviewAndReferance/getQuestionList');
  },
  getDashboardData(url) {
    return api.post(url);
  },
  getDashboardModule(url,sortData){
    return api.post(url, {'data': sortData});
  },
  getParticipantModule(url,sortData){
    return api.post(url, {'data': sortData});
  },
  getTypeName(data){
    return api.post('user/get-type-name',{'data':data});
  },
  //get notification by user
  getNotificationByUser(userData){
    return api.post('notification/get-notification-list',{'data':userData});
  },
  addInterviewAndReferance(data)
  {
       return api.post('interviewAndReferance/addInterviewAndReferance',data);
  },
  getParticipnatDocumentData(data){
    return api.post('documents/list',{'data':data});
  },
  getDocumentTypeList(){
    return api.post('documents-type/list');
  },
  updateDocumentstatus(data){
    return api.post('documents/update-status',{'data':data});
  },
  getParticipnatDataById(data){
     return api.post('participant/get-data',{'data':data});
  },
  updateParticipantDataById(data){
     return api.post('participant/update',{'data':data});
  },
  getParticipantModuleList(participant_id){
    return api.post('elearning/participant-module-list',{'participant_id':participant_id});
  },
  geteLearningStatus(participant_id){
    return api.post('elearning/elearning-status',{'participant_id':participant_id});
  },
  getInterviewAndReferenceDetailByUserId(data)
  {
      return api.post('interviewAndReferance/getInterviewAndReferenceDetailByUserId',data);
  },
  getAnswerListByInterviewAndReferenceId(data)
  {
      return api.post('interviewAndReferance/getAnswerListByInterviewAndReferenceId',data);
  },
  getDocumentInfoByUserId(data)
  {
      return api.post('documents/getDocumentInfoByUserId',data);
  },
  getParticipantBookingDetailByUserId(data)
  {
      return api.post('booking/getParticipantBookingDetailByUserId',data);
  },
  getELearningStatusByParticipantId(data)
  {
      return api.post('elearning/getELearningStatusByParticipantId',data);
  },
  getParticipantDocumentStatusByTypeSlug(data){
    return api.post('documents/get-document-status',{'data':data});
  },
  getInterviewAndReferencestatus(data){
    return api.post('interview-and-referance/status',{'data':data});
  },
  //for notification
  getNotificationListByPagination(userData)
  {
      return api.post(userData.pageUrl,{'data':userData});
  },  
  setEngagementStatus(data){
    return api.post('participant/engagement-status',{'data':data});
  },
  sendeLearningInvition(participantId){
    return api.post('participant/elearning-invitation',{'participantId':participantId});
  },
  readNotificationByUser(notificationId,userType){
    return api.post('notification/read-notification',{'notificationId':notificationId, 'userType': userType}); 
  },
  getUserName(data){
    return api.post('user/get-name',{'data':data});    
  },
  getChildSafeGaurdingRating(data){
    return api.post('user/get-rating',{'data':data});    
  },
  confirmStatutoryDeclaration(data){
    return api.post('participant/confirm-statutory-declaration',{data});    
  },
  getStatutoryDeclaration(participantId){
    return api.post('participant/get-statutory-declaration',{'participantId': participantId});    
  },
  getLearningContents(page, keyword, tags, languages, providers, contents)
  {
    return api.post('learning/learning-contents', {'page': page,'keyword': keyword, 'tags': tags, 'languages': languages, 'providers': providers, 'contents': contents});    
  },
  getProviders()
  {
    return api.post('learning/providers');    
  },
  addToDefaultCollection(loId)
  {
    return api.post('learning/add-default-collection', {'loId': loId});    
  },
  getUserEnrollments(page)
  {
    return api.post('learning/user-enrollments', {'page': page});    
  },
  getLogin()
  {
    return api.post('learning/get-user-in');    
  },    
  downloadPDF(data){
    return api.post('participant/statutory-declaration-download',{'data': data});    
  },
  getInTouch(data)
  {
    return api.post('home/get-in-touch',{'data': data});    
  }
}
