import Vue from 'vue'
import VueRouter from 'vue-router'
import AuthService from './services/auth'
import store from './store'
import axios from 'axios'
import Ls from './services/ls'


//layouts s
//import LayoutBack from './views/layouts/LayoutFront.vue'

/*
 |--------------------------------------------------------------------------
 | Admin Views
 |--------------------------------------------------------------------------|
 */


import Edashboard from './views/Admin/edashboard'
import UserList from './views/Admin/pages/users/userList'
import UserAdd from './views/Admin/pages/users/userAdd'
import UserEdit from './views/Admin/pages/users/userEdit'
import ParticipantsOverview from './views/Admin/pages/participants/participantsOverview'

import UserTypeList from './views/Admin/pages/userTypeList'
import UserTypeAdd from './views/Admin/pages/userTypeAdd'
import UserTypeEdit from './views/Admin/pages/userTypeEdit'

import DepartmentList from './views/Admin/pages/departmentList'
import DepartmentAdd from './views/Admin/pages/departmentAdd'
import DepartmentEdit from './views/Admin/pages/departmentEdit' 

import ModuleList from './views/Admin/pages/moduleList'
import ModuleAdd from './views/Admin/pages/moduleAdd'
import ModuleEdit from './views/Admin/pages/moduleEdit'

import UserCategoryList from './views/Admin/pages/userCategoryList'
import UserCategoryAdd from './views/Admin/pages/userCategoryAdd'
import UserCategoryEdit from './views/Admin/pages/userCategoryEdit'

import TemplateList from './views/Admin/pages/templateList'
import TemplateAdd from './views/Admin/pages/templateAdd'
import TemplateEdit from './views/Admin/pages/templateEdit'

import AdminList from './views/Admin/pages/adminList'
import AdminAdd from './views/Admin/pages/adminAdd'
import AdminEdit from './views/Admin/pages/adminEdit'
import adminResetPassword from './views/Admin/pages/reset_password'
import adminChangePassword from './views/Admin/pages/changePassword'

import contentLibrary from './views/Admin/pages/contentLibrary'
/*
 |--------------------------------------------------------------------------
 | Frontend Views
 |--------------------------------------------------------------------------|
 */


import adminLogin from './views/Admin/pages/login'

import login from './views/Front/ParticipantLogin'

const router = new VueRouter({
    mode: 'history',
    linkActiveClass: 'active',
    routes: [

    {
        path: '/admin-login',
        component: adminLogin,
        meta: {
            title: 'Login',
        },
        name: 'adminLogin'

    },
    {
        path: '/admin-forgot-password',
        component: resolve => require(['./views/Admin/pages/forgot_password.vue'], resolve),
        meta: {
            title: 'Forgot Password',
            backProcessCheck : false,
            breadcrumb: ``
        },
        name: 'forgot_password'
    },
    {
        path: '/admin-reset-password/:token/:email',
        component: resolve => require(['./views/Admin/pages/reset_password.vue'], resolve),
        meta: {
            title: 'Reset Password',
            backProcessCheck : false,
            breadcrumb: ``
        },
        name: 'reset_password'
    },
    {
        path: '/admin', 
        component: resolve => require(['./views/Admin/default.vue'], resolve),
        children: [
            {
                path: 'edashboard',
                component: Edashboard,
                meta: {
                    requiresAdminAuth: true, 
                    title: 'Dashboard',
                    backProcessCheck : false,
                    helpTextAdmin:'',
                    breadcrumb: [{
                        text: ' Dashboard',
                        href: '/admin',
                    }]
                },
                name: 'edashboard'

            },
            {
                 path: '/',
                 component: adminLogin,
                 meta: {
                     title: 'Login',
                     backProcessCheck : false,
                 },
                 name: 'adminInnerLogin'
 
             },
            {

                path: 'user_list',
                component: UserList,
                meta: {
                    requiresAdminAuth: true, 
                    title: 'User List',
                    backProcessCheck : false,
                    title: 'User',
                    activeAdminClass:'active',
                    helpTextAdmin:'The User List is a record of every user that has ever existed on the DEEP platform.',
                    breadcrumb: [{
                        text: ' User List',
                        href: '/user_list',
                    }]
                },
                name: 'user_list'
            },
            {
                path: 'user_add',
                component: UserAdd,
                meta: {
                   requiresAdminAuth: true, 
                    backProcessCheck : true,
                    title: 'User',
                    activeAdminClass:'active',
                    helpTextAdmin:'The User List is a record of every user that has ever existed on the DEEP platform.',
                    breadcrumb: [{
                        text: ' User ADD',
                        href: '/user_add',
                    }]
                },
                name: 'user_add'

            },
            {
                path: 'user_edit/:id/:page/:perPage',
                component: UserEdit,
                meta: {
                    requiresAdminAuth: true, 
                    backProcessCheck : true,
                    title: 'User',
                    helpTextAdmin:'The User List is a record of every user that has ever existed on the DEEP platform.',
                    breadcrumb: [{
                        text: ' User Update',
                        href: '/user_edit',
                    }]
                },
                name: 'user_edit'
                

            },

             {
                path: 'paricipants_overview',
                component: ParticipantsOverview,
                meta: {
                    title: 'Participants Overview',
                    backProcessCheck : false,
                    helpTextAdmin:'',
                    breadcrumb: [{
                        text: 'Participants Overview',
                        href: '/paricipants_overview',
                    }]
                },
                name: 'paricipants_overview'

            },
            {

                path: 'user_type_list',
                component: UserTypeList,
                meta: {
                    requiresAdminAuth: true, 
                    title: 'User Type',
                    backProcessCheck : false,
                    helpTextAdmin:'User Type defines a user’s permissions within the DEEP platform.',
                    breadcrumb: [{
                        text: ' User Types',
                        href: '/user_type_list',
                    }]
                },
                name: 'user_type_list'
            },
            {

                path: 'user_type_add',
                component: UserTypeAdd,
                meta: {
                    requiresAdminAuth: true, 
                    backProcessCheck : true,
                    title: 'User Type',
                    helpTextAdmin:'User Type defines a user’s permissions within the DEEP platform.',
                    breadcrumb: [{
                        text: 'Add User Type',
                        href: '/add_user_type',
                    }]
                },
                name: 'user_type_add'
            },
            {

                path: 'user_type_edit/:id',
                component: UserTypeEdit,
                meta: {
                    requiresAdminAuth: true, 
                    backProcessCheck : true,
                    title: 'User Type',
                    helpTextAdmin:'User Type defines a user’s permissions within the DEEP platform.',
                    breadcrumb: [{
                        text: 'Edit User Type',
                        href: '/user_type_edit',
                    }]
                },
                name: 'user_type_edit'
            },
            {
                path: 'department_list',
                component: DepartmentList,
                meta: {
                    requiresAdminAuth: true, 
                    title: 'Department',
                    backProcessCheck : false,
                    helpTextAdmin:'Department defines the section of the organisation the user is from.',
                    breadcrumb: [{
                        text: 'Departments',
                        href: '/department_list',
                    }]
                },
                name: 'department_list'
            },
            {
                path: 'department_add',
                component: DepartmentAdd,
                meta: {
                    requiresAdminAuth: true, 
                    title: 'Department',
                    backProcessCheck : true,
                    helpTextAdmin:'Department defines the section of the organisation the user is from.',
                    breadcrumb: [{
                        text: 'Add Department',
                        href: '/department_add',
                    }]
                },
                name: 'department_add'
            },
            {
                path: 'department_edit/:id',
                component: DepartmentEdit,
                meta: {
                    requiresAdminAuth: true, 
                    title: 'Department',
                    backProcessCheck : true,
                    helpTextAdmin:'Department defines the section of the organisation the user is from.',
                    breadcrumb: [{
                        text: 'Edit Department',
                        href: '/department_edit',
                    }]
                },
                name: 'department_edit'
            },
            {
                path: 'module_list',
                component: ModuleList,
                meta: {
                    requiresAdminAuth: true, 
                    title: 'Module',
                    backProcessCheck : false,
                    helpTextAdmin:'Modules are the programs that participants must successfully complete to meet the child safeguarding requirements. ',
                    breadcrumb: [{
                        text: 'Modules',
                        href: '/module_list',
                    }]
                },
                name: 'module_list'
            },
            {
                path: 'module_add',
                component: ModuleAdd,
                meta: {
                    requiresAdminAuth: true, 
                    backProcessCheck : true,
                    title: 'Module',
                    helpTextAdmin:'Modules are the programs that participants must successfully complete to meet the child safeguarding requirements. ',
                    breadcrumb: [{
                        text: 'Add Module',
                        href: '/module_add',
                    }]
                },
                name: 'module_add'
            },
            {
                path: 'module_edit/:id',
                component: ModuleEdit,
                meta: {
                    requiresAdminAuth: true, 
                    title: 'Module',
                    backProcessCheck : true,
                    helpTextAdmin:'Modules are the programs that participants must successfully complete to meet the child safeguarding requirements. ',
                    breadcrumb: [{
                        text: 'Edit Module',
                        href: '/module_edit',
                    }]
                },
                name: 'module_edit'
            },
            {
                path: 'user_category_list',
                component: UserCategoryList,
                meta: {
                    requiresAdminAuth: true, 
                    title: 'User Category',
                    backProcessCheck : false,
                    helpTextAdmin:'User Categories define the type of user a participant is. They are created for each User based on the type of contact the user has with the organisation. For example, Employee (casual), Volunteer, or Contractor.',
                    breadcrumb: [{
                        text: 'User Category',
                        href: '/user_category_list',
                    }]
                },
                name: 'user_category_list'
            },
            {
                path: 'user_category_add',
                component: UserCategoryAdd,
                meta: {
                    requiresAdminAuth: true, 
                    title: 'User Category',
                    backProcessCheck : true,
                    helpTextAdmin:'User Categories define the type of user a participant is. They are created for each User based on the type of contact the user has with the organisation. For example, Employee (casual), Volunteer, or Contractor.',
                    breadcrumb: [{
                        text: 'Add User Category',
                        href: '/user_category_add',
                    }]
                },
                name: 'user_category_add'
            },
            {
                path: 'user_category_edit/:id',
                component: UserCategoryEdit,
                meta: {
                    requiresAdminAuth: true, 
                    title: 'User Category',
                    backProcessCheck : true,
                    helpTextAdmin:'User Categories define the type of user a participant is. They are created for each User based on the type of contact the user has with the organisation. For example, Employee (casual), Volunteer, or Contractor.',
                    breadcrumb: [{
                        text: 'Edit User Category',
                        href: '/user_category_edit',
                    }]
                },
                name: 'user_category_edit'
            },
            {
                path: 'template_list',
                component: TemplateList,
                meta: {
                    requiresAdminAuth: true, 
                    title: 'Template',
                    backProcessCheck : false,
                    helpTextAdmin:'',
                    breadcrumb: [{
                        text: 'Template',
                        href: '/template_list',
                    }]
                },
                name: 'template_list'
            },
            {
                path: 'template_add',
                component: TemplateAdd,
                meta: {
                    requiresAdminAuth: true, 
                    title: 'Template',
                    backProcessCheck : true,
                    helpTextAdmin:'',
                    breadcrumb: [{
                        text: 'Add Template',
                        href: '/template_add',
                    }]
                },
                name: 'template_add'
            },
            {
                path: 'template_edit/:id',
                component: TemplateEdit,
                meta: {
                    requiresAdminAuth: true, 
                    title: 'Template',
                    backProcessCheck : true,
                    helpTextAdmin:'',
                    breadcrumb: [{
                        text: 'Edit Template',
                        href: '/template_edit',
                    }]
                },
                name: 'template_edit'
            },
            {
                path: 'admin_list',
                component: AdminList,
                meta: {
                    requiresAdminAuth: true, 
                    title: 'Admin',
                    backProcessCheck : false,
                    helpTextAdmin:'Admin users have the highest level of access within DEEP. Admin users can verify the documents of participants.',
                    breadcrumb: [{
                        text: 'Admin User',
                        href: '/admin_list',
                    }]
                },
                name: 'admin_list'
            },
            {
                path: 'admin_add',
                component: AdminAdd,
                meta: {
                    requiresAdminAuth: true, 
                    title: 'Admin',
                    backProcessCheck : true,
                    helpTextAdmin:'Admin users have the highest level of access within DEEP. Admin users can verify the documents of participants.',
                    breadcrumb: [{
                        text: 'Add Admin User',
                        href: '/admin_add',
                    }]
                },
                name: 'admin_add'
            },
            {
                path: 'admin_edit/:id',
                component: AdminEdit,
                meta: {
                    requiresAdminAuth: true, 
                    title: 'Admin',
                    backProcessCheck : true,
                    helpTextAdmin:'Admin users have the highest level of access within DEEP. Admin users can verify the documents of participants.',
                    breadcrumb: [{
                        text: 'Edit Admin User',
                        href: '/admin_edit',
                    }]
                },
                name: 'admin_edit'
            },
            {
                path: 'learning-contents',
                component: contentLibrary,
                meta: {
                    requiresAdminAuth: true, 
                    backProcessCheck : false,
                    title: 'Content library',
                    breadcrumb: [{
                        text: 'Content library',
                        href: '/learning-contents',
                    }]
                },
                name: 'learningContents'
            },
            {
                path: 'change-password', 
                component : adminChangePassword,
                meta: {
                    requiresAdminAuth: true, 
                    title: 'Change Password',
                    backProcessCheck : false,
                    helpTextAdmin:'',
                    breadcrumb: [{
                        text: 'Change Password',
                        href: '/change-password',
                    }]
                },
                name: 'admin_change_password'
            },
            //Admin default route
            {
                path: '*', 
                component : adminLogin,
                meta: {
                    title: 'Login',
                }
                
            }


            ],

    },

      // DEFAULT ROUTE
    {
        path: '*', 
        component : login,
        meta: {
            title: 'Login',
            backProcessCheck : false,
        }
        
    },
    
    {
        path: '/admin/resetpassword/:token', 
        component: adminResetPassword, 
        meta: {
            title: 'Reset Password',
            backProcessCheck : false,
            breadcrumb: [{
                text: 'Deep Community',
                //href: '/childSafeguarding',
            }]
        }
    },



    ],
});

window.popStateDetected = false;
window.addEventListener('popstate', () => {
  window.popStateDetected = true;
});



router.beforeEach((to, from, next) => {

    const IsItABackButton = window.popStateDetected;
    if (/*IsItABackButton ||*/ from.matched.some(m => m.meta.backProcessCheck)) {
        const endProgress = confirm("Your data will be lost if not saved. Are you sure you want to go back?");
        if (endProgress) {
            return next()
        }else{
            return next(false);
            //return '';
        } 
    }
    // If the next route is requires user to be Logged IN
    if (to.matched.some(m => m.meta.requiresFrontAuth)){
        let token = Ls.get('auth.front.token');
        let role = Number(Ls.get('role'));
        axios.defaults.headers.common['Authorization'] = `Bearer ${token}`;
        axios.defaults.headers.common['Cache-Control'] = 'no-cache';
        axios.defaults.headers.common['Pragma'] = 'no-cache';
        axios.defaults.headers.common['Expires'] = 'Sat, 01 Jan 2000 00:00:00 GMT';
        
        return AuthService.check().then(authenticated => {
            if(!authenticated.status){
                toastr.error('Please login to access this page', '', {timeOut: 5000});

                return next({ path : '/front/login'})

            }
            store.dispatch("SetUserData", authenticated.userData);
            
            
            if (role != 0) {
                store.dispatch("SetUserType", role);
            }

            store.commit("routeChange", "start");
            store.commit('setBackImage',to.meta.backImage);
            store.commit('setHomePage',to.name);
            store.commit('setStrategyPage',to.name);
            return next()
        })
    }
    if (to.matched.some(m => m.meta.requiresAdminAuth)){
        // axios.defaults.headers.common['uType'] = 'admin';
        let token = Ls.get('auth.back.token');

        axios.defaults.headers.common['Authorization'] = `Bearer ${token}`;
        axios.defaults.headers.common['Cache-Control'] = 'no-cache';
        axios.defaults.headers.common['Pragma'] = 'no-cache';
        axios.defaults.headers.common['Expires'] = 'Sat, 01 Jan 2000 00:00:00 GMT';

         return AuthService.adminCheck().then(authenticated => {
            if(!authenticated.status){
                toastr.error('Please login to access this page', '', {timeOut: 5000});

                return next({ path : '/admin-login'})
            }
            store.dispatch("SetUserData", authenticated.userData);

            store.commit("routeChange", "start");
            return next()
        })
    }
   
    

 

    return next()
});
router.afterEach((to, from) => {
    document.title = to.meta.title;
    store.commit("routeChange", "end");
   
    if (window.innerWidth < 992) {
        store.commit('left_menu', "close");
    } else {
        store.commit('left_menu', "open");
    }
})

export default router