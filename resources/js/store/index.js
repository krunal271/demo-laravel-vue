import Vue from 'vue'
import Vuex from 'vuex'

import * as actions from './actions'
import * as getters from './getters'
import * as types from './mutation-types'
import User from './modules/users'
import Booking from './modules/booking'
import participant from './modules/participant'



Vue.use(Vuex)

const debug = process.env.NODE_ENV !== 'production'

const state = {
  	isAdmin:'',
	left_open: true,
    right_open: false,
    back_Image:'',
    home_page:'',
    strategy_page:'',
	preloader: true,
	user: {
	    name: "Addison",
	    picture: "images/avatar1.jpg",
	    job: "Project Manager"
	}
    
}

// const vuexLocalStorage = new VuexPersist({
//   key: 'vuex', // The key to store the state on in the storage provider.
//   storage: window.localStorage, // or window.sessionStorage or localForage
//   // Function that passes the state and returns the state with only the objects you want to store.
//   // reducer: state => state,
//   // Function that passes a mutation and lets you decide if it should update the state in localStorage.
//   // filter: mutation => (true)
// })


const mutations = {
	 left_menu(state, option) {
        if (option == "open") {
            state.left_open = true
        } else if (option == "close") {
            state.left_open = false
        } else if (option == "toggle") {
            state.left_open = !state.left_open
        }
        if (state.left_open) {
            document.getElementsByTagName("body")[0].classList.remove("left-hidden")
        } else {
            document.getElementsByTagName("body")[0].classList.add("left-hidden")
        }
    },
    rightside_bar(state, option) {
        if (option == "open") {
            state.right_open = true
        } else if (option == "close") {
            state.right_open = false
        } else if (option == "toggle") {
            state.right_open = !state.right_open
        }
        if (state.right_open) {
            document.getElementsByTagName("body")[0].classList.add("sidebar-right-opened")
        } else {
            document.getElementsByTagName("body")[0].classList.remove("sidebar-right-opened")
        }
    },
    routeChange(state, loader) {
        if (loader == "start") {
            state.preloader = true
        } else if (loader == "end") {
            state.preloader = false
        }
    },
    setBackImage(state, image) {
        if (image != "") {
            state.back_Image = image
        } else {
            state.back_Image = ''
        }
    },
    setHomePage(state, name){
        if (name != "") {
            state.home_page = name
        } else {
            state.home_page = ''
        }
    },
    setStrategyPage(state, name){
        if (name != "") {
            state.strategy_page = name
        } else {
            state.strategy_page = ''
        }
    }
}
export default new Vuex.Store({
  state,
  actions,
  mutations,
  getters,
  modules: {
    User,
    Booking,
    participant
  },
  strict: debug
})
