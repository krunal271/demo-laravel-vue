let mutations = {
    // left_menu(state, option) {
    //     if (option == "open") {
    //         state.left_open = true
    //     } else if (option == "close") {
    //         state.left_open = false
    //     } else if (option == "toggle") {
    //         state.left_open = !state.left_open
    //     }
    //     if (state.left_open) {
    //         document.getElementsByTagName("body")[0].classList.remove("left-hidden")
    //     } else {
    //         document.getElementsByTagName("body")[0].classList.add("left-hidden")
    //     }
    // },
    // rightside_bar(state, option) {
    //     if (option == "open") {
    //         state.right_open = true
    //     } else if (option == "close") {
    //         state.right_open = false
    //     } else if (option == "toggle") {
    //         state.right_open = !state.right_open
    //     }
    //     if (state.right_open) {
    //         document.getElementsByTagName("body")[0].classList.add("sidebar-right-opened")
    //     } else {
    //         document.getElementsByTagName("body")[0].classList.remove("sidebar-right-opened")
    //     }
    // },
    // routeChange(state, loader) {
    //     if (loader == "start") {
    //         state.preloader = true
    //     } else if (loader == "end") {
    //         state.preloader = false
    //     }
    // }
}
export default mutations
export const SET_USER_ID = 'SET_USER_ID'
export const SET_USER_PAGE = 'SET_USER_PAGE'
export const SET_USER_PAGE_NO = 'SET_USER_PAGE_NO'
export const SET_USER_PER_PAGE = 'SET_USER_PER_PAGE'
export const SET_USER_SEARCH_DATA = 'SET_USER_SEARCH_DATA'
export const SET_USER_DATA = 'SET_USER_DATA'
export const SET_BOOKING_ID = 'SET_BOOKING_ID'
export const SET_BOOKING_PAGE = 'SET_BOOKING_PAGE'
export const SET_BOOKING_PAGE_NO = 'SET_BOOKING_PAGE_NO'
export const SET_BOOKING_PER_PAGE = 'SET_BOOKING_PER_PAGE'
export const SET_SELECTED_PARTICIPANT = 'SET_SELECTED_PARTICIPANT'
export const SET_BOOKING_SEARCH_DATA = 'SET_BOOKING_SEARCH_DATA'
export const SET_PARTICIPANT_ID = 'SET_PARTICIPANT_ID'
export const SET_PARTICIPANT_SEARCH_NAME = 'SET_PARTICIPANT_SEARCH_NAME'
export const SET_PARTICIPANT_SEARCH_EMAIL = 'SET_PARTICIPANT_SEARCH_EMAIL'
export const SET_PARTICIPANT_PAGE_NO = 'SET_PARTICIPANT_PAGE_NO'
export const SET_PARTICIPANT_PER_PAGE = 'SET_PARTICIPANT_PER_PAGE'
export const SET_PARTICIPANT_SEARCH_DATA = 'SET_PARTICIPANT_SEARCH_DATA'
export const SET_USER_SORTED_COLUMN = 'SET_USER_SORTED_COLUMN'
export const SET_USER_ORDER = 'SET_USER_ORDER'
export const SET_ROLE_TYPE = 'SET_ROLE_TYPE'
export const SET_USER_TYPE = 'SET_USER_TYPE'
