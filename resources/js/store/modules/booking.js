import * as types from '../mutation-types'
import _ from 'lodash'
import booking from '../../api/apiService'
import moment from 'moment'

// initial state
const state = {
	'bookingId':0,
  'bookingPage':'',
  'bookingPageNo':0,
  'bookingPerPage':0,
  'selectedParticipnat' : 0,
  'bookingSearchData':{},
}


// actions
const actions = {
  SetBookingId ({commit},bookingId) {
    commit(types.SET_BOOKING_ID, bookingId)
  },
  SetBookingPage ({commit},bookingPage) {
    commit(types.SET_BOOKING_PAGE, bookingPage)
  },
  SetBookingPageNo ({commit},bookingPageNo) {
    commit(types.SET_BOOKING_PAGE_NO, bookingPageNo)
  },
  SetBookingPerPage ({commit},bookingPerPage) {
    commit(types.SET_BOOKING_PER_PAGE, bookingPerPage)
  },
  SetSelectedParticipnat ({commit},selectedParticipnat) {
    commit(types.SET_SELECTED_PARTICIPANT, selectedParticipnat)
  },
  SetBookingSearchData ({commit},bookingSearchData) {
    commit(types.SET_BOOKING_SEARCH_DATA, bookingSearchData)
  },
  
 }
// mutations
const mutations = {
	[types.SET_BOOKING_ID] (state, bookingId) {
    state.bookingId = bookingId
  },
  [types.SET_BOOKING_PAGE] (state, bookingPage) {
    state.bookingPage = bookingPage
  },
  [types.SET_BOOKING_PAGE_NO] (state, bookingPageNo) {
    state.bookingPageNo = bookingPageNo
  },
  [types.SET_BOOKING_PER_PAGE] (state, bookingPerPage) {
    state.bookingPerPage = bookingPerPage
  },
  [types.SET_SELECTED_PARTICIPANT] (state, selectedParticipnat) {
    state.selectedParticipnat = selectedParticipnat
  },
  [types.SET_BOOKING_SEARCH_DATA] (state, bookingSearchData) {
    state.bookingSearchData = bookingSearchData
  },
  
}

export default {
  state,
  // getters,
  actions,
  mutations
}