import * as types from '../mutation-types'
import _ from 'lodash'
import booking from '../../api/apiService'
import moment from 'moment'

// initial state
const state = {
	'participnatId':0,
  'participantSearchName':'',
  'participantSearchEmail':'',
  'participantPageNo':0,
  'participantPerPage':0,
  'participantSearchData':{},
}


// actions
const actions = {
  SetParticipnatId ({commit},participnatId) {
    commit(types.SET_PARTICIPANT_ID, participnatId)
  },
  SetParticipantSearchName ({commit},participantSearchName) {
    commit(types.SET_PARTICIPANT_SEARCH_NAME, participantSearchName)
  },
  SetParticipantSearchEmail ({commit},participantSearchEmail) {
    commit(types.SET_PARTICIPANT_SEARCH_EMAIL, participantSearchEmail)
  },
  SetParticipantPageNo ({commit},participantPageNo) {
    commit(types.SET_PARTICIPANT_PAGE_NO, participantPageNo)
  },
  SetParticipantPerPage ({commit},participantPerPage) {
    commit(types.SET_PARTICIPANT_PER_PAGE, participantPerPage)
  },
  SetParticipantSearchData ({commit},participantSearchData) {
    commit(types.SET_PARTICIPANT_SEARCH_DATA, participantSearchData)
  },
  
 }
// mutations
const mutations = {
	[types.SET_PARTICIPANT_ID] (state, participnatId) {
    state.participnatId = participnatId
  },
  [types.SET_PARTICIPANT_SEARCH_NAME] (state, participantSearchName) {
    state.participantSearchName = participantSearchName
  },
  [types.SET_PARTICIPANT_SEARCH_EMAIL] (state, participantSearchEmail) {
    state.participantSearchEmail = participantSearchEmail
  },
  [types.SET_PARTICIPANT_PAGE_NO] (state, participantPageNo) {
    state.participantPageNo = participantPageNo
  },
  [types.SET_PARTICIPANT_PER_PAGE] (state, participantPerPage) {
    state.participantPerPage = participantPerPage
  },
  [types.SET_PARTICIPANT_SEARCH_DATA] (state, participantSearchData) {
    state.participantSearchData = participantSearchData
  },
  
}

export default {
  state,
  // getters,
  actions,
  mutations
}