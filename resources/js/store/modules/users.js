import * as types from '../mutation-types'
import _ from 'lodash'
import user from '../../api/apiService'
import moment from 'moment'

// initial state
const state = {
	'userId':0,
  'userPage':'',
  'userPageNo':0,
  'userPerPage':0,
  'sortedColumn':'',
  'order':'',
  'userSearchData':{},
  'userData':{},
  'roleType': 0




}


// actions
const actions = {
  SetUserId ({commit},userId) {
    commit(types.SET_USER_ID, userId)
  },
  SetUserPage ({commit},userPage) {
    commit(types.SET_USER_PAGE, userPage)
  },
  SetUserPageNo ({commit},userPageNo) {
    commit(types.SET_USER_PAGE_NO, userPageNo)
  },
  SetUserPerPage ({commit},userPerPage) {
    commit(types.SET_USER_PER_PAGE, userPerPage)
  },
  SetUserSearchData ({commit},userSearchData) {
    commit(types.SET_USER_SEARCH_DATA, userSearchData)
  },
  SetUserData ({commit},userData) {
    commit(types.SET_USER_DATA, userData)
  },
  SetUserSortedColumn ({commit},sortedColumn) {
    commit(types.SET_USER_SORTED_COLUMN, sortedColumn)
  },
  SetUserOrder ({commit},order) {
    commit(types.SET_USER_ORDER, order)
  },
  SetRoleType ({commit},userTypeId) {
    commit(types.SET_ROLE_TYPE, userTypeId)
  },
  SetUserType ({commit},userTypeId) {
    commit(types.SET_USER_TYPE, userTypeId)
  }
 }
// mutations
const mutations = {
	[types.SET_USER_ID] (state, userId) {
    state.userId = userId
  },
  [types.SET_USER_PAGE] (state, userPage) {
    state.userPage = userPage
  },
  [types.SET_USER_PAGE_NO] (state, userPageNo) {
    state.userPageNo = userPageNo
  },
  [types.SET_USER_PER_PAGE] (state, userPerPage) {
    state.userPerPage = userPerPage
  },
  [types.SET_USER_SEARCH_DATA] (state, userSearchData) {
    state.userSearchData = userSearchData
  },
  [types.SET_USER_DATA] (state, userData) {
    state.userData = userData
  },
  [types.SET_ROLE_TYPE] (state, userTypeId) {
    state.roleType = userTypeId
  },
  [types.SET_USER_TYPE] (state, userTypeId) {
    state.userData.userTypeId = userTypeId
  },
  [types.SET_USER_SORTED_COLUMN] (state, sortedColumn) {
    state.sortedColumn = sortedColumn
  },
  [types.SET_USER_ORDER] (state, order) {
    state.order = order
  },
}

export default {
  state,
  // getters,
  actions,
  mutations
}