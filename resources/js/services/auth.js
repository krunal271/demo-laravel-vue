import Ls from './ls'

export default {

    login(loginData){
        var vm =this;
        return axios.post('/api/auth/login', loginData).then(response =>  {
            Ls.set('auth.token',response.data.token)
            Ls.set('auth.front.token',response.data.token);
            Ls.set('auth.section','front');
            // We set Email Over here
            // Ls.set('email',response.data.userData.email);
             let userData = JSON.stringify(response.data.userData);
            Ls.set('userData',userData);
            // Ls.set('userData',response.data.userData);
            Ls.set('userType',response.data.userData.userTypeId);
            Ls.set('role',response.data.userData.userTypeId);
            //return {'status':true,'userData':response.data.userData};
            if (response.data.userData.status == 'inactive') {
                toastr['error']('Your accoount is not activated yet', '');
                return false;
            }
            if (response.data.userData.status == 'active') {
                return {'status':true,'userData':response.data.userData};
            }

        }).catch(error => {
            if (error.response.status == 401) {
                toastr['error']('Invalid credentials', 'Error');
                Ls.remove('auth.token');
                Ls.remove('email');
                localStorage.clear();
                return "error";
            }
             if (error.response.status == 500) {
                toastr['error']('Sorry something goes wrong. Please try again.', 'Error');
                Ls.remove('auth.token')
                Ls.remove('email');
                localStorage.clear();

                return "error";
            }
            else {
                return "error";
                
                // Something happened in setting up the request that triggered an Error
            }
        });


    },
    adminLogin(loginData){
        var vm =this;
        return axios.post('/api/auth/admin/login', loginData).then(response =>  {
            Ls.set('auth.token',response.data.token);
            Ls.set('auth.back.token',response.data.token)
            Ls.set('auth.section','back');

            // We set Email Over here
            Ls.set('email',response.data.userData.email);
            let userData = JSON.stringify(response.data.userData);
            Ls.set('userData',userData);
            Ls.set('userType',response.data.userData.userTypeId);
            //return {'status':true,'userData':response.data.userData};
            if (response.data.userData.status == 'inactive') {
                toastr['error']('Your accoount is not activated yet', '');
                return false;
            }
            if (response.data.userData.status == 'active') {
                return {'status':true,'userData':response.data.userData};
            }

        }).catch(error => {
            if (error.response.status == 401) {
                toastr['error']('Invalid credentials', 'Error');
                Ls.remove('auth.token')
                Ls.remove('email');
                return "error";
            }
             if (error.response.status == 500) {
                toastr['error']('Sorry something goes wrong. Please try again.', 'Error');
                Ls.remove('auth.token')
                Ls.remove('email');
                return "error";
            }
            else {
                return "error";
                
                // Something happened in setting up the request that triggered an Error
            }
        });
        

    },

    logout(){
        return axios.get('/api/auth/logout').then(response =>  {
            Ls.remove('auth.token')
            Ls.remove('email')
            Ls.remove('vuex')
            Ls.remove('role')
            localStorage.clear();
            // here we have to reload the page
        }).catch(error => {
        });
        // Reload
    },
    check(){
        return axios.get('/api/auth/check').then(response =>  {
            if(response.data.authenticated == false) {

              if(response.data.message != undefined) {
                toastr['error'](response.data.message, 'Error');
              }
            } else {
                Ls.set('userId',response.data.userData.userId);
                 // this.$store.dispatch('SetUserDetailsData',response.data.userData.id);
            }
            // return !!response.data.authenticated;
            return {'status':!!response.data.authenticated,'userData':response.data.userData};

        }).catch(error => {
        });
    },
    adminCheck(){
        return axios.get('/api/auth/admin/check').then(response =>  {
            if(response.data.authenticated == false) {

              if(response.data.message != undefined) {
                toastr['error'](response.data.message, '');
              }
                toastr['error']('Please login again', '');

            } else {
                // store.commit("routeChange", "start");

                Ls.set('userId',response.data.userData.userId);
                 // this.$store.dispatch('SetUserDetailsData',response.data.userData.id);
            }
            return {'status':!!response.data.authenticated,'userData':response.data.userData};
        }).catch(error => {
        });
    },

}
