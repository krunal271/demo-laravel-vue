$(document).ready(function(){
    $("#start_date1, #from_date, #to_Date").datepicker({
        format: 'D dd M yyyy',
        autoclose: true,
        todayHighlight: true
    });

    if($('.navbar-collapse').hasClass('show')){
        $('.navbar').addClass("openNav");
    } else{
        $('.navbar').removeClass("openNav");
    }

    $('[data-toggle="tooltip"]').tooltip();  

  // Full Calendar
  $('#calendar').fullCalendar({
    header: {
      left: 'prev,next',
      center: 'title',
      right: ''
    },
    defaultDate: '2018-03-12',
    navLinks: true, // can click day/week names to navigate views
    editable: true,
    eventLimit: true, // allow "more" link when too many events
    events: [
      {
        // title: '',
        // start: '2018-03-01'
      },
    ]
  });

  /* Height caclculate for login right div - add for safari browser */
  $('.loginImg-wrapper').css('height', $(window).outerHeight());
  $('body').css('padding-right', '0px');
});


