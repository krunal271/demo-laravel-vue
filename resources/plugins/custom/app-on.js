function ValidateEmail(mail)
{
    if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,50})+$/.test(mail)) {
        return true;
    } else { 
      return false;
    }
} 
function signInUser()
{   
        let loginData =  {email: $.trim($("#email").val()),password: $.trim($("#password").val()),remember: '',forgotpassword: 0};
        let emailStatus = ValidateEmail(loginData.email);        
        if (loginData.email == '' || emailStatus === false) {
            $(".error-email").removeClass('d-none');
            return false;
        } else {
            $(".error-email").addClass('d-none');
        }         

        if (loginData.password == '') {
            $(".error-password").removeClass('d-none');
            return false;
        } else {
            $(".error-password").addClass('d-none');
        }         

        if (loginData.email != '' && loginData.password != '' && emailStatus === true) {
          $(".preloader").show();
          return axios.post('/api/auth/login', loginData).then(response =>  {
          localStorage.setItem('auth.token',response.data.token)
          localStorage.setItem('auth.front.token',response.data.token);
          localStorage.setItem('auth.section','front');
          let userData = JSON.stringify(response.data.userData);
          localStorage.setItem('userData',userData);
          localStorage.setItem('userType',response.data.userData.userTypeId);
          localStorage.setItem('role',response.data.userData.userTypeId);
          if (response.data.userData.status == 'inactive') {
              toastr['error']('Your accoount is not activated yet', '');
              return false;
          }
          if (response.data.userData.status == 'active') {
              $(".preloader").hide();
              window.location.href = "/front/home";
              return {'status':true,'userData':response.data.userData};
          }                
        }).catch(error => {
            $(".preloader").hide();
            toastr.error('Invalid credentials', 'Error', {timeOut: 5000});
            if (error.response.status == 401) {
              localStorage.removeItem('auth.token');
              localStorage.removeItem('email');
              localStorage.clear();
              return "error";
            }
            if (error.response.status == 500) {
              localStorage.removeItem('auth.token')
              localStorage.removeItem('email');
              localStorage.clear();
              return "error";
            }
            else {
              return "error";
            }
          return false;
        });
  }
}
function forgotPasswordLink()
{
  let forgot_password = {
      email: $("#forgot_email").val(),
  }
  let emailStatus = ValidateEmail(forgot_password.email);
  if (emailStatus === false) {
      $(".error-forgot-mail").removeClass('d-none');
  } else {
      $(".error-forgot-mail").addClass('d-none');
      $(".preloader").show();
      return axios.post('/api/front/password/sendreset/link', forgot_password).then(
        response => {
            var resetLink  = '';
            if(response.data.status == 200) {
                var resetLink  = 'password/reset/'+response.data.token+'?email='+response.data.email;
                toastr.success('Mail sent successfully', 'Forgot password', {timeOut: 5000})
                $(".preloader").hide();
            }else if(response.data.status == 404) {
                toastr.error('Record not found', 'Error', {timeOut: 5000});
                $(".preloader").hide();
            } else{
                toastr.error('Something goes wrong', 'Error', {timeOut: 5000});
                $(".preloader").hide();
            }
        }
      )
  
  }
  
}

function CheckPassword(inputtxt) 
{ 
    var decimal=  /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[^a-zA-Z0-9])(?!.*\s).{8,64}$/;
    if (inputtxt.match(decimal))  { 
        $(".error-password").addClass('d-none');
        return true;
    } else { 
        $(".error-password").removeClass('d-none');
        return false;
    }
} 
function resetPassword()
{
    let url = $(location).attr('href').split("/").splice(0, 10).join("/");
    let param = url.split( '/' );
    let password = $("#password").val();
    let confirmPassword = $("#password_confirmation").val();
    let passwordStatus = CheckPassword(password);
    let resetData = {
        'token': param[4],
        'email': param[5],
        'password': password,
    }
    if (password == confirmPassword && passwordStatus) {
        $(".error-confirm-password").addClass('d-none');
        $(".preloader").show();
        return axios.post('/api/front/password/update',{'data': resetData}).then(
            (response) => {
                if (response.data.code == 200) {
                    $(".preloader").hide();
                    toastr.success('Password has been changed successfully', '', {timeOut: 5000});
                    setTimeout(function(){
                        window.location.href = '/';
                    },1000)
                    
                } else {
                    toastr.error('Smething goes wrong.', '', {timeOut: 5000});
                }
                $(".preloader").hide();
            },
                (error) => {
                $(".preloader").hide();
            },
        );
        return true;
    } else {
        $(".error-confirm-password").removeClass('d-none');
        return false;
    }
}
function cancel()
{
    window.location.href = '/';
}
/*Confirm account selection*/
let url = $(location).attr('href').split("/").splice(0, 10).join("/");
let param = url.split( '/' );
let pdata = {'token': param[4],'email': param[5]}
let confirmAccountData = {'givenName':'','surName':'','userId':'','token':param[4],'email':param[5],'password': '','confirmPassword':''}
var confirmURL= window.location.toString(); 
var res = confirmURL.match(/confirm-account/g);

if (Array.isArray(res)) {
    axios.post('/api/user/checkConfirmAccountRegistrationKey', pdata).then(
        response => {
            console.log('response');
            if(response.data.code == 200){
                getDetailsOfConfirmAccount();
            } else {
                toastr.error('Sorry link has been expired.', '', {timeOut: 5000});
                setInterval(function(){ 
                    window.location.href= '/';
                }, 2000);
            }
        }
    )
}

function getDetailsOfConfirmAccount()
{
    let vm=this;
    axios.post('/api/user/getDetailsOfConfirmAccount', pdata).then(
        response => {
            if(response.data.code == 200){
            $("#givenName").val(response.data.data.user_details.givenName);
            $("#surName").val(response.data.data.user_details.surName);
            confirmAccountData.givenName = response.data.data.user_details.givenName;
            confirmAccountData.surName = response.data.data.user_details.surName;
            confirmAccountData.userId = response.data.data.user_details.userId;
            } else {
            toastr.error(response.data.message, '', {timeOut: 5000});
            setInterval(function(){ 
                window.location.href= '/';
                }, 1000);
            }
        },
        error => {
            toastr.error('Something went wrong', '', {timeOut: 5000});
        },
    );
}
function addValidateBeforeSubmit() 
{
    let password = $.trim($("#password").val());
    let confirmPassword = $.trim($("#confirmPassword").val());
    let passwordStatus = CheckPassword(password);
    if (password != confirmPassword || !passwordStatus)  {
        $(".error-password").removeClass('d-none');
        $(".error-confirm-password").removeClass('d-none');
        return false;
    } else {
        $(".error-confirm-password").addClass('d-none');
        $(".error-password").addClass('d-none');
    }
    if(password != '' && passwordStatus)
    {
        $(".preloader").show();
        confirmAccountData.password = confirmPassword;
        confirmAccountData.confirmPassword = confirmPassword;
        axios.post('/api/user/addConfirmAccount', {'confirmAccountData': confirmAccountData}).then(
            (response)=> {
            if(response.data.code == 200){
                toastr.success(response.data.message, '', {timeOut: 5000});
                setInterval(function(){ 
                    window.location.href= '/';
                }, 3000);
                $(".preloader").hide();
            } 
            else if (response.data.code == 300) {
                $(".preloader").hide();
                toastr.error(response.data.message, '', {timeOut: 5000});
            }
            else {
                $(".preloader").hide();
                toastr.error('Something Went wrong.', '', {timeOut: 5000});
            }
            },
            (error)=>{
                $(".preloader").hide();
            }
        ).catch(
            error => {
                $(".preloader").hide();
                toastr.error('Something Went wrong.', '', {timeOut: 5000});
            }
        )

    }
}
function homePage()
{
    window.location.href = '/';
}
$("#forgotPassword").click(function(){
    $(".forgot-container").removeClass('d-none');
    $(".signup-container").addClass('d-none');
    $(".login-container").addClass('d-none');
});
$(".cancel").click(function(){
    $(".forgot-container").addClass('d-none');
    $(".signup-container").addClass('d-none');
    $(".login-container").removeClass('d-none');
});