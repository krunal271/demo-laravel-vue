<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="csrf-token" content="{{csrf_token()}}">
        <title>Deep</title>
        <link rel="shortcut icon" type="image/png" href="/images/deep.png"/>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <script src="https://cdn.polyfill.io/v2/polyfill.min.js"></script>  
        <script type="text/javascript" src="{{mix('/assets/js/core/front/plugins.js')}}"></script>
        <link  href=" {{ mix('css/front/app.css') }}" rel="stylesheet">
    </head>
    <body>
        <div  class="skin-default" id="app">
            <div data-v-750d289a="" class="preloader" style="position: fixed; width: 100%; height: 100%; top: 0px; left: 0px; z-index: 100001; backface-visibility: hidden; background: rgba(255, 255, 255, 0.85); display:none;"><div data-v-750d289a="" class="loader_img" style="width: 50px; height: 50px; position: absolute; left: 50%; top: 50%; background-position: center center; margin: -25px 0px 0px -25px; color: rgb(0, 114, 206);"><i data-v-750d289a="" class="fa fa-circle-o-notch fa-spin fa-5x"></i></div></div>        
            <div class="login-wrapper clearfix">
                <div class="loginForm-wrapper">
                    <img src="../../../images/logo.svg" alt="" class="logo">
                    <form class="form-horizontal">
                        <h2 class="text-center my-4">Reset Password</h2>
                        <div class="form-group">
                            <label>New Password *</label>
                            <input type="password" class="form-control" id="password"  name="password" placeholder="..." ref="password"  autocomplete="off" >
                            <span class="help is-danger error-password d-none">The password must be of minimum 8 character and maximum 64 characters long.<br>Password must contain at least: 1 uppercase letter, 1 lowercase letter, 1 number, and 1 special character.</span>
                        </div>
                        <div class="form-group">
                            <label>Confirm Password *</label>
                            <input type="password" class="form-control" placeholder="..." id="password_confirmation" name="password_confirmation" autocomplete="off">
                            <span class="help error-confirm-password is-danger d-none">
                                The password confirmation does not match. 
                            </span>
                        </div>
                        <div class="form-group text-center mt-5">
                            <button class="btn btn-primary btn-lg mr-3" type="button"  onclick="resetPassword()">Reset Password</button>
                            <button class="btn btn-primary btn-lg mr-3" type="button" onclick="cancel()">Cancel</button>
                        </div>
                    </form>
                </div>
                <div class="loginImg-wrapper">
                    <div class="login-overlay"></div>
                    <div class="loginTmg-inner">
                        <h1>Join Our Community</h1>
                        <div class="my-4">
                            <p>
                            We believe every child and young person has the right to live a life free from any form of violence, and deserves to grow up in a safe, peaceful, nurturing and enabling environment where they can fully exercise their rights.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script type="text/javascript" src="/js/login.js"></script>
    </body>
</html>