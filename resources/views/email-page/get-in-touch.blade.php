<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>:: Deep ::</title>
    <link rel="shortcut icon" type="image/png" href="/images/deep.png"/>
    <style type="text/css">
      body{
        font-family: Arial;
        color: #6A81A4;
        font-size: 14px;
        margin: 0px;
        padding: 0px;
      }
      p{
        margin: 5px 0;
        line-height: 1.5;
      }
      ul li{ padding: 4px 0; }
      h2{margin: 5px 0;}
      table tr th{margin-bottom: 30px;}
    </style>
  </head>
  <body>
    <table width="960px" style="margin: 0 auto;" cellspacing="0" cellpadding="0">
      <tbody>
        <tr>
          <td>
          <table width="100%" style="background-color: #F3F4F9;">
              <tbody>
                <tr>
                <td style="text-align: center;">
                      {{ HTML::image('images/emails/logo.png', 'Deep', array('width'=>'200px','style'=>'margin-top: 40px;')) }}
                    </td>
                </tr>
                <tr>
                  <td style="text-align: center;">
                      {{ HTML::image('images/emails/mail_slider_content1.png', 'Deep', array('style'=>'margin-top:  40px;')) }}
                  </td>
                </tr>
                <tr>
                  <td>
                    <h1 style="text-align: center; font-size: 48px; font-family: Arial; text-transform: uppercase;">CONTACT FORM</h1>
                  </td>
                </tr>
              </tbody>
            </table>
          </td>
        </tr>
        <tr>
          <td>
            <table width="100%" style="margin-top: 20px; padding: 0 20px;">
              <tbody>
                <tr>
                  <td width="80px"><strong>Email:</strong></td><td>{{ $data['email'] }}</td>
                </tr>                  
                <tr>
                  <td width="80px"><strong>Subject:</strong></td><td>{{ $data['subject'] }}</td>
                </tr>
                <tr>
                  <td style="padding: 10px;"></td>
                </tr>
                <tr>
                  <td colspan="2"><strong>Message:</strong></td>
                </tr>
                <tr>                  
                  <td colspan="2">{{ $data['message'] }}</td>
                </tr>
                <tr>
                  <td>
                  </td>
                </tr>
              </tbody>
            </table>
          </td>
        </tr>
        <tr>
          <td style="padding: 20px;"></td>
        </tr>
        <tr style="background-color: #F6F7F9; text-align: center;">
          <td style="padding: 10px 10px;">
            <p>Copyright © {{ date('Y') }} Plan,  All rights reserved.</p>
          </td>
        </tr>
      </tbody>
    </table>
  </body>
</html>