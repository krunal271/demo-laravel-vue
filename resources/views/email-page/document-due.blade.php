<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>:: Deep ::</title>
    <link rel="shortcut icon" type="image/png" href="/images/deep.png"/>
    <style type="text/css">
      body{
        font-family: Arial;
        color: #6A81A4;
        font-size: 14px;
        margin: 0px;
        padding: 0px;
      }
      p{
        margin: 5px 0;
        line-height: 1.5;
      }
      ul li{ padding: 4px 0; }
      h2{margin: 5px 0;}
      table tr th{margin-bottom: 30px;}
    </style>
  </head>
  <body>
    <table width="960px" style="margin: 20px auto 0 auto;" cellspacing="0" cellpadding="0">
      <tbody>
        <tr>
          <td>
            <table width="100%" style="background-color: #F3F4F9;">
              <tbody>
                <tr>
                <td style="text-align: center;">
                      {{ HTML::image('images/emails/logo.png', 'Deep', array('width'=>'200px','style'=>'margin-top: 40px;')) }}
                    </td>
                </tr>
                <tr>
                  <td style="text-align: center;">
                    <img src="mail_slider_content1.png" alt="" style="margin-top: 40px;">
                  </td>
                </tr>
                <tr>
                  <td>
                    <h1 style="text-align: center; font-size: 48px; font-family: Veneer; text-transform: uppercase;">Document Expiry (Due) </h1>
                  </td>
                </tr>
              </tbody>
            </table>
          </td>
        </tr>
        <tr>
          <td>
            <table width="100%" style="margin-top: 20px; padding: 0px 20px;">
              <tbody>
                <tr>
                  <td>
                    <p><strong>Dear {{$name}},</strong> <br/>
                      <br/>

                      Your {{$docName}} is expected to expire on {{$expiryDate}}.You will need to update your {{$docName}} before that date. </p>
                    <p style="margin-top: 20px;">
                        Please log onto DEEP  <a href="{{$loginUrl}}">Login Here</a>  to update your {{$docName}}. Your manager has also been notified so please check with them if you have any questions or issues.<br/></p>

                    <p>
                        Please note this is an automated alert generate from the DEEP platform. If you believe you have receive this in error, please contact your Plan International Australia Manager.
                    </p>
                  </td>
                </tr>

              </tbody>
            </table>
          </td>
        </tr>
        <tr>
            <td style="padding: 20px">&nbsp;</td>
        </tr>
        <tr style="background-color: #F6F7F9; text-align: center;">
          <td style="padding: 10px 10px;">
            <p>Copyright © {{ date('Y') }} Plan,  All rights reserved.</p>
          </td>
        </tr>
      </tbody>
    </table>
  </body>
</html>