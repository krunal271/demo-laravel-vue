<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Deep</title>
    <link rel="shortcut icon" type="image/png" href="/images/deep.png"/>
    <style type="text/css">
      body{
        font-family: Arial;
        color: #6A81A4;
        font-size: 14px;
        margin: 0px;
        padding: 0px;
      }
      p{
        margin: 5px 0;
        line-height: 1.5;
      }
      ul li{ padding: 4px 0; }
      h2{margin: 5px 0;}
      table tr th{margin-bottom: 30px;}
    </style>
  </head>
  <body>
    <table width="960px" style="margin: 0 auto;" cellspacing="0" cellpadding="0">
      <tbody>
        <tr>
          <td>
            <table width="100%" style="background-color: #F3F4F9;">
              <tbody>
                <tr>
                    <td style="text-align: center;">
                      {{ HTML::image('images/emails/logo.png', 'Deep', array('width'=>'200px','style'=>'margin-top: 40px;')) }}
                    </td>
                </tr>
                <tr>
                  <td style="text-align: center;">
                      {{ HTML::image('images/emails/mail_slider_content1.png', 'Deep', array('style'=>'margin-top:  40px;')) }}
                  </td>
                </tr>
                <tr>
                  <td>
                    <h1 style="text-align: center; font-size: 48px; font-family: Arial; text-transform: uppercase;">Reset Your Password</h1>
                  </td>
                </tr>
              </tbody>
            </table>
          </td>
        </tr>
        <tr>
          <td>
            <table width="100%" style="margin-top: 20px;">
              <tbody>
                <tr>
                  <td>
                    <p>Dear {{ $name }}, <br/>
                      <br/>

                      We received a request to reset your password for your Plan International Australia’s (PIA’s) Child Safeguarding Account: <a href="{{ $resetLink }}">{{ $resetLink }}</a>. We're here to help!</p>
                      <p style="margin-top:15px;">Simply click on the button to set a new password:</p>
                  </td>
                </tr>
                <tr>
                  <td>
                    <p style="text-align: center;"><a href="{{ $resetLink }}"><button type="submit" name="" style="background-color: #0072CE; border-color: #0072CE; font-size: 1rem; padding: 10px; min-width: 200px; color: #fff; border-radius: 3px; box-shadow: none; border-style: none; margin: 30px 0;">Set a New Password</button></a></p>
                    <p>If you didn't ask to change your password, don't worry! Your password is still safe and you can delete this email.</p>
                    <p style="margin-top: 30px;">Cheers, <br/> Plan International Australia (PIA)</p>
                  </td>
                </tr>
                <tr>
                  <td style="padding: 10px 10px;">&nbsp;</td>
                </tr>
              </tbody>
            </table>
          </td>
        </tr>
        <tr style="background-color: #F6F7F9; text-align: center;">
          <td style="padding: 10px 10px;">
            <p>Copyright ©  {{ date('Y') }} Plan,  All rights reserved.</p>
          </td>
        </tr>
      </tbody>
    </table>
  </body>
</html>