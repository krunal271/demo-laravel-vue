<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Deep</title>
    <link rel="shortcut icon" type="image/png" href="/images/deep.png"/>
    <style type="text/css">
      body{
        font-family: Arial;
        color: #6A81A4;
        font-size: 14px;
        margin: 0px;
        padding: 0px;
      }
      p{
        margin: 5px 0;
        line-height: 1.5;
      }
      ul li{ padding: 4px 0; }
      h2{margin: 5px 0;}
      table tr th{margin-bottom: 30px;}
    </style>
  </head>
  <body>
    <table width="960px" style="margin: 0 auto;" cellspacing="0" cellpadding="0">
      <tbody>
        <tr>
          <td>
            <table width="100%" style="background-color: #F3F4F9;">
              <tbody>
                <tr>
                <td style="text-align: center;">
                      {{ HTML::image('images/emails/logo.png', 'Deep', array('width'=>'200px','style'=>'margin-top: 40px;')) }}
                    </td>
                </tr>
                <tr>
                  <td style="text-align: center;">
                      {{ HTML::image('images/emails/mail_slider_content1.png', 'Deep', array('style'=>'margin-top:  40px;')) }}
                  </td>
                </tr>
                <tr>
                  <td>
                    <h1 style="text-align: center; font-size: 48px; font-family: Arial; text-transform: uppercase;">Invite Participant</h1>
                  </td>
                </tr>
              </tbody>
            </table>
          </td>
        </tr>
        <tr>
          <td>
            <table width="100%" style="margin-top: 20px;">
              <tbody>
                <tr>
                  <td>
                    <p>Dear {{ (!empty($name))?$name:$first_name }}, <br/>
                      <br/>
                      <p>Thank you very much for registering with Plan International Australia’s (PIA’s) Child Safeguarding platform.</p><br/>
                      <p>We believe every child and young person has the right to live a life free from any form of violence, and deserves to grow up in a safe, peaceful, nurturing and enabling environment where they can fully exercise their rights.</p><br/>
                      <p>We consider child safeguarding to be critical to PIA’s Operations. We appreciate your understanding and cooperation in our objective to best ensure the safety and protection of all children and young people in contact with PIA.</p><br/>
                      <p>The DEEP platform is specifically designed to help Plan International Australia ensure the safeguarding of children and young people. You will find the portal both easy to use and helpful, and you will have an opportunity to participate in some self-guided e-learning study about child safeguarding.</p><br/>
                      <p>To verify, please click the link below and follow the instructions to verify your account. DEEP works best in Google Chrome, so if you can, please use Chrome to verify your details and log in.</p>
                      <center>
                        <p style="text-align: center;"><a href="{{ $resetLink }}"><button type="submit" name="" style="background-color: #0072CE; border-color: #0072CE; font-size: 1rem; padding: 10px; min-width: 200px; color: #fff; border-radius: 3px; box-shadow: none; border-style: none; margin: 30px 0;">Verify Account</button></a></p>
                      </center>
                      <p>As we work together to build a just world for children, Plan International Australia, with your consent, will collect some of your personal information. By registering with DEEP, you are providing your consent. You can read our full privacy statement here: <br/><a href="https://www.plan.org.au/contact/privacy" target="_blank">https://www.plan.org.au/contact/privacy</a></p>
                      <p>The DEEP Child Safeguarding system is currently in its MVP (minimum viable product) stage. We take your information very seriously, and there will be changes coming to the system to make it more secure. If you have any questions or concerns about this, please notify your hiring manager or Plan International Australia contact.</p>
                      <p>Once verified, you can sign into DEEP any time, by visiting <a href="https://deep.myskillslive.com" target="_blank">https://deep.myskillslive.com</a></p>
                      <h2 style="margin-top: 25px; margin-bottom: 12px;"> Next Steps </h2>
                      <p>Using the platform will guide you through the steps of the onboarding process. These will require you to:</p>
                      <div>
                          <h3>1. Update your profile with your details <a href="{{ url('front/personaldetails') }}">here</a></h3>
                          <h3 style="margin-bottom: 2px;">2. Complete a Police Check. Click <a href="{{ url('front/documents') }}">here</a> to begin the process</h3>
                          <p style="padding-left: 20px; margin-top: 0px;">A 'nationally coordinated criminal history check' describes both: the checking process undertaken by the ACIC and police, and the result received by the accredited body. Commonly known as a ‘police check’.</p>
                          <h3 style="margin-bottom: 2px;">3. Upload your Working With Children Check</h3>
                          <p style="padding-left: 20px; margin-top: 0px;">A working with children's check is a state based screening clearance process for those working or volunteering with children in Australia. Individual states have their own requirements and check/card names. This applies to you if you are working with children. If your position is classified as working with children and you don’t currently have a valid WWCC, you can apply for one <a href="{{ url('front/documents') }}">here</a></p>
                          <h3>4. Complete your online learning modules <a href="{{ url('front/elearning') }}">here</a></h3>
                          <h3 style="margin-bottom: 2px;">5. Complete a In Person training session the Child Safeguarding Focal Point.</h3>
                          <p style="padding-left: 20px; margin-top: 0px;">This is a requirement for Plan International Employees and must take place within your first six months of engagement with Plan. Your Hiring Manager will schedule a session with you through DEEP.</p>
                      </div>
                      <br/>
                      <p>You can always monitor your completion of all the onboarding steps by clicking on the <a href="{{ url('front/childSafeguarding') }}">Dashboard</a> link when you are logged into DEEP.</p>
                      <p style="margin-top: 15px;"0>We look forward to welcoming you to the Plan International Australia Community!</p>
                  </td>
                </tr>
                <tr>
                  <td>
                    <p style="margin-top: 30px;">Cheers, <br/> Plan International Australia (PIA)</p>
                  </td>
                </tr>
                <tr>
                  <td style="padding: 10px 10px;">&nbsp;</td>
                </tr>
              </tbody>
            </table>
          </td>
        </tr>
        <tr style="background-color: #F6F7F9; text-align: center;">
          <td style="padding: 10px 10px;">
            <p>Copyright © {{ date('Y') }} Plan,  All rights reserved.</p>
          </td>
        </tr>
      </tbody>
    </table>
  </body>
</html>