<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="csrf-token" content="{{csrf_token()}}">
        <title>Deep</title>
        <link rel="shortcut icon" type="image/png" href="/images/deep.png"/>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <script src="https://cdn.polyfill.io/v2/polyfill.min.js"></script>  
        <script type="text/javascript" src="{{mix('/assets/js/core/front/plugins.js')}}"></script>
        <link  href=" {{ mix('css/front/app.css') }}" rel="stylesheet">
    </head>
    <body>
        <div  class="skin-default" id="app">
            <div data-v-750d289a="" class="preloader" style="position: fixed; width: 100%; height: 100%; top: 0px; left: 0px; z-index: 100001; backface-visibility: hidden; background: rgba(255, 255, 255, 0.85); display:none;"><div data-v-750d289a="" class="loader_img" style="width: 50px; height: 50px; position: absolute; left: 50%; top: 50%; background-position: center center; margin: -25px 0px 0px -25px; color: rgb(0, 114, 206);"><i data-v-750d289a="" class="fa fa-circle-o-notch fa-spin fa-5x"></i></div></div>
            <div class="login-wrapper clearfix">
                <div class="loginForm-wrapper">
                    <img src="../../../images/logo.svg" alt="" class="logo">
                    <div class="login-container">
                        <h2 class="text-center my-4">Sign In</h2>
                        <div class="form-group">
                            <label>Email *</label>
                            <input type="text" class="form-control" id="email" name="email" placeholder="Email" autocomplete="off">
                            <span class="help is-danger error-email d-none">
                                Please enter valid email address.
                            </span>
                        </div>
                        <div class="form-group">
                            <label>Password *</label>
                            <input type="password" class="form-control" id="password" name="password"  autocomplete="off">
                            <span class="help is-danger error-password d-none">
                                Field is required
                            </span>
                        </div>
                        <div class="row form-group">
                        <div class="col-sm-6">
                            </div> 
                            <div class="col-sm-6 text-right">
                                <a href="#" id="forgotPassword">Forgot Password</a>
                            </div>
                        </div>
                        <div class="form-group text-center">
                            <button class="btn btn-primary btn-lg mr-3" type="button" onclick="signInUser()">Submit</button>
                            <button class="btn btn-primary btn-lg mr-3 cancel" type="button">Cancel</button>
                        </div>
                    </div>
                    <div class="forgot-container d-none">
                        <h2 class="text-center my-4">Forgot Password</h2>
                        <div class="form-group">
                            <label>Email *</label>
                            <input type="text" class="form-control" placeholder="Email" id="forgot_email" name="forgot_email">
                            <span class="help is-danger error-forgot-mail d-none">
                                Please enter valid email address.
                            </span>
                        </div>
                        <div class="form-group text-center mt-5">
                            <button class="btn btn-primary btn-lg mr-3" onclick="forgotPasswordLink()" type="button">Forgot</button>
                            <button class="btn btn-primary btn-lg cancel" type="button">Cancel</button>
                        </div>
                    </div>
                    <div class="signup-container mt-2 d-none">
                        <h2 class="text-center my-4">Join DEEP (Confirm Account)</h2>
                        <p>Please check the details below. You will need to choose and confirm a password. Then click the Confirm button to confirm your DEEP account.</p>
                        <div class="form-group">
                            <label>Given Name</label>
                            <input type="text" class="form-control" placeholder="" value="Josephine" disabled>
                        </div>
                        <div class="form-group">
                            <label>Last Name</label>
                            <input type="text" class="form-control" placeholder="" value="Mentor" disabled>
                        </div>
                        <div class="form-group">
                            <label>Password *</label>
                            <input type="password" class="form-control" placeholder="Password" autocomplete="off">
                        </div>
                        <div class="form-group">
                            <label>Confirm Password *</label>
                            <input type="password" class="form-control" placeholder="Confirm Password" autocomplete="off">
                        </div>
                        <div class="form-group text-center mt-lg-5 mt-0">
                            <button class="btn btn-primary btn-lg mr-3" type="submit">Confirm Account</button>
                            <button class="btn btn-primary btn-lg cancel" type="submit">Cancel</button>
                        </div>
                    </div>
                </div>
                <div class="loginImg-wrapper">
                    <div class="login-overlay"></div>
                    <div class="loginTmg-inner">
                        <h1>Join Our Community</h1>
                        <div class="my-4">
                            <p>
                            We believe every child and young person has the right to live a life free from any form of violence, and deserves to grow up in a safe, peaceful, nurturing and enabling environment where they can fully exercise their rights.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <script src="/js/login.js"></script>
    </body>
</html>