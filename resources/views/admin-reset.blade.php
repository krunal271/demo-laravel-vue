<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
   <meta name="csrf-token" content="{{csrf_token()}}">
    <title>Deep</title>
    <link rel="shortcut icon" type="image/png" href="/images/deep.png"/>
    <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
    <script type="text/javascript" src="{{mix('/assets/js/core/front/plugins.js')}}"></script>
    <!-- <link href="../assets/assets/css/bootstrap.min.css" rel="stylesheet">-->
    <!-- <link href="../assets/css/fonts.css" rel="stylesheet">  -->
    <!-- <link href="../../public/css/front/app.css" rel="stylesheet"> -->
    <!-- <link href="../../public/css/custom.css" rel="stylesheet"> -->
        <link href=" {{ mix('css/front/app.css') }}" rel="stylesheet">

    <!-- <link href="../assets/css/custom.css" rel="stylesheet">  -->
   
</head>
<body>
 <!--    <div class="reset-wrapper">
        <p class="text-center"><img src="../assets/images/logo.svg" alt="" srcset="" class="reset-logo"></p>
        <div class="reset-container mt-5">
          <h2 class="text-center mt-2">Reset Password </h2>
          <form action="" class="mt-4">
            <div class="form-group">
              <label>New Password *</label> 
              <input type="password" placeholder="..." value="" required="required" class="form-control">
            </div>
            <div class="form-group">
              <label>Confirm  Password *</label> 
              <input type="password" placeholder="..." value="" required="required" class="form-control">
            </div>
            <div class="form-group text-center mt-4">
              <button type="submit" class="btn btn-primary btn-lg mr-3">Reset Password</button> 
              <button type="submit" class="btn btn-primary btn-lg mr-3">Cancel</button>
            </div>
          </form>
        </div>
    </div> -->
     <div  class="skin-default" id="app">
            <app></app>
        </div>
    <div class="overlay"></div>
    <!-- <script src="../plugins/bootstrap.min.js"></script> -->
        <script src="{{ mix('js/app.js') }}"></script>

</body>
</html>