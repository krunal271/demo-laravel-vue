 <!DOCTYPE html>
    <html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
       <meta name="csrf-token" content="{{csrf_token()}}">
        <title>Deep</title>
        <link rel="shortcut icon" type="image/png" href="/images/deep.png"/>
        <meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
        <script src="https://cdn.polyfill.io/v2/polyfill.min.js"></script>
        <link href=" {{ mix('css/admin/app.css') }}" rel="stylesheet">
    </head>
    <body>
        <div  class="skin-default" id="app">
            <app></app>
        </div>
        <script type="text/javascript" src="{{mix('/assets/js/core/admin/plugins.js')}}"></script>
        <script src="{{ mix('js/app.js') }}"></script>
    </body>
    </html>