
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>:: DEEP ::</title>
    <link rel="shortcut icon" type="image/png" href="/images/deep.png"/>
    <style type="text/css">
      body{
        font-family: Arial;
        color: #6A81A4;
        font-size: 14px;
        margin: 0px;
        padding: 0px;
      }
      p{
        margin: 5px 0;
        line-height: 1.5;
      }
      ul li{ padding: 4px 0; }
      h2{margin: 5px 0;}
      table tr th{margin-bottom: 30px;}
    </style>
  </head>
  <body>
    <table width="960px" style="margin: 0 auto;" cellspacing="0" cellpadding="0">
      <tbody>
        <tr>
          <td>
            <table width="100%" style="background-color: #F3F4F9;">
              <tbody>
                <tr>
                    <td style="text-align: center;">
                    {{ HTML::image('images/emails/logo.png', 'Deep', array('width'=>'200px','style'=>'margin-top: 40px;')) }}
                    </td>
                </tr>
                <tr>
                  <td style="text-align: center;">
                    <!--  <img src="mail_slider_content1.png" alt="" style="margin-top: 40px;">  -->
                   {{ HTML::image('images/emails/mail_slider_content1.png', 'Deep', array('style'=>'margin-top: 40px;')) }}
                  </td>
                </tr>
                <tr>
                  <td>
                    <h1 style="text-align: center; font-size: 35px; font-family: Arial; text-transform: uppercase;">Thanks for coming to our In Person Session</h1>
                  </td>
                </tr>
              </tbody>
            </table>
          </td>
        </tr>
        <tr>
          <td>
            <table width="100%" style="margin-top: 20px;">
              <tbody>
                <tr>
                  <td>

                    <p>Dear {{$name}}, <br/>
                      <br/>

                      Thank you for your engagement with Plan International Australia’s Child Safeguarding system.</p>

                      <p style="margin-top:15px;">We believe every child and young person has the right to live a life free from any form of violence, and deserves to grow up in a safe, peaceful, nurturing and enabling environment where they can fully exercise their rights.</p>

                      <p style="margin-top:15px;">The Child Safeguarding Focal Point has confirmed that you attended the below In Person session:</p>


                  </td>
                </tr>
                <tr>
                  <td>
                    <p style="margin-top:5px;"><b>Date :</b> {{$bookingDate}}</p>
                    <p><b>Time :</b> {{$bookingTime}}</p><br>
                    <p style="margin-top:10px;">Thank you for coming and participating in the session. If you have any questions about the session, please ask our Child Safeguarding Focal Point.</p><br>
                  </td>
                </tr>
              </tbody>
            </table>
          </td>
        </tr>
        <tr>
          <td style="padding: 30px 10px;"></td>
        </tr>
        <tr style="background-color: #F6F7F9; text-align: center;">
          <td style="padding: 10px 10px;">
            <p>Copyright © {{ date('Y') }} Plan,  All rights reserved.</p>
          </td>
        </tr>
      </tbody>
    </table>
  </body>
</html>