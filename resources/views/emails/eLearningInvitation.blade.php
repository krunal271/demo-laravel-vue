<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Deep</title>
    <link rel="shortcut icon" type="image/png" href="/images/deep.png"/>
    <style type="text/css">
      body{
        font-family: Arial;
        color: #6A81A4;
        font-size: 14px;
        margin: 0px;
        padding: 0px;
      }
      p{
        margin: 5px 0;
        line-height: 1.5;
      }
      ul li{ padding: 4px 0; }
      h2{margin: 5px 0;}
      table tr th{margin-bottom: 30px;}
    </style>
  </head>
  <body>
    <table width="960px" style="margin: 0 auto;" cellspacing="0" cellpadding="0">
      <tbody>
        <tr>
          <td>
            <table width="100%" style="background-color: #F3F4F9;">
              <tbody>
                <tr>
                    <td style="text-align: center;">
                    {{ HTML::image('images/emails/logo.png', 'Deep', array('width'=>'200px','style'=>'margin-top: 40px;')) }}
                    </td>
                </tr>
                <tr>
                  <td style="text-align: center;">
                     {{ HTML::image('images/emails/mail_slider_content1.png', 'Deep', array('style'=>'margin-top:  40px;')) }}
                  </td>
                </tr>
                <tr>
                  <td>
                    <h1 style="text-align: center; font-size: 35px; font-family: Arial; text-transform: uppercase;">E-Learning</h1>
                  </td>
                </tr>
              </tbody>
            </table>
          </td>
        </tr>
        <tr>
          <td>
            <table width="100%" style="margin-top: 20px;">
              <tbody>
                <tr>
                  <td>
                    <p>Hello {{ $user['user_detail']['givenName'] }} {{ $user['user_detail']['surName'] }},
                     <br/>
                      <br/>
                      Please complete your e-learning module by clicking below link. <br/>
                      <center><a href="{{ $user['link'] }}" class="btn btn-success"><button style="background-color: #0072CE; border-color: #0072CE; font-size: 1rem; padding: 10px; min-width: 200px; color: #fff; border-radius: 3px; box-shadow: none; border-style: none; margin: 30px 0;">Start Module</button></a></center>
                      </p>
                  </td>
                </tr>
                <tr>
                  <td>
                    <p>Cheers,<br/>
                      Plan International Australia (PIA)
                    </p>
                  </td>
                </tr>
                <tr>
                  <td style="padding: 10px 10px;">&nbsp;</td>
                </tr>
              </tbody>
            </table>
          </td>
        </tr>
        <tr style="background-color: #F6F7F9; text-align: center;">
          <td style="padding: 10px 10px;">
            <p>Copyright ©  {{ date('Y') }} Plan,  All rights reserved.</p>
          </td>
        </tr>
      </tbody>
    </table>
  </body>
</html>