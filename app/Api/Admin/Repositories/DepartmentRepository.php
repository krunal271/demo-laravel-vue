<?php

namespace App\Api\Admin\Repositories;

use App\Models\Department;
use App\User;

/**
 * Here goes the description of the class. It should explain what the main
 * purpose of this class is to CRUD of department
*/
class DepartmentRepository
{
    /**
     * [__construct]
     */
    public function __construct()
    {
        //
    }
    /**
     * [getDepartmentSelectListByUserTypeId - get department select list by
     * user type id]
     * @param  [type] $userTypeId [description]
     * @return [type]             [description]
     */
    public function getDepartmentSelectListByUserTypeId($userTypeId)
    {
        $list = Department::orderBy('created_at', 'asc')->where('department.userTypeId', $userTypeId)->where('status', 'active')->get();
        return $list;
    }
    /**
     * [getDepartmentList - get department list with pagination and search]
     * @param  [type]  [description]
     * @return [type]             [description]
     */
    public function getDepartmentList($noOfPage, $searchData, $sortData)
    {
        if (!empty($searchData)) {
            $searchKey = trim(strtolower($searchData['searchKey']));
            $searchText = trim(strtolower($searchData['searchText']));
            /*
            $deptList = Department::with('userDepartment');
            if ($searchKey === 'usertype') {
                $deptList->whereHas('userType', function ($query1) use ($searchKey, $searchText) {
                    $query1->where('user_type.type', 'LIKE', '%' . $searchText . '%');
                });
            }
            if ($searchKey === 'departmentname') {
                $deptList->where('department.name', 'like', '%' . $searchText . '%');
            }
            if ($searchKey === 'status') {
                $deptList->where('department.status', '=', $searchText);
            }
            $deptList = $deptList->orderBy($sortData['sortedColumn'], $sortData['order'])->paginate($noOfPage);
            */
            $deptList = Department::select('department.*','user_type.type')->with('userType')->join('user_type','user_type.userTypeId','=','department.userTypeId');
            if ($searchKey === 'departmentname') {
                $deptList->where('department.name', 'like', '%' . $searchText . '%');
            }
            if ($searchKey === 'status') {
                $deptList->where('department.status', '=', $searchText);
            }
            if ($searchKey === 'usertype') {
                $deptList->where('user_type.type', 'like', '%'.$searchText.'%');
            }
            
            if ($sortData['sortedColumn'] == 'type') {
                $deptList = $deptList->orderBy('user_type.type', $sortData['order'])->paginate($noOfPage);
            } else {
                $deptList = $deptList->orderBy('department.'.$sortData['sortedColumn'], $sortData['order'])->paginate($noOfPage);
            }
            
        } else {
            $deptList = Department::with('userDepartment')->orderBy($sortData['sortedColumn'], $sortData['order'])->paginate($noOfPage);
        }
        return $deptList;
    }
    /**
     * [addDepartment - add department]
     * @param [type] $departmentData [description]
     */
    public function addDepartment($departmentData)
    {
        $deptObj = new Department();
        $deptObj->userTypeId = $departmentData['userTypeId'];
        $deptObj->name = $departmentData['department'];
        $deptObj->status = $departmentData['status'];
        $deptStatus = $deptObj->save();
        return $deptStatus;
    }
    /**
     * [deleteDepartment - delete department by department id]
     * @param  [type] $departmentId [description]
     * @return [type]               [description]
     */
    public function deleteDepartment($departmentId)
    {
        $dCount = User::where('departmentId', $departmentId)->count();
        if ($dCount > 0) {
            return 2;
        } else {
            $deptStatus = Department::destroy($departmentId);
        }
        return $deptStatus;
    }
    /**
     * [deleteMultipleDepartmentById - delete multiple department by id]
     * @param  [type] $departmentIdArray [description]
     * @return [type]                    [description]
     */
    public function deleteMultipleDepartmentById($departmentIdArray)
    {
        foreach ($departmentIdArray as $key => $departmentId) {
            $dCount = User::where('departmentId', $departmentId)->count();
            if ($dCount === 0) {
                $deptStatus = Department::destroy($departmentId);
            }
        }
        return 2;
    }
    /**
     * [getDepartmentById - get department details by department id]
     * @param  [type] $data [description]
     * @return [type]       [description]
     */
    public function getDepartmentById($data)
    {
        $data = Department::select('userTypeId', 'departmentId', 'name', 'status')->find($data['departmentId']);
        return $data;
    }
    /**
     * [editDepartmentById - edit department by form data]
     * @param  [type] $departmentData [description]
     * @return [type]                 [description]
     */
    public function editDepartmentById($departmentData)
    {
        $deptData = array('userTypeId' => $departmentData['userTypeId'], 'name' => $departmentData['name'], 'status' => $departmentData['status']);
        $deptStatus = Department::where('departmentId', $departmentData['departmentId'])->update($deptData);

        return $deptStatus;
    }
    /**
     * [updateMultipleDepartmentStatusByDepartmentId - update multiple
     * department status department id]
     * @param  [type] $departmentData [description]
     * @return [type]                 [description]
     */
    public function updateMultipleDepartmentStatusByDepartmentId($departmentData)
    {
        if (isset($departmentData['departmentIds'])) {
            $departmentIdArr = $departmentData['departmentIds'];
        }
        $updateStatus = $departmentData['actionStatus'];

        $user = Department::whereIn('departmentId', $departmentIdArr)->update([
            'status' => $updateStatus,
        ]);
        return $user;
    }
}
