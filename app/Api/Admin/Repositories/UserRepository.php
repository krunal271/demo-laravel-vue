<?php

namespace App\Api\Admin\Repositories;

use Carbon\Carbon;
use DB;
use App\User;
use App\Models\UserDetail;
use App\Models\UserType;
use Excel;
use File;
use Hash;
use Schema;
use Mail;
use App\Api\Traits\LearningTrait;
/**
 * Here goes the description of the class. It should explain what the main
 * purpose of this class is to CRUD of user
*/
class UserRepository
{

    use LearningTrait;
    /**
      * [__construct]
      */
    public function __construct()
    {
        //
    }
    /**
     * [getUserListPagination - get user list by pagination and searchdata]
     * @param  [type] $noOfPage [description]
     * @return [type]           [description]
     */
    public function getUserListPagination($noOfPage, $searchData, $sortedColumn, $order)
    {
        if (!empty($searchData)) {
            $searchKey=trim(strtolower($searchData['searchKey']));
            $searchText=trim(strtolower($searchData['searchText']));
            if ($searchKey!=='' && $searchText!=='') {
                $reportQuery=User::leftJoin('user_type', 'user_type.userTypeId', '=', 'user.userTypeId')->leftJoin('user_category', 'user_category.userCategoryId', '=', 'user.userCategoryId')->leftJoin('user_detail', 'user_detail.userId', '=', 'user.userId');
                $reportQuery=$reportQuery->where('user_type.type', '!=', 'superAdmin');
                if ($searchKey==='usertype') {
                    $reportQuery->where('user_type.type', 'LIKE', '%' .$searchText. '%');
                }
                if ($searchKey==='usercategory') {
                    $reportQuery->where('user_category.type', 'LIKE', '%' .$searchText. '%');
                }
                if ($searchKey==='name') {
                    $allStnames=explode(' ', $searchText);
                    foreach ($allStnames as $stName) {
                        $reportQuery->where('user_detail.givenName', 'like', '%'.$stName.'%')
                              ->orWhere('user_detail.surname', 'like', '%'.$stName.'%');
                        // ->orWhere('user_detail.prefferedName', 'like', '%'.$stName.'%');
                    }
                }
                if ($searchKey==='contactphone') {
                    $reportQuery->where('user_detail.contactPhone', 'LIKE', '%' .$searchText. '%');
                }
               
                if ($searchKey==='email') {
                    $reportQuery->where('user.email', 'LIKE', '%' .$searchText. '%');
                }
                if ($searchKey==='status') {
                    $reportQuery->where('user.status', $searchText);
                }
                $reportQuery->orderBy($sortedColumn, $order);
                $reportQuery->select(DB::raw('CONCAT(user_detail.givenName, " ", user_detail.surname) AS full_name'), 'user.userId as userId', 'user.email', 'user.status as status', 'user.created_at', 'user_type.type as userTypeName', 'user_category.type as userCategoryName', 'user_detail.contactPhone');
                $list=$reportQuery->paginate($noOfPage);


                /*$reportQuery=User::with('userType')->with('userCategory')->with('userDetails');
                if ($searchKey==='usertype') {
                    $reportQuery->whereHas('userType', function ($query1) use ($searchKey,$searchText) {
                        $query1->where('type', '!=', 'superAdmin');
                        if ($searchKey==='usertype') {
                            $query1->where('type', 'LIKE', '%' .$searchText. '%');
                        }
                    });
                }

                if ($searchKey==='usercategory') {
                    $reportQuery->whereHas('userCategory', function ($query3) use ($searchKey,$searchText) {
                        if ($searchKey==='usercategory') {
                            $query3->where('type', 'LIKE', '%' .$searchText. '%');
                        }
                    });
                }
                $reportQuery->whereHas('userDetails', function ($query2) use ($searchKey,$searchText) {
                    if ($searchKey==='name') {
                        $allStnames=explode(' ', $searchText);
                        foreach ($allStnames as $stName) {
                            $query2->where('givenName', 'like', '%'.$stName.'%')
                                  ->orWhere('surname', 'like', '%'.$stName.'%')
                                  ->orWhere('prefferedName', 'like', '%'.$stName.'%');
                        }
                    }
                    if ($searchKey==='contactphone') {
                        $query2->where('contactPhone', 'LIKE', '%' .$searchText. '%');
                    }
                });
                if ($searchKey==='email') {
                    $reportQuery->where('email', 'LIKE', '%' .$searchText. '%');
                }
                if ($searchKey==='status') {
                    $reportQuery->where('status', $searchText);
                }
                dd($reportQuery->toSql());
                $list = $reportQuery->orderBy('created_at', 'asc')->paginate($noOfPage);*/
                
               
                if (count($list)>0) {
                    return ['code' => 200 ,'data'=>$list,'title'=>'User List','message'=>'Getting User List successfully.'];
                } else {
                    return ['code' => 300 ,'data'=>'','title'=>'User List','message'=>'No record found.'];
                }
            } else {
                $list=$this->userDefaultResult($noOfPage, $sortedColumn, $order);
                if ($searchKey==='') {
                    return ['code' => 300 ,'data'=>$list,'title'=>'User List','message'=>'Please select search.'];
                }
                if ($searchText==='') {
                    return ['code' => 300 ,'data'=>$list,'title'=>'User List','message'=>'Please enter search value.'];
                }
            }
        } else {
            $list=$this->userDefaultResult($noOfPage, $sortedColumn, $order);
            return ['code' => 200 ,'data'=>$list,'title'=>'User List','message'=>'Getting User List successfully.'];
        }
    }
    /**
     * [userDefaultResult - user list]
     * @param  [type] $noOfPage [description]
     * @return [type]           [description]
     */
    public function userDefaultResult($noOfPage, $sortedColumn, $order)
    {
        /*$reportQuery=User::with('userType');
        $reportQuery->with('userCategory');
        $reportQuery->with('userDetails');
        $reportQuery->whereHas('userType', function ($query1) {
                $query1->where('type', '!=', 'superAdmin');
                //$query1->orderBy('type', 'desc');
        });
        if ($sortedColumn==='email' || $sortedColumn==='status') {
            $reportQuery->orderBy($sortedColumn, $order);
        }
        dd($reportQuery->toSql());
        $list=$reportQuery->paginate($noOfPage);*/

        $reportQuery=User::leftJoin('user_type', 'user_type.userTypeId', '=', 'user.userTypeId')->leftJoin('user_category', 'user_category.userCategoryId', '=', 'user.userCategoryId')->leftJoin('user_detail', 'user_detail.userId', '=', 'user.userId');
        $reportQuery=$reportQuery->where('user_type.type', '!=', 'superAdmin');
        $reportQuery->orderBy($sortedColumn, $order);
        $reportQuery->select(DB::raw('CONCAT(user_detail.givenName, " ", user_detail.surname) AS full_name'), 'user.userId as userId', 'user.email', 'user.status as status', 'user.created_at', 'user_type.type as userTypeName', 'user_category.type as userCategoryName', 'user_detail.contactPhone');
        $list=$reportQuery->paginate($noOfPage);
         
        return $list;
    }

    /**
     * [getHiringManagerSelectList - get hirung manager for select box.]
     * @return [type] [description]
     */
    public function getHiringManagerSelectList()
    {
        $list=User::with('userDetail')->with('userType')
        ->whereHas('userType', function ($query1) {
            $query1->where('type', 'HMU');
            $query1->orWhere('isHmu', '1');
        })
        ->where('status', 'active')
        ->get();
        return $list;
    }

    /**
     * [create - create user ]
     * @param  [type] $request [description]
     * @return [type]          [description]
     */
    public function createUser($formData)
    {
        //$formData=$request->userData;
        $engagementStatus = 'applicationInProgress';
        $createBy = $formData['loginId'];
        $typeName=$formData['userTypeName'];
        //for check email duplicate
        $checkEmail=$this->checkEmailDuplicate($formData['email']);
        if ($checkEmail>0) {
            return ['code' => 300 ,'data'=>'','title'=>'User Add','message'=>'Email already exist.'];
        }
        //for check orgemail duplicate
        $checkOrgEmail=$this->checkOrgEmailDuplicate($formData['orgEmail']);
        if ($checkOrgEmail>0 && $formData['orgEmail']!=='' && $formData['orgEmail']!==null) {
            return ['code' => 300 ,'data'=>'','title'=>'User Add','message'=>'Org Email already exist.'];
        }
        $phone=$formData['contactNoPrefix'].'|'.$formData['contactPhone'];
        //for check contactphone duplicate
        $checkContactPhone=$this->checkContactPhoneDuplicate($phone);
        if ($checkContactPhone>0) {
            return ['code' => 300 ,'data'=>'','title'=>'User Add','message'=>'Contact phone already exist.'];
        }
        
        
       
        //$dob=Carbon::createFromFormat('d/m/Y', $formData['dateOfBirth'])->format('Y-m-d');

        //for user add
        $userAdd= new User;
        $userAdd->userTypeId=$formData['userTypeId'];
        $userAdd->email=$formData['email'];
        $userAdd->orgEmail=$formData['orgEmail'];
        $userAdd->role=$formData['role'];
        $userAdd->profileCompleteDate=$formData['confirmDate'];
        if ($typeName==='Participant') {
            //$userAdd->password=Hash::make(trim($formData['password']));
            //} else {
            $userAdd->userCategoryId=$formData['userCategoryId'];
            $userAdd->departmentId=$formData['departmentId'];
            $userAdd->hiringManager=$formData['hiringManager'];
        }
           
        $userAdd->status=$formData['status'];
        $userAdd->engagementStatus=$engagementStatus;
        $userAdd->createdBy=$createBy;
        $userAdd->created_at=Carbon::now();
        $userAdd->save();
        $userId=$userAdd->userId;
        //for user details add
        $userDetail= new UserDetail;
        $userDetail->userId=$userId;
        $userDetail->givenName=$formData['givenName'];
        $userDetail->surName=$formData['surName'];
        $userDetail->prefferedName=$formData['prefferedName'];
        $userDetail->occupation=$formData['occupation'];
        $userDetail->gender=$formData['gender'];
        if ($formData['gender']==='self_desc') {
            $userDetail->selfDescribe=$formData['selfDescribe'];
        } else {
            $userDetail->selfDescribe=null;
        }
        $userDetail->dateOfBirth=$formData['dateOfBirth'];
        $userDetail->contactPhone=$phone;
        $userDetail->levelOfContact=$formData['levelOfContact'];
        $userDetail->pacificBasedRole=$formData['pacificBaseRole'];
        if($userDetail->pacificBasedRole === 'Y'){
            $userDetail->pacificBaseCountry = $formData['pacificBaseCountry']; 
        }
        $userDetail->policeCheque=$formData['policeCheckRequire'];
        if ($formData['contactNo2'] !== '') {
            $contactphoneNo2 = $formData['contactNo2Prefix'].'|'.$formData['contactNo2'];
            $userDetail->contactPhone2= $contactphoneNo2;
        }
        $userDetail->address=$formData['address'];
        $userDetail->aboriginal=$formData['aboriginalValue'];
        $userDetail->disability=$formData['disability'];
        $userDetail->emergencyContactPrimary=$formData['emergencyPrimary'];
        $userDetail->relationshipPrimary=$formData['relationshipPrimary'];
        if ($formData['emergencyPrimaryContact'] !== '') {
            $emergencyPrimaryContact = $formData['emergencyPrimaryContactPrefix'].'|'.$formData['emergencyPrimaryContact'];
            $userDetail->contactPhonePrimary=$emergencyPrimaryContact;
        }
        $userDetail->emergencyContactSecondary=$formData['emergencySecondary'];
        $userDetail->relationshipSecondary=$formData['relationshipSecondary'];
        if ($formData['emergencySecordaryContact'] !== '') {
            $emergencySecordaryContact = $formData['emergencySecordaryContactPrefix'].'|'.$formData['emergencySecordaryContact'];
            $userDetail->contactPhoneSecondary=$emergencySecordaryContact;
        }
        $userDetail->candidateLegalWorkAustralia=$formData['legalWorkInAustralia'];
        $userDetail->residentialStatus=$formData['residentialStatus'];
        if ($formData['residentialStatus'] === 'other') {
            $userDetail->otherResidentialStatus=$formData['otherResidentialStatus'];
        }
        $userDetail->confirmInformation=$formData['confirm'];
        $userDetail->visaAttachments=$formData['visaFileVal'];
        $userDetail->createdBy=$createBy;
        $userDetail->created_at=Carbon::now();
        //$userDetail->dateOfBirth=$dob;
        $userDetail->save();

        if ($userId!==0 || $userDetail!==0) {
            //if ($typeName==='Participant') {
            $this->sendMail($userId, $createBy);
            //}
            return ['code' => 200 ,'data'=>'','title'=>'User Add','message'=>'User Added successfully.'];
        } else {
            return ['code' => 300 ,'data'=>'','title'=>'User Add','message'=>'User not added.'];
        }
    }

    /**
    *  send mail of create participant
    *
    *  @return status
    *
    *  Author : Richa
    **/
    public function sendMail($userId, $loginId)
    {
        // $this->userObj = new User();
        $userDetails=$this->getUserDetailsByUserId($userId);
        $loginDetails=$this->getUserDetailsByUserId($loginId);
        $token = str_random(64);
        $passwordReset = \DB::table('password_reset')->where('email', $userDetails->email)->first();
        if (empty($passwordReset)) {
            DB::table('password_reset')->insert([
                            'email' => $userDetails->email,
                            'token' => $token,
                            'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                            'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
                    ]);
        } else {
            DB::table(config('auth.passwords.users.table'))->where('email', $userDetails->email)->
                        update([
                        'token' => $token,
                        'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                        'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
                    ]);
        }
        $url = '';
        $link = '';
        $url =  config('app.url');
        //$url = URL::current();
        //$url = url('/');
        $link = $url. '/confirm-account/' . $token . '/' . $userDetails->email;
        $name=ucfirst($userDetails['userDetails']['givenName']).' '.ucfirst($userDetails['userDetails']['surName']);
        $data=array();
        $data['user']=$userDetails;
        $data['name']=$name;
        $data['first_name']=$name;
        $data['link']=$link;
        $mail=Mail::send('emails.createParticipant', $data, function ($m) use ($userDetails,$loginDetails,$name) {
            $m->from(env('MAIL_USERNAME'), 'DEEP');
            $m->to($userDetails->email, $name)->subject('Create Participant');
        });
        $userEdit=User::findOrFail($userId);
        $userEdit->inviteUser='Y';
        $userEdit->save();
        return $mail;
    }

    /**
     * [checkContactPhoneDuplicate - check duplicate for contact phone]
     * @param  [type] $contactPhone [description]
     * @return [type]               [description]
     */
    public function checkContactPhoneDuplicate($contactPhone)
    {
        $list= UserDetail::where('contactPhone', $contactPhone)->get();
        return count($list);
    }
    /**
     * [checkEmailDuplicate - check email duplication]
     * @param  [type] $email [description]
     * @return [type]        [description]
     */
    public function checkEmailDuplicate($email)
    {
        $list= User::where('email', $email)->get();
        return count($list);
    }

    /**
     * [checkOrgEmailDuplicate - check org email duplicate]
     * @param  [type] $orgEmail [description]
     * @return [type]           [description]
     */
    public function checkOrgEmailDuplicate($orgEmail)
    {
        $list= User::where('orgEmail', $orgEmail)->get();
        return count($list);
    }

    /**
     * [checkEditEmailDuplicate - edit email duplidate]
     * @param  [type] $email  [description]
     * @param  [type] $userId [description]
     * @return [type]         [description]
     */
    public function checkEditEmailDuplicate($email, $userId)
    {
        $list= User::where('email', $email)->where('userId', '!=', $userId)->get();
        return count($list);
    }

    /**
     * [checkEditOrgEmailDuplicate - org email duplicate]
     * @param  [type] $orgEmail [description]
     * @param  [type] $userId   [description]
     * @return [type]           [description]
     */
    public function checkEditOrgEmailDuplicate($orgEmail, $userId)
    {
        $list= User::where('orgEmail', $orgEmail)->where('userId', '!=', $userId)->get();
        return count($list);
    }

    /**
     * [checkEditContactPhoneDuplicate - edit contact phone duplicate check]
     * @param  [type] $contactPhone [description]
     * @param  [type] $userId       [description]
     * @return [type]               [description]
     */
    public function checkEditContactPhoneDuplicate($contactPhone, $userId)
    {
        $list= UserDetail::where('contactPhone', $contactPhone)->where('userId', '!=', $userId)->get();
        return count($list);
    }

    /**
     * [getUserDetailsByUserId - get user details]
     * @param  [type] $userId [description]
     * @return [type]         [description]
     */
    public function getUserDetailsByUserId($userId)
    {
        $details=User::with('userType')
            ->with('userDetails')
            ->where('userId', $userId)
            ->first();
        //$details['userDetails']['dateOfBirth']=Carbon::createFromFormat('Y-m-d', $details['userDetails']['dateOfBirth'])->format('d/m/Y');
        return $details;
    }

    /**
     * [editUser - edit user]
     * @param  [type] $request [description]
     * @return [type]          [description]
     */
    public function editUser($formData)
    {
        //$formData=$request->userData;
        $userId=$formData['userId'];
        $userDetailId=$formData['userDetailId'];
        $updatedBy = $formData['loginId'];
        //for check email duplicate
        $checkEmail=$this->checkEditEmailDuplicate($formData['email'], $userId);
        if ($checkEmail>0) {
            return ['code' => 300 ,'data'=>'','title'=>'User Update','message'=>'Email already exist.'];
        }
        //for check orgemail duplicate
        $checkOrgEmail=$this->checkEditOrgEmailDuplicate($formData['orgEmail'], $userId);
        if ($checkOrgEmail>0 && $formData['orgEmail']!=='' && $formData['orgEmail']!==null) {
            return ['code' => 300 ,'data'=>'','title'=>'User Update','message'=>'Org Email already exist.'];
        }
        $phone=$formData['contactNoPrefix'].'|'.$formData['contactPhone'];
        //for check contactphone duplicate
        $checkContactPhone=$this->checkEditContactPhoneDuplicate($phone, $userId);
        if ($checkContactPhone>0) {
            return ['code' => 300 ,'data'=>'','title'=>'User Update','message'=>'Contact phone already exist.'];
        }
        
        //$dob=Carbon::createFromFormat('d/m/Y', $formData['dateOfBirth'])->format('Y-m-d');
        //for user edit
        
        $userUpdate=array();
        $userUpdate['userTypeId']=$formData['userTypeId'];
        if ($formData['userTypeName']==='Participant') {
            $userUpdate['userCategoryId']=$formData['userCategoryId'];
            $userUpdate['departmentId']=$formData['departmentId'];
            $userUpdate['hiringManager']=$formData['hiringManager'];
            $userUpdate['role']=$formData['role'];
        } else {
            $userUpdate['userCategoryId']=null;
            $userUpdate['departmentId']=null;
            $userUpdate['hiringManager']=null;
            $userUpdate['role']=null;
        }
        $userUpdate['email']=$formData['email'];
        $userUpdate['orgEmail']=$formData['orgEmail'];
        // if($formData['password']!='')
        // {
        //     $userUpdate['password']=Hash::make(trim($formData['password']));
        // }
        $userUpdate['status']=$formData['status'];
        $userUpdate['updatedBy']=$updatedBy;
        $userUpdate['updated_at']=Carbon::now();

        if ($formData['isHmu'] === true) {
            $userUpdate['isHmu'] = '1';
            $userUpdate['hmuDate'] = Carbon::now();
        } elseif ($formData['isHmu'] === false) {
            $userUpdate['isHmu'] = '0';
            $userUpdate['hmuDate'] = Carbon::now();
        }

        if ($formData['isAdmin'] === true) {
            $userUpdate['isAdmin'] = '1';
            $userUpdate['adminDate'] = Carbon::now();
        } elseif ($formData['isAdmin'] === false) {
            $userUpdate['isAdmin'] = '0';
            $userUpdate['adminDate'] = Carbon::now();
        }

        User::where('userId', $userId)->update($userUpdate);
        //for user details edit
        $userDetail=UserDetail::findOrFail($userDetailId);
        $userDetail->givenName=$formData['givenName'];
        $userDetail->surName=$formData['surName'];
        $userDetail->prefferedName=$formData['prefferedName'];
        $userDetail->occupation=$formData['occupation'];
        //$userDetail['dateOfBirth']=$dob;
        $userDetail->dateOfBirth=$formData['dateOfBirth'];
        $userDetail->contactPhone=$phone;
        $userDetail->gender=$formData['gender'];
        if ($formData['gender']==='self_desc') {
            $userDetail->selfDescribe=$formData['selfDescribe'];
        } else {
            $userDetail->selfDescribe=null;
        }
        if ($formData['userTypeName']==='Participant') {
            $userDetail->levelOfContact=$formData['levelOfContact'];
            $userDetail->pacificBasedRole=$formData['pacificBaseRole'];
            if($userDetail->pacificBasedRole === 'Y'){
                $userDetail->pacificBaseCountry = $formData['pacificBaseCountry']; 
            }
            $userDetail->policeCheque=$formData['policeCheckRequire'];
            if ($formData['contactNo2'] !== '') {
                $contactphoneNo2 = $formData['contactNo2Prefix'].'|'.$formData['contactNo2'];
                $userDetail->contactPhone2= $contactphoneNo2;
            }
            $userDetail->address=$formData['address'];
            $userDetail->aboriginal=$formData['aboriginalValue'];
            $userDetail->disability=$formData['disability'];
            $userDetail->emergencyContactPrimary=$formData['emergencyPrimary'];
            $userDetail->relationshipPrimary=$formData['relationshipPrimary'];
            if ($formData['emergencyPrimaryContact'] !== '') {
                $emergencyPrimaryContact = $formData['emergencyPrimaryContactPrefix'].'|'.$formData['emergencyPrimaryContact'];
                $userDetail->contactPhonePrimary=$emergencyPrimaryContact;
            }
            $userDetail->emergencyContactSecondary=$formData['emergencySecondary'];
            $userDetail->relationshipSecondary=$formData['relationshipSecondary'];
            if ($formData['emergencySecordaryContact'] !== '') {
                $emergencySecordaryContact = $formData['emergencySecordaryContactPrefix'].'|'.$formData['emergencySecordaryContact'];
                $userDetail->contactPhoneSecondary=$emergencySecordaryContact;
            }
            $userDetail->candidateLegalWorkAustralia=$formData['legalWorkInAustralia'];
            $userDetail->residentialStatus=$formData['residentialStatus'];
            if ($formData['residentialStatus'] === 'other') {
                $userDetail->otherResidentialStatus=$formData['otherResidentialStatus'];
            }
            $userDetail->confirmInformation=$formData['confirm'];
            $userDetail->visaAttachments=$formData['visaFileVal'];
        } else {
            $userDetail->levelOfContact=null;
            $userDetail->pacificBasedRole=null;
            $userDetail->policeCheque=null;
            $userDetail->contactPhone2= null;
            $userDetail->address=null;
            $userDetail->aboriginal=null;
            $userDetail->disability=null;
            $userDetail->emergencyContactPrimary=null;
            $userDetail->relationshipPrimary=null;
            $userDetail->contactPhonePrimary=null;
            $userDetail->emergencyContactSecondary=null;
            $userDetail->relationshipSecondary=null;
            $userDetail->contactPhoneSecondary=null;
            $userDetail->candidateLegalWorkAustralia=null;
            $userDetail->residentialStatus=null;
            $userDetail->otherResidentialStatus=null;
            $userDetail->confirmInformation=null;
            $userDetail->visaAttachments=null;
        }
        
        $userDetail->updatedBy=$updatedBy;
        $userDetail->updated_at=Carbon::now();
        $userDetail->save();
        if ($userId!==0 || $userDetail!==0) {
            return ['code' => 200 ,'data'=>'','title'=>'User Update','message'=>'User Updated successfully.'];
        } else {
            return ['code' => 300 ,'data'=>'','title'=>'User Update','message'=>'User not added.'];
        }
    }
    /**
     * [deleteUserByUserId -delete user]
     * @param  [type] $userId [description]
     * @return [type]         [description]
     */
    public function deleteUserByUserId($userId)
    {
        $user = User::find($userId);
        $this->updateUserDetails($userId);
        $user->delete();
        $user->UserDetail()->delete();
        $user->bookings()->delete();
        return $userId;
    }
    /**
     * [deleteMultipleUserByUserId - delete user multiple]
     * @param  [type] $userIdArray [description]
     * @return [type]              [description]
     */
    public function deleteMultipleUserByUserId($userIdArray)
    {
        //$user = User::destroy($userIdArray);
        foreach ($userIdArray as $userId) {
            // $this->updateUserDetails($userId);
            // $userdel = User::find($userId);
            // $userdel->bookings()->delete();
            $user = User::find($userId);
            $this->updateUserDetails($userId);
            $user->delete();
            $user->UserDetail()->delete();
            $user->bookings()->delete();
        }
        return $user;
    }
    /**
     * [updateMultipleUserStatusByUserId - change status of user]
     * @param  [type] $userData [description]
     * @return [type]           [description]
     */
    public function updateMultipleUserStatusByUserId($userData)
    {
        if (isset($userData['userIds'])) {
            $userIdArr = $userData['userIds'];
        }
        $updateStatus = $userData['actionStatus'];
        // $userIdArr = array(1,2,3,4,5);
        // dd(implode(',',$userIdArr));
        $user = User::whereIn('userId', $userIdArr)->update([
            'status'=>$updateStatus
        ]);
        return $user;
    }
}
