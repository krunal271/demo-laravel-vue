<?php

namespace App\Api\Admin\Repositories;

use App\Models\AdminUser;
use App\Models\Module;
use App\User;
use Illuminate\Support\Facades\Hash;
use DB;

/**
 * Here goes the description of the class. It should explain what the main
 * purpose of this class is to admin user add,edit,delete,multiple delete
 * and get details of admin user.
*/
class AdminUserRepository
{
    /**
     * [__construct - there is no initialization method here.]
     */
    public function __construct()
    {
        //
    }
    /**
     * [getAdminList - getting admin user list with pagination and with search
     * data.]
     * @param  [integer] $noOfPage   - per page admin user.
     * @param  [array] $searchData - admin search data for filtering admin
     * user.
     * @return [array] $list   - list of admin pagination data in array
     */
    public function getAdminList($noOfPage, $searchData, $sortedColumn, $order)
    {
        if (count($searchData) > 0) {
            $searchKey = trim(strtolower($searchData['searchKey']));
            $searchText = trim(strtolower($searchData['searchText']));
            if (!empty($searchKey) && !empty($searchText)) {
                $list = AdminUser::with('usertype');

                if ($searchKey === 'usertype') {
                    $list->whereHas('userType', function ($query1) use ($searchKey, $searchText) {
                        $query1->where('type', 'LIKE', '%' . $searchText . '%');
                    });
                }

                if ($searchKey === 'firstname') {
                    $list->where('givenName', 'like', '%' . $searchText . '%');
                }

                if ($searchKey === 'lastname') {
                    $list->where('surName', 'like', '%' . $searchText . '%');
                }

                if ($searchKey === 'usetname') {
                    $list->where('usetname', 'like', '%' . $searchText . '%');
                }

                if ($searchKey === 'email') {
                    $list->where('email', 'like', '%' . $searchText . '%');
                }

                if ($searchKey === 'phone') {
                    $list->where('contactPhone', 'like', '%' . $searchText . '%');
                }

                if ($searchKey === 'status') {
                    $list->where('status', '=', $searchText);
                }

                $list = $list->where('admin_user.userTypeId', '1')->orderBy('admin_user.created_at', 'asc')->paginate($noOfPage);
            } else {
                $list=$this->getDefaultAdminData($noOfPage, $sortedColumn, $order);
            }
        } else {
            $list=$this->getDefaultAdminData($noOfPage, $sortedColumn, $order);
        }
        return $list;
    }

    public function getDefaultAdminData($noOfPage, $sortedColumn, $order)
    {
        $reportQuery = AdminUser::with(['usertype' => function ($q) use ($sortedColumn,$order) {
            if ($sortedColumn==='userType') {
                $q->orderBy('type', $order);
            }
        }]);
        $reportQuery->where('admin_user.userTypeId', '1');
        if ($sortedColumn!=='userType') {
            $reportQuery->orderBy($sortedColumn, $order);
        }
         
        $list=$reportQuery->paginate($noOfPage);
       
        return $list;
    }
    /**
     * [getDashboardData - get admin dashboard details]
     * @return [array] $dashboard - array with differant details of admin user.
     */
    public function getDashboardData()
    {
        $dashboard = array();
        // $newParticipants = User::with('userType')->with('userDetails')->with('department');
        // $newParticipants->whereHas('userType', function ($query) {
        //     $query->where('user.userTypeId', 4)->whereMonth('user.created_at', date('m'))->whereYear('user.created_at', date('Y'));
        // });

        $newParticipants = User::join('user_detail','user.userId','=','user_detail.userId')
        ->leftjoin('department','department.departmentId','=','user.departmentId')
        ->whereMonth('user.created_at', date('m'))->whereYear('user.created_at', date('Y'))
        ->select('user.userId', 'user_detail.givenName','department.name','user.created_at','user.status')
        ->orderBy('user.created_at', 'desc')
        ->getQuery(); 
        
        $allParticipants = User::with('userType')->with('userDetails')->with('department');
        $allParticipants->whereHas('userType', function ($query) {
            $query->where('user.userTypeId', 4)->whereYear('user.created_at', date('Y'));
        });
        $hmu = User::with('userType')->with('userDetails')->with('department');
        $hmu->whereHas('userType', function ($query) {
            $query->where('user.userTypeId', 3)->whereYear('user.created_at', date('Y'));
        });
        $modules = Module::all();
        $dashboard['new_participant'] = $newParticipants->get()->toArray();
        $dashboard['all_participant'] = $allParticipants->get()->toArray();
        $dashboard['hmu'] = $hmu->get()->toArray();
        $dashboard['modules'] = $modules->take(10)->toArray();
        return $dashboard;
    }

    public function getModuleDashboardData($data) 
    {
        $modules = Module::orderBy($data['moduleSortedColumn'],$data['moduleOrder'])->limit(10);
        $dashboard['modules'] = $modules->get()->toArray();
        return $dashboard;
    }

    public function getParticipantDashboardData($data)
    {
        $newParticipants = User::join('user_detail','user.userId','=','user_detail.userId')
        ->leftjoin('department','department.departmentId','=','user.departmentId')
        ->whereMonth('user.created_at', date('m'))->whereYear('user.created_at', date('Y'))
        ->select('user.userId', 'user_detail.givenName','department.name','user.created_at','user.status')
        ->orderBy($data['participantSortedColumn'],$data['participantOrder'])
        ->getQuery(); 
        $dashboard['new_participant'] = $newParticipants->get()->toArray();
        return $dashboard;
        // $newParticipants = User::select('user.*','user_detail.givenName')->join('user_detail','user_detail.userId','=','user.userId')->orderBy($data['participantSortedColumn'],$data['participantOrder']);
        // $dashboard['new_participant'] = $newParticipants->get()->toArray();
        // return $dashboard;
    }
    /**
     * [checkAdminEmail - count email id is already exist or not]
     * @param  [string] $email - email id value of admin user
     * @return [integer]  $mail - count email values
     */
    public function checkAdminEmail($email)
    {
        $mail = AdminUser::where('email', $email)->get()->count();
        return $mail;
    }
    /**
     * [addAdmin - add admin user by form data]
     * @param [array] $adminData - form data to add admin user.
     * @return [boolean] $adminStatus - admin add then status is 0 else 1
     */
    public function addAdmin($adminData)
    {
        $contactNo = '';
        if ($adminData['contactPhone'] != '') {
            $contactNo = $adminData['contactPhonePrefix'].'|'.$adminData['contactPhone'];
        }
        $admin = new AdminUser;
        $admin->userTypeId = $adminData['userTypeId'];
        $admin->givenName = $adminData['givenName'];
        $admin->surName = $adminData['surName'];
        $admin->username = $adminData['username'];
        $admin->contactPhone = $contactNo;
        $admin->email = $adminData['email'];

        if ($adminData['password'] !== '') {
            $admin->password = Hash::make(trim($adminData['password']));
        }

        $admin->status = $adminData['status'];
        $adminStatus = $admin->save();
        return $adminStatus;
    }
    /**
     * [deleteAdmin - delete admin user by admin id]
     * @param  [integer] $adminId - admin id for delete
     * @return [boolean]   $adminStatus - if delete then status 1 else 0
     */
    public function deleteAdmin($adminId)
    {
        $adminStatus = AdminUser::destroy($adminId);
        return $adminStatus;
    }
    /**
     * [deleteMultipleAdminById - delete multiple admin by admin id]
     * @param  [array] $adminIdArray - multiple admin is in array
     * @return [boolean]  $admin - if delete then status 1 else 0
     */
    public function deleteMultipleAdminById($adminIdArray)
    {
        $admin = AdminUser::where('adminUserId', '>', '1')->delete($adminIdArray);
        return $admin;
    }
    /**
     * [getAdminById - get admin details by admin id]
     * @param  [integer] $data - admin id in data
     * @return [array]  $adminData - array of admin details
     */
    public function getAdminById($data)
    {
        $adminData = AdminUser::find($data['adminId']);
        return $adminData;
    }
    /**
     * [editAdminById - edit admin by form data]
     * @param  [array] $adminData - form data
     * @return [boolean]  $adminStatus - if updated then status 1 else 0
     */
    public function editAdminById($adminData)
    {
        $contactNo = '';
        if ($adminData['contactPhone'] != '') {
            $contactNo =  $adminData['contactPhonePrefix'].'|'.$adminData['contactPhone'];
        }
        $admin = array(
            'email' => $adminData['email'],
            'givenName' => $adminData['givenName'],
            'surName' => $adminData['surName'],
            'contactPhone' => $contactNo,
            'status' => $adminData['status'],
        );
        $adminStatus = AdminUser::where('AdminUserId', $adminData['AdminUserId'])->update($admin);
        return $adminStatus;
    }
    /**
     * [updateMultipleAdminStatusByAdminId - update multiple status active or
     * inactive]
     * @param  [array] $adminData - admin ids for changing status
     * @return [boolean]  $module  -   if updated then status 1 else 0
     */
    public function updateMultipleAdminStatusByAdminId($adminData)
    {
        if (isset($adminData['adminIds'])) {
            $adminIdArr = $adminData['adminIds'];
        }
        $updateStatus = $adminData['actionStatus'];
        $module = AdminUser::whereIn('AdminUserId', $adminIdArr)->update([
            'status' => $updateStatus,
        ]);
        return $module;
    }
}
