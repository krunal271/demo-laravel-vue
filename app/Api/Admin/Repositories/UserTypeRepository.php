<?php

namespace App\Api\Admin\Repositories;

use Carbon\Carbon;
use DB;
use App\User;
use App\Models\UserType;
use App\Models\Department;
use App\Models\UserCategory;
use Excel;
use File;

/**
 * Here goes the description of the class. It should explain what the main
 * purpose of this class is to CRUD of user types like admin,hmu or
 * participant etc.
*/
class UserTypeRepository
{
    /**
     * [__construct nothing to initialize]
     */
    public function __construct()
    {
        //
    }
    /**
     * [getUserTypeSelectList - gettting list of user type for select box.]
     * @return [array] $list - array return with user type list.
     */
    public function getUserTypeSelectList()
    {
        $list = UserType::orderBy('created_at', 'asc')->where('status', 'active')->where('type', '!=', 'SuperAdmin')->get();
        return $list;
    }
    /**
     * [getUserTypes - get user types list with pagination]
     * @param  [integer] $noOfPage   - no of record per page
     * @param  [array] $searchData - seatch user type data
     * @return [array] $list - user type array with pagination
     */
    public function getUserTypes($noOfPage, $searchData, $sortData)
    {
        if (!empty($searchData)) {
            $searchKey = trim(strtolower($searchData['searchKey']));
            $searchText = trim(strtolower($searchData['searchText']));
            if (!empty($searchKey) && !empty($searchText)) {
                if ($searchKey === 'type') {
                    $list = UserType::where('type', 'like', '%'.$searchText.'%')->where('userTypeId', '>', '1')->orderBy($sortData['sortedColumn'], $sortData['order'])->paginate($noOfPage);
                } elseif ($searchKey === 'status') {
                    $list = UserType::where('status', '=', $searchText)->where('userTypeId', '>', '1')->orderBy($sortData['sortedColumn'], $sortData['order'])->paginate($noOfPage);
                }
            } else {
                $list = UserType::where('userTypeId', '>', '1')->orderBy($sortData['sortedColumn'], $sortData['order'])->paginate($noOfPage);
            }
        } else {
            $list = UserType::where('userTypeId', '>', '1')->orderBy($sortData['sortedColumn'], $sortData['order'])->paginate($noOfPage);
        }


        return $list;
    }
    /**
     * [addUserType - add user type by form data]
     * @param [array] $userType - form data
     * @return [boolean] - true or false
     */
    public function addUserType($userType)
    {
        $type = new UserType;
        $type->type = $userType['userType'];
        $type->status = $userType['userTypeStatus'];

        if ($type->save()) {
            return true;
        } else {
            return false;
        }
    }
    /**
     * [removeUserType - remove user type by user id]
     * @param  [integer] $userTypeId - id of user type
     * @return [boolean] - return true or false
     */
    public function removeUserType($userTypeId)
    {
        $depCount = Department::where('userTypeId', $userTypeId)->count();
        $ucCount = UserCategory::where('userTypeId', $userTypeId)->count();
        $uCount = User::where('userTypeId', $userTypeId)->count();
        
        if (($depCount > 0) || ($ucCount > 0) || ($uCount > 0)) {
            return 2;
        } else {
            $typeStatus = UserType::destroy($userTypeId);
            if ($typeStatus===1) {
                return true;
            } else {
                return false;
            }
        }
    }
    /**
     * [getUserTypeById - get user type details by id]
     * @param  [type] $userTypeId - user type id
     * @return [boolean] - true or false
     */
    public function getUserTypeById($userTypeId)
    {
        $userType = UserType::find($userTypeId);
        if (!empty($userType)) {
            return $userType;
        } else {
            return false;
        }
    }
    /**
     * [editUserType - edit user by form data]
     * @param  [type] $userTypeData - form data
     * @return [boolean]  $typeStatus
     */
    public function editUserType($userTypeData)
    {
        $userType['type'] = $userTypeData['type'];
        $userType['status'] = $userTypeData['status'];
        $typeStatus = UserType::where('userTypeId', $userTypeData['id'])->update($userType);
        return $typeStatus;
    }
    /**
     * [deleteMultipleUserTypeById - delet multiple user types by multiple
     * user type ids]
     * @param  [array] $userTypeIdArray
     * @return [boolean]  $usertype
     */
    public function deleteMultipleUserTypeById($userTypeIdArray)
    {
        foreach ($userTypeIdArray as $userTypeId) {
            $userTypeId;
            $depCount = Department::where('userTypeId', $userTypeId)->count();
            $ucCount = UserCategory::where('userTypeId', $userTypeId)->count();
            $uCount = User::where('userTypeId', $userTypeId)->count();
            
            if (($depCount === 0) && ($ucCount === 0) && ($uCount === 0)) {
                $usertype = UserType::destroy($userTypeId);
            } else {
                $usertype = 1;
            }
        }
        return $usertype;
    }
    /**
     * [updateMultipleUserTypeStatusById - update multiple status of user]
     * @param  [array] $userTypeData - user type ids
     * @return [boolean] $user
     */
    public function updateMultipleUserTypeStatusById($userTypeData)
    {
        if (isset($userTypeData['userTypeIds'])) {
            $userTypeIdArr = $userTypeData['userTypeIds'];
        }
        $updateStatus = $userTypeData['actionStatus'];
        $user = UserType::whereIn('userTypeId', $userTypeIdArr)->update([
            'status'=>$updateStatus
        ]);
        return $user;
    }
}
