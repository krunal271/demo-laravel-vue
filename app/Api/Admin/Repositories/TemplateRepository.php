<?php

namespace App\Api\Admin\Repositories;

use Carbon\Carbon;
use DB;
use App\Models\Template;
use Excel;
use File;

/**
 * Here goes the description of the class. It should explain what the main
 * purpose of this class is to CRUD of module
*/
 class TemplateRepository
 {
    
    /**
     * [__construct]
     */
     public function __construct()
     {
         //
     }
     /**
      * [getTemplateList - getting template list with pagination]
      * @param  [type] $noOfPage   [description]
      * @param  [type] $searchData [description]
      * @return [type]             [description]
      */
     public function getTemplateList($noOfPage, $searchData)
     {
         if (!empty($searchData)) {
             $searchKey = trim(strtolower($searchData['searchKey']));
             $searchText = trim(strtolower($searchData['searchText']));
             if (!empty($searchKey) && !empty($searchText)) {
                 if ($searchKey == 'name') {
                     $list = Template::where('name', 'like', '%'.$searchText.'%')->paginate($noOfPage);
                 } elseif ($searchKey == 'status') {
                     $list = Template::where('status', '=', $searchText)->paginate($noOfPage);
                 } elseif ($searchKey == 'type') {
                     $list = Template::where('type', 'like', '%' .$searchText.'%')->paginate($noOfPage);
                 }
             } else {
                 $list = Template::orderBy('created_at', 'asc')->paginate($noOfPage);
             }
         } else {
             $list = Template::orderBy('created_at', 'asc')->paginate($noOfPage);
         }


         return $list;
     }
     /**
      * [addTemplate - add template]
      * @param [type] $templateData [description]
      */
     public function addTemplate($templateData)
     {
         $template = new Template;
      
         $template->name = $templateData['name'];
         $template->type = $templateData['type'];
         $template->subject = $templateData['subject'];
         $template->status = $templateData['status'];
         $template->template = $templateData['template'];

         $templateStatus = $template->save();

         return $templateStatus;
     }
     /**
      * [deleteTemplate - delete template]
      * @param  [type] $templateId [description]
      * @return [type]             [description]
      */
     public function deleteTemplate($templateId)
     {
         $templateStatus = Template::destroy($templateId);
         return $templateStatus;
     }
     /**
      * [deleteMultipleTemplateById description]
      * @param  [type] $moduleIdArray [description]
      * @return [type]                [description]
      */
     public function deleteMultipleTemplateById($moduleIdArray)
     {
         $module = Template::destroy($moduleIdArray);
         return $module;
     }
     /**
      * [getTemplateById description]
      * @param  [type] $data [description]
      * @return [type]       [description]
      */
     public function getTemplateById($data)
     {
         $data = Template::select('emailTemplateId', 'name', 'type', 'status', 'subject', 'template')->find($data['templateId']);
         return $data;
     }
     /**
      * [editTemplateById description]
      * @param  [type] $templateData [description]
      * @return [type]               [description]
      */
     public function editTemplateById($templateData)
     {
         //print_r($templateData);die;
         $template = array('name' => $templateData['name'],'type' => $templateData['type'],'subject' => $templateData['subject'], 'status' => $templateData['status'], 'template' => $templateData['template']);

         $templateStatus = Template::where('emailTemplateId', $templateData['emailTemplateId'])->update($template);

         return $templateStatus;
     }
     /**
      * [updateMultipleTemplateStatusByTemplateId description]
      * @param  [type] $templateData [description]
      * @return [type]               [description]
      */
     public function updateMultipleTemplateStatusByTemplateId($templateData)
     {
         if (isset($templateData['templateIds'])) {
             $templateIdArr = $templateData['templateIds'];
         }
         $updateStatus = $templateData['actionStatus'];
         // $userIdArr = array(1,2,3,4,5);
         // dd(implode(',',$userIdArr));
         $template = Template::whereIn('emailTemplateId', $templateIdArr)->update([
            'status'=>$updateStatus
        ]);
         return $template;
     }
 }
