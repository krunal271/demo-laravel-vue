<?php

namespace App\Api\Admin\Repositories;

use App\Models\Module;
use Illuminate\Support\Facades\Storage;
use Carbon\Carbon;

/**
 * Here goes the description of the class. It should explain what the main
 * purpose of this class is to CRUD of module
*/
class ModuleRepository
{
    /**
     * [__construct]
     */
    public function __construct()
    {
        //
    }
    /**
     * [getDepartmentList - get module list by search data and pagination]
     * @param  [type]  [description]
     * @return [type]             [description]
     */
    public function getModuleList($noOfPage, $searchData, $sortData)
    {
        if (!empty($searchData)) {
            $searchKey = trim(strtolower($searchData['searchKey']));
            $searchText = trim(strtolower($searchData['searchText']));
            if (!empty($searchKey) && !empty($searchText)) {
                if ($searchKey ==='modulename') {
                    $list = Module::where('moduleName', 'like', '%' . $searchText . '%');
                } elseif ($searchKey ==='passingscore') {
                    $list = Module::where('passingscore', '=', $searchText);
                } elseif ($searchKey ==='attempts') {
                    $list = Module::where('allowTotalAttempt', '=', $searchText);
                } elseif ($searchKey ==='status') {
                    $list = Module::where('status', '=', $searchText);
                }
                $list = $list->orderBy($sortData['sortedColumn'], $sortData['order'])->paginate($noOfPage);
            } else {
                $list = Module::orderBy($sortData['sortedColumn'], $sortData['order'])->paginate($noOfPage);
            }
        } else {
            $list = Module::orderBy($sortData['sortedColumn'], $sortData['order'])->paginate($noOfPage);
        }

        return $list;
    }
    /**
     * [addModule - add module]
     * @param [type] $requestData [description]
     */
    public function addModule($requestData)
    {
        $moduleData = $requestData;
        $fileData = $requestData->get('image');

        if ($fileData !== '') {
            $fileName = 'modules/image_' . time() . '.' . explode('/', explode(':', substr($fileData, 0, strpos($fileData, ';')))[1])[1]; //generating unique file name;

            @list($type, $fileData) = explode(';', $fileData);
            @list(, $fileData) = explode(',', $fileData);


            Storage::disk('public')->put($fileName, base64_decode($fileData));
        } else {
            $fileName = '';
        }
        
        $module = new Module;
        $moduleDate = $moduleData['moduleDate'];
        //$module->moduleDate =  $moduleDate;
        //echo $module->moduleDate; die;
        $module->moduleName = $moduleData['moduleName'];
        $module->hiddenName = strtolower(str_replace(' ', '-', $moduleData['moduleName']));
        $module->moduleDate = Carbon::createFromFormat('d/m/Y', $moduleData['moduleDate']);
        $module->allowTotalAttempt = $moduleData['allowTotalAttempt'];
        $module->image = $fileName;
        $module->description = $moduleData['description'];
        $module->status = $moduleData['status'];
        //print_r($module);die;
        $mdStatus = $module->save();

        return $mdStatus;
    }
    /**
     * [deleteModule - delete module]
     * @param  [type] $moduleId [description]
     * @return [type]           [description]
     */
    public function deleteModule($moduleId)
    {
        $moduleStatus = Module::destroy($moduleId);
        return $moduleStatus;
    }
    /**
     * [deleteMultipleModuleById - delete multiple module]
     * @param  [type] $moduleIdArray [description]
     * @return [type]                [description]
     */
    public function deleteMultipleModuleById($moduleIdArray)
    {
        $module = Module::destroy($moduleIdArray);
        return $module;
    }
    /**
     * [getModuleById - get module details by module id]
     * @param  [type] $data [description]
     * @return [type]       [description]
     */
    public function getModuleById($data)
    {
        $data = Module::select('moduleId', 'moduleName', 'passingScore', 'moduleDate', 'allowTotalAttempt', 'image', 'description', 'status')->find($data['moduleId']);
        return $data;
    }
    /**
     * [editModuleById - edit module details by module id]
     * @param  [type] $moduleData [description]
     * @return [type]             [description]
     */
    public function editModuleById($moduleData)
    {
        $fileData = $moduleData['image'];
        if (!empty($moduleData['moduleDate'])) {
            $moduleDate = Carbon::createFromFormat('d/m/Y', $moduleData['moduleDate'])->format('Y-m-d');
        }

        $module = array('allowTotalAttempt' => $moduleData['allowTotalAttempt'], 'description' => $moduleData['description'], 'moduleName' => $moduleData['moduleName'], 'moduleDate' => $moduleDate, 'status' => $moduleData['status']);

        if ($fileData !== null && $fileData !=='') {
            $fileName = 'modules/image_' . time() . '.' . explode('/', explode(':', substr($fileData, 0, strpos($fileData, ';')))[1])[1];
            //generating unique file name;
            
            @list($type, $fileData) = explode(';', $fileData);
            @list(, $fileData) = explode(',', $fileData);
            Storage::disk('public')->put($fileName, base64_decode($fileData));
            $module['image'] = $fileName;
            $image = Module::where('moduleId', $moduleData['moduleId'])->first()->toArray();
            Storage::delete('/public/' . $image['image']);
        }

        $moduleStatus = Module::where('moduleId', $moduleData['moduleId'])->update($module);

        return $moduleStatus;
    }
    /**
     * [updateMultipleModuleStatusByModuleId update module status by module id]
     * @param  [type] $moduleData [description]
     * @return [type]             [description]
     */
    public function updateMultipleModuleStatusByModuleId($moduleData)
    {
        if (isset($moduleData['moduleIds'])) {
            $moduleIdArr = $moduleData['moduleIds'];
        }
        $updateStatus = $moduleData['actionStatus'];
        // $userIdArr = array(1,2,3,4,5);
        // dd(implode(',',$userIdArr));
        $module = Module::whereIn('moduleId', $moduleIdArr)->update([
            'status' => $updateStatus,
        ]);
        return $module;
    }
}
