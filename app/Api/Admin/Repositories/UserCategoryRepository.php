<?php

namespace App\Api\Admin\Repositories;

use Carbon\Carbon;
use DB;
use App\Models\UserCategory;
use Excel;
use File;
use App\User;

/**
 * Here goes the description of the class. It should explain what the main
 * purpose of this class is to CRUD of user category
*/
class UserCategoryRepository
{
    /**
     * [__construct]
     */
    public function __construct()
    {
        //
    }
    /**
     * [getUserCategorySelectListByUserTypeId - get category list for select
     * box]
     * @param  [type] $userTypeId [description]
     * @return [type]             [description]
     */
    public function getUserCategorySelectListByUserTypeId($userTypeId)
    {
        $list= UserCategory::orderBy('created_at', 'asc')->where('userTypeId', $userTypeId)->where('status', 'active')->get();
        return $list;
    }
    /**
     * [getUserCategoryList - get category list by pagination]
     * @param  [type] $noOfPage   [description]
     * @param  [type] $searchData [description]
     * @return [type]             [description]
     */
    public function getUserCategoryList($noOfPage, $searchData, $sortedColumn, $order)
    {
        $ucList = UserCategory::select('user_category.*','user_type.type as usertype')->with('userTypeCategory');

        $searchKey = trim(strtolower($searchData['searchKey']));
        $searchText = trim(strtolower($searchData['searchText']));
        $ucList->join('user_type', 'user_type.userTypeId', '=', 'user_category.userTypeId');
        if ($searchKey == 'usertype') {
            $ucList->where('user_type.type', 'like', '%'.$searchText.'%');
        }
        if ($searchKey == 'catgory') {
            $ucList->where('user_category.type', 'like', '%'.$searchText.'%');
        }
        if ($searchKey == 'status') {
            $ucList->where('user_category.status', '=', $searchText);
        }

        if ($sortedColumn == 'userType') {
            $ucList->orderBy('user_type.type',$order);
        } 
        if ($sortedColumn == 'status') {
            $ucList->orderBy('user_category.status',$order);
        } 
        if ($sortedColumn == 'type') {
            $ucList->orderBy('user_category.type',$order);
        }            
        //dd($ucList->toSql());
        $list=$ucList->paginate($noOfPage);
        return $list;
    }
    /**
     * [addUserCategory - add user category]
     * @param [type] $userCategoryData [description]
     */
    public function addUserCategory($userCategoryData)
    {
        $userCategory = new UserCategory;
      
        $userCategory->type = $userCategoryData['type'];
        $userCategory->userTypeId = $userCategoryData['userTypeId'];
        $userCategory->status = $userCategoryData['status'];

        $ucStatus = $userCategory->save();

        return $ucStatus;
    }
    /**
     * [deleteUserCategory - delete user category by id ]
     * @param  [type] $userCategoryId [description]
     * @return [type]                 [description]
     */
    public function deleteUserCategory($userCategoryId)
    {
        $ucCount = User::where(['userCategoryId'=> $userCategoryId])->count();
        if ($ucCount > 0) {
            return 2;
        } else {
            $userCategoryStatus = UserCategory::destroy($userCategoryId);
        }
        
        return $userCategoryStatus;
    }
    /**
     * [deleteMultipleUserCategoryById - delete multiple user by category id]
     * @param  [type] $userCategoryIdArray [description]
     * @return [type]                      [description]
     */
    public function deleteMultipleUserCategoryById($userCategoryIdArray)
    {
        foreach ($userCategoryIdArray as $userCategoryId) {
            $ucCount = User::where(['userCategoryId'=> $userCategoryId])->count();
            if ($ucCount == 0) {
                $user = UserCategory::destroy($userCategoryId);
            }
        }
        return 2;
    }
    /**
     * [getUserCategoryById - get user category]
     * @param  [type] $data [description]
     * @return [type]       [description]
     */
    public function getUserCategoryById($data)
    {
        $data = UserCategory::select('userTypeId', 'userCategoryId', 'type', 'status')->find($data['userCategoryId']);
        return $data;
    }
    /**
     * [editUserCategoryById - edit user category by user id]
     * @param  [type] $userCategoryData [description]
     * @return [type]                   [description]
     */
    public function editUserCategoryById($userCategoryData)
    {
        $userCategory = array('userTypeId'=> $userCategoryData['userTypeId'], 'type'=> $userCategoryData['type'], 'status'=> $userCategoryData['status']);

        $userCategoryStatus = UserCategory::where('userCategoryId', $userCategoryData['userCategoryId'])->update($userCategory);

        return $userCategoryStatus;
    }
    /**
     * [updateMultipleUserCategoryStatusByUserCategoryId - update multiple
     * status ]
     * @param  [type] $userCategoryData [description]
     * @return [type]                   [description]
     */
    public function updateMultipleUserCategoryStatusByUserCategoryId($userCategoryData)
    {
        if (isset($userCategoryData['userCategoryIds'])) {
            $userCategoryIdArr = $userCategoryData['userCategoryIds'];
        }
        $updateStatus = $userCategoryData['actionStatus'];
        // $userIdArr = array(1,2,3,4,5);
        // dd(implode(',',$userIdArr));
        $user = UserCategory::whereIn('userCategoryId', $userCategoryIdArr)->update([
            'status'=>$updateStatus
        ]);
        return $user;
    }
}
