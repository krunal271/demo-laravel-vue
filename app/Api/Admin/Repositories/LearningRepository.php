<?php

namespace App\Api\Admin\Repositories;

use Carbon\Carbon;
use App\User;
use App\Models\Go1User;
use App\Models\Go1Collection;
use App\Models\UserEnrollment;
use App\Models\LanguageCode;

/**
 * Get learning contents
*/
class LearningRepository
{
    
    private $accessToken;
    /**
     * [__construct to initialize]
     */
    public function __construct()
    {
        $this->accessToken = session('admin_token');
    }

    /**
     * [getLearningContents - get learning contents from Go1]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function getLearningContents($page,$keyword,$tags,$languages,$providers,$contents)
    {
        $url = 'https://api.go1.com/v2/learning-objects?offset='.$page.'&limit=16';
        if (!empty($keyword)) {
            $url .= '&keyword='.$keyword;
        }
        
        if ($tags !== NULL) {
            $tag_url = '';
            foreach ($tags as $value) {
                $tag_url .= "&tags[]=".$value;
            }
            $url .= $tag_url;
        }

        if ($languages !== NULL) {
            $languages_url = '';
            foreach ($languages as $value) {
                $languages_url .= "&language[]=".$value;
            }
            $url .= $languages_url;
        }

        if ($providers !== NULL) {
            $providers_url = '';
            foreach ($providers as $value) {
                $providers_url .= "&provider[]=".$value;
            }
            $url .= $providers_url;
        }

        if ($contents == 1) {
            $url .= $contents == 1 ? '&collection=default' : '';
        }

        //echo $url;die;
        $headers = ['Accept'=> 'application/json', 'Content-Type'=> 'application/json', 'Authorization'=> $this->accessToken];
        $client = new \GuzzleHttp\Client();
        $response = $client->request('GET', $url, ['headers'=> $headers]);
        if ($response->getStatusCode() === 200){
            return $response->getBody()->getContents();
        } else {
            return false;
        }
    }

    /**
     * [getLearningObjectDetails - get learning content details from Go1]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function getLearningObjectDetails($learningObjectId)
    {
        $client = new \GuzzleHttp\Client();

        $headers = ['Accept'=> 'application/json', 'Content-Type'=> 'application/json', 'Authorization'=> $this->accessToken];
        $response = $client->request('GET', 'https://api.go1.com/v2/learning-objects/'.$learningObjectId, 
            ['headers'=> $headers]);

        if ($response->getStatusCode() === 200) {
            return $response->getBody()->getContents();
        } else {
            return false;
        }   
    }

    /**
     * [getDefaultCollection - get all courses from existing collection]
     * @param  [description]
     * @return [type]           [description]
     */
    public function getDefaultCollection()
    {
        $client = new \GuzzleHttp\Client();
        $headers = ['Accept'=> 'application/json', 'Content-Type'=> 'application/json', 'Authorization'=> $this->accessToken];
        $response = $client->request('GET', 'https://api.go1.com/v2/learning-objects?collection=default', ['headers'=> $headers]);
        if ($response->getStatusCode() === 200) {
            return $response->getBody()->getContents();
        } else {
            return false;
        }   
    }

    /**
     * [addToDefaultCollection - add course to default collection]
     * @param  $loId [description]
     * @return [type]           [description]
     */
    public function addToDefaultCollection($loId)
    {
        $client = new \GuzzleHttp\Client();
        $headers = ['Accept'=> 'application/json', 'Content-Type'=> 'application/json', 'Authorization'=> $this->accessToken];
        $collection = $this->getLearningObjectDetails($loId);  
        $collectionDetails = json_decode($collection);
        $collectionData = ['loId'=>$collectionDetails->id, 'title'=>$collectionDetails->title, 'image'=>$collectionDetails->image, 'type'=>$collectionDetails->type];
        $res = $client->request('POST','https://api.go1.com/v2/collections/default/items/add',
                            [ 
                                'headers'=> $headers,
                                'json'=> ["lo" => [$loId]]
                            ]
        );                
        if ($res->getStatusCode() === 204) {
            $collectionStatus = Go1Collection::firstOrCreate(['loId'=>$collectionDetails->id],$collectionData);
            $this->createEnrollment($loId);
            return true;
        } else {
            return false;
        }   
    }

    /**
     * [createEnrollment - enroll user to course]
     * @param  $loId [description]
     * @return [type]           [description]
     */
    public function createEnrollment($loId)
    {
        $client = new \GuzzleHttp\Client();
        $go1Users = array();
        $go1Users = Go1User::all()->toArray();
        $headers = ['Accept'=> 'application/json', 'Content-Type'=> 'application/json', 'Authorization'=> $this->accessToken];
        if (count($go1Users) > 0) {
            foreach ($go1Users as $go1User) {
                try {
                    $res = $client->request('POST','https://api.go1.com/v2/enrollments',
                                            [
                                                'headers'=> $headers,
                                                'json'=> ["lo_id"=> $loId, "parent_lo_id"=> 0,"parent_enrollment_id"=> 0, "user_id"=> $go1User['go1UserId']]
                                            ]
                                        );
                    if ($res->getStatusCode() === 201) {                                            
                        $enrollDetails = json_decode($res->getBody()->getContents());
                        UserEnrollment::create(['enrollmentId'=> $enrollDetails->id, 'go1UserId'=> $go1User['go1UserId'], 'loId'=>$enrollDetails->lo_id, 'parentLoId'=>$enrollDetails->parent_lo_id, 'parentEnrollmentId'=>$enrollDetails->parent_enrollment_id, 'status'=>$enrollDetails->status]);
                    }                        
                } catch(Exception $e) {
                }
            }
        }
    }

    /**
     * [getFilterList - get filter contents from Go1 to filter learning contents]
     * @param  [description]
     * @return [type]           [description]
     */
    public function getFilterList()
    {
        $client = new \GuzzleHttp\Client();
        $headers = ['Accept'=> 'application/json', 'Content-Type'=> 'application/json', 'Authorization'=> $this->accessToken];
        $response = $client->request('GET','https://api.go1.com/v2/learning-objects?offset=0&limit=0',
                            [ 
                                'headers'=> $headers,
                            ]
        );                
        $languages = LanguageCode::all()->toArray();
        if ($response->getStatusCode() === 200) {
            return ['languages'=>$languages, 'providers'=>json_decode($response->getBody()->getContents())];
        } else {
            return false;
        }
    }
}
