<?php

namespace App\Api\Admin\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use App\Models\UserCategory;
use App\Api\Admin\Repositories\UserCategoryRepository;
use Validator;
use DB;
use Carbon\Carbon;

/**
 * Here goes the description of the class. It should explain what the main
 * purpose of this class is to CRUD of user category
*/
class UserCategoryController extends Controller
{
    /**
     * [__construct - initialize user category repository object]
     */
    public function __construct()
    {
        $this->userCatObj = new UserCategoryRepository();
    }
    /**
     * [getUserCategorySelectListByUserTypeId - get user category list for
     * slelect box]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function getUserCategorySelectListByUserTypeId(Request $request)
    {
        $userTypeId = $request->userTypeId;
        $userCatList=$this->userCatObj->getUserCategorySelectListByUserTypeId($userTypeId);
        
        if (!empty($userCatList)) {
            return ['code' => 200 ,'data'=>$userCatList,'title'=>'User Category','message'=>'Getting case type successfully.'];
        } else {
            return ['code'=> 300 ,'data'=>'','title'=>'User Category','message'=>'Something went wrong'];
        }
    }
    /**
     * [getUserCategoryList - get user category list by pagination]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function getUserCategoryList(Request $request)
    {
        $noOfPage = $request->noOfpage;
        $searchData = $request->searchData;
        $sortedColumn = $request->sortedColumn;
        $order = $request->order;
        $moduleList = $this->userCatObj->getUserCategoryList($noOfPage, $searchData, $sortedColumn, $order);

        if (!empty($moduleList)) {
            return ['code' => 200 ,'data'=>$moduleList,'title'=>'User Category','message'=>'Getting case type successfully.'];
        } else {
            return ['code'=> 300 ,'data'=>'','title'=>'User Category','message'=>'Something went wrong'];
        }
    }
    /**
     * [addUserCategory - add user category]
     * @param Request $request [description]
     */
    public function addUserCategory(Request $request)
    {
        $validator = Validator::make($request->all(), [
                        'type'=>'required|min:3|max:55',
                        'status'=>'required',
                        'userTypeId'=> 'required|numeric'
                    ]);
        if ($validator->fails()) {
            return ['code' => 300 ,'data'=>$validator->errors()->messages(),'title'=>'','message'=>'Please verify all fields'];
        } else {
            $userCategoryStatus = $this->userCatObj->addUserCategory($request->all());
            if ($userCategoryStatus===true) {
                return ['code' => 200 ,'data'=>$userCategoryStatus,'title'=>'User Category','message'=>'User Category added successfully.'];
            } else {
                return ['code'=> 300 ,'data'=>'','title'=>'User Category','message'=>'Something went wrong'];
            }
        }
    }
    /**
     * [deleteUserCategory - delete user category by id]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function deleteUserCategory(Request $request)
    {
        $userCategoryStatus = $this->userCatObj->deleteUserCategory($request->input('userCategoryId'));

        if ($userCategoryStatus===1) {
            if ($userCategoryStatus == 2) {
                return ['code' => 300 ,'data'=>$userCategoryStatus,'title'=>'User Category','message'=>'User Category associated with user.'];
            } else {
                return ['code' => 200 ,'data'=>$userCategoryStatus,'title'=>'User Category','message'=>'User Category removed successfully.'];
            }
        } else {
            return ['code'=> 300 ,'data'=>'','title'=>'User Category','message'=>'Something went wrong'];
        }
    }
    /**
     * [deleteMultipleUserCategoryById - delete multiple user category by id]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function deleteMultipleUserCategoryById(Request $request)
    {
        $userCategoryIdArray = $request->userCategoryId;
        $deleteUserCategory = $this->userCatObj->deleteMultipleUserCategoryById($userCategoryIdArray);

        if ($deleteUserCategory===2) {
            return ['code' => 200 ,'data'=>$deleteUserCategory,'title'=>'User Category','message'=>'User Category not assoicted with any user successfully deleted.'];
        } else {
            return ['code'=> 300 ,'data'=>'','title'=>'User Category','message'=>'Something went wrong'];
        }
    }
    /**
     * [getUserCategoryById - get user category id details by id]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function getUserCategoryById(Request $request)
    {
        $userCategoryData = $this->userCatObj->getUserCategoryById($request->all());

        if (!empty($userCategoryData)) {
            return ['code' => 200 ,'data'=>$userCategoryData,'title'=>'User Category','message'=>'User CategoryUser Category details'];
        } else {
            return ['code'=> 300 ,'data'=>'','title'=>'User Category','message'=>'Something went wrong'];
        }
    }
    /**
     * [editUserCategoryById - edit user category by id]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function editUserCategoryById(Request $request)
    {
        $validator = Validator::make($request->input('userCategoryData'), [
                        'type'=>'required|min:3|max:55',
                        'status'=>'required',
                        'userTypeId'=> 'required|numeric'
                    ]);
        if ($validator->fails()) {
            return ['code' => 300 ,'data'=>$validator->errors()->messages(),'title'=>'','message'=>'Please verify all fields'];
        } else {
            $userCategoryData = $this->userCatObj->editUserCategoryById($request->input('userCategoryData'));
            if ($userCategoryData===1) {
                return ['code' => 200 ,'data'=>$userCategoryData,'title'=>'User Category','message'=>'User Category details updated successfully'];
            } else {
                return ['code'=> 300 ,'data'=>'','title'=>'User Category','message'=>'Something went wrong'];
            }
        }
    }
    /**
     * [updateMultipleUserCategoryStatusByUserCategoryId - update multiple
     * status by id]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function updateMultipleUserCategoryStatusByUserCategoryId(Request $request)
    {
        $userCategoryData = $request->toArray()['userCategoryId'];
        $activeUserCategory = $this->userCatObj->updateMultipleUserCategoryStatusByUserCategoryId($userCategoryData);
        if ($activeUserCategory!==0) {
            return ['code' => 200 ,'data'=>$activeUserCategory,'title'=>'Update user category status','message'=>'User category status udpated successfully.'];
        } else {
            return ['code'=> 300 ,'data'=>'','title'=>'Update user category status','message'=>'Something went wrong'];
        }
    }
}
