<?php

namespace App\Api\Admin\Controllers;

use Illuminate\Http\Request;
use App\Models\Go1Client;
use App\Api\Admin\Repositories\LearningRepository;

/**
 * Purpose of this class is to search and get learning content details
 */
class LearningController extends Controller
{
	/**
     * [__construct - initialization of learning repository]
     */
	public function __construct()
	{
		$this->lrnObj = new LearningRepository();
	}    

	/**
     * [getLearningContents - get learning contents]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
	public function getLearningContents(Request $request)
	{
        $page = $request->input('page');
        $keyword = $request->input('keyword');
        $tags = $request->input('tags');
        $languages = $request->input('languages');
        $providers = $request->input('providers');
        $contents = $request->input('contents');
		$response = $this->lrnObj->getLearningContents($page,$keyword,$tags,$languages,$providers,$contents);

		if ($response !==  false) {
            return ['code' => 200, 'data' => json_decode($response), 'title' => 'Learning', 'message' => 'Getting case type successfully.'];
        } else {
            return ['code' => 300, 'data' => '', 'title' => 'Module', 'message' => 'Something went wrong'];
        }
		
	}

	/**
     * [getLearningObjectDetails - get more learning object details]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
	public function getLearningObjectDetails()
	{
		$response = $this->lrnObj->getLearningObjectDetails('2212044');

		if ($response !==  false) {
            return ['code' => 200, 'data' => json_decode($response), 'title' => 'Learning', 'message' => 'Getting case type successfully.'];
        } else {
            return ['code' => 300, 'data' => '', 'title' => 'Module', 'message' => 'Something went wrong'];
        }
		
	}

	/**
     * [getLearningObjectDetails - get default courses added to list]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
	public function getDefaultCollection(Request $request)
    {
		$response = $this->lrnObj->getDefaultCollection();        

		if ($response !==  false) {
            return ['code' => 200, 'data' => json_decode($response), 'title' => 'Learning', 'message' => 'Default collection.'];
        } else {
            return ['code' => 300, 'data' => '', 'title' => 'Module', 'message' => 'Something went wrong'];
        }
    }

    /**
     * [addToDefaultCollection - add course to library]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function addToDefaultCollection(Request $request)
    {
        $loId = $request->input('loId');
        $response = $this->lrnObj->addToDefaultCollection($loId); 

        if ($response ===  true) {
            return ['code' => 200, 'data' => 'Item added to collection', 'title' => 'Learning', 'message' => 'Default collection.'];
        } else {
            return ['code' => 300, 'data' => '', 'title' => 'Module', 'message' => 'Something went wrong'];
        }
    }

    /**
     * [createGo1User - create Go1 user]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function createGo1User(Request $request){
        $email = $request->all()['data']['email'];
        $firstName = $request->all()['data']['firstName'];
        $lastName = $request->all()['data']['lastName'];
        
        $response = $this->lrnObj->createGo1User($email,$firstName,$lastName); 

        if ($response ===  true) {
            return ['code' => 200, 'data' => '', 'title' => 'Learning', 'message' => 'Create user successfully.'];
        } else {
            return ['code' => 300, 'data' => '', 'title' => 'Learning', 'message' => 'Something went wrong'];
        }
    }    

    /**
     * [getProviders - get providers list from Go1]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function getProviders()
    {
        $providers = $this->lrnObj->getFilterList();
        if ($providers) {
            return ['code' => 200, 'data' => $providers, 'title' => 'Learning', 'message' => 'Providers'];
        } else {
            return ['code' => 300, 'data' => '', 'title' => 'Learning', 'message' => 'Something went wrong'];
        }
    }
}
