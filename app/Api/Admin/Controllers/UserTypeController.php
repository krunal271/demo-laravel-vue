<?php

namespace App\Api\Admin\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use App\Models\UserType;
use App\Api\Admin\Repositories\UserTypeRepository;
use DB;
use Carbon\Carbon;
use Validator;

/**
 * Here goes the description of the class. It should explain what the main
 * purpose of this class is to CRUD of user types like admin,hmu or
 * participant etc.
*/
class UserTypeController extends Controller
{
    /**
     * [__construct - to initialize usertype repository object.]
     */
    public function __construct()
    {
        $this->userTypeObj = new UserTypeRepository();
    }
    /**
     * [getUserTypeSelectList - get user type select list for select box]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function getUserTypeSelectList(Request $request)
    {
        $userTypeList=$this->userTypeObj->getUserTypeSelectList();
        if (!empty($userTypeList)) {
            return ['code' => 200 ,'data'=>$userTypeList,'title'=>'User Type','message'=>'Getting case type successfully.'];
        } else {
            return ['code'=> 300 ,'data'=>'','title'=>'User Type','message'=>'Something went wrong'];
        }
    }
    /**
     * [getUserTypes - get user types list using pagination]
     * @param  Request $request - getting no of record, and searchdata
     * @return [array]  - return code or user type list
     */
    public function getUserTypes(Request $request)
    {
        $noOfPage = $request->noofRecord;
        $searchData = $request->searchData;
        $sortData = $request->sortData;
        $userTypeList=$this->userTypeObj->getUserTypes($noOfPage, $searchData, $sortData);
        if (!empty($userTypeList)) {
            return ['code' => 200 ,'data'=>$userTypeList,'title'=>'User Type','message'=>'Getting case type successfully.'];
        } else {
            return ['code'=> 300 ,'data'=>'','title'=>'User Type','message'=>'Something went wrong'];
        }
    }
    /**
     * [addUserType - adsing user type]
     * @param Request $request - form data
     * @return [array]  - return code or boolean
     */
    public function addUserType(Request $request)
    {
        $validator = Validator::make($request->all(), [
                        'userType'=>'required|min:3|max:55',
                        'userTypeStatus'=>'required',
                    ]);
        if ($validator->fails()) {
            return ['code' => 300 ,'data'=>$validator->errors()->messages(),'title'=>'','message'=>'Please verify all fields'];
        } else {
            $userType = $request->all();
            $typeStatus = $this->userTypeObj->addUserType($userType);
            if ($typeStatus===true) {
                return ['code' => 200 ,'data'=>$typeStatus,'title'=>'User Type','message'=>'User type added successfully.'];
            } else {
                return ['code'=> 300 ,'data'=>'','title'=>'User Type','message'=>'Something went wrong'];
            }
        }
    }
    /**
     * [removeUserType - remove usertype by user type id]
     * @param  Request $request - user type id pass
     * @return [array]  - return code or boolean
     */
    public function removeUserType(Request $request)
    {
        $typeStatus = $this->userTypeObj->removeUserType($request->input('userTypeId'));

        if ($typeStatus===true) {
            if ($typeStatus === 2) {
                return ['code' => 300 ,'data'=>$typeStatus,'title'=>'User Type','message'=>'User type associated with department or user category.'];
            } else {
                return ['code' => 200 ,'data'=>$typeStatus,'title'=>'User Type','message'=>'User type removed successfully.'];
            }
        } else {
            return ['code'=> 300 ,'data'=>'','title'=>'User Type','message'=>'Something went wrong'];
        }
    }
    /**
     * [getUserTypeById - get user type details by user id.]
     * @param  Request $request - user type id
     * @return [array]  - return code and user details data
     */
    public function getUserTypeById(Request $request)
    {
        $userType = $this->userTypeObj->getUserTypeById($request->input('userTypeId'));
        if (!empty($userType)) {
            return ['code' => 200 ,'data'=>$userType,'title'=>'User Type','message'=>'User type removed successfully.'];
        } else {
            return ['code'=> 300 ,'data'=>'','title'=>'User Type','message'=>'Something went wrong'];
        }
    }
    /**
     * [editUserType - edit user type by form data]
     * @param  Request $request - form data
     * @return [boolean] - return code and boolean value
     */
    public function editUserType(Request $request)
    {
        $validator = Validator::make($request->all(), [
                        'type'=>'required|min:3|max:55',
                        'status'=>'required',
                    ]);
        if ($validator->fails()) {
            return ['code' => 300 ,'data'=>$validator->errors()->messages(),'title'=>'','message'=>'Please verify all fields'];
        } else {
            $userType = $this->userTypeObj->editUserType($request->all());
            if ($userType===1) {
                return ['code' => 200 ,'data'=>$userType,'title'=>'User Type','message'=>'User type updated successfully.'];
            } else {
                return ['code'=> 300 ,'data'=>'','title'=>'User Type','message'=>'Something went wrong'];
            }
        }
    }
    /**
     * [getSearchUserType - get search user type by search data]
     * @param  Request $request - search data
     * @return
     */
    public function getSearchUserType(Request $request)
    {
        $searchData = $request->all();
        $searchType = $this->userTypeObj->searchUserType($searchData);
    }
    /**
     * [deleteMultipleUserTypeById - delete user type by multiple id]
     * @param  Request $request - user type id
     * @return [array]  - return code with boolean value
     */
    public function deleteMultipleUserTypeById(Request $request)
    {
        $userTypeIdArray = $request->userTypeId;
        $deleteUser=$this->userTypeObj->deleteMultipleUserTypeById($userTypeIdArray);
        if ($deleteUser===1) {
            return ['code' => 200 ,'data'=>$deleteUser,'title'=>'User Type','message'=>'User types not associated with any department or user category has been successfully deleted.'];
        } else {
            return ['code'=> 300 ,'data'=>'','title'=>'User Delete','message'=>'Something went wrong'];
        }
    }
    /**
     * [updateMultipleUserTypeStatusById - change multiple status of user type]
     * @param  Request $request - user type id array
     * @return [array] - return code with boolean value
     */
    public function updateMultipleUserTypeStatusById(Request $request)
    {
        $userTypeData = $request->toArray()['userTypeId'];
        $activeUser=$this->userTypeObj->updateMultipleUserTypeStatusById($userTypeData);
        if ($activeUser!==0) {
            return ['code' => 200 ,'data'=>$activeUser,'title'=>'Update user status','message'=>'User Type status udpated successfully.'];
        } else {
            return ['code'=> 300 ,'data'=>'','title'=>'Update user status','message'=>'Something went wrong'];
        }
    }
}
