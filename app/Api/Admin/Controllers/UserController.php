<?php

namespace App\Api\Admin\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use App\User;
use App\Api\Admin\Repositories\UserRepository;
use Validator;

/**
 * Here goes the description of the class. It should explain what the main
 * purpose of this class is to CRUD of user
*/
class UserController extends Controller
{
    /**
     * [__construct - initialize object of user repository]
     */
    public function __construct()
    {
        $this->userObj = new UserRepository();
    }
    /**
     * [getUserList - get user list at admin side ]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function getUserListPagination(Request $request)
    {
        $noOfPage = $request->noofRecord;
        $searchData = $request->searchData;
        $sortedColumn = $request->sortedColumn;
        $order = $request->order;
        $userList=$this->userObj->getUserListPagination($noOfPage, $searchData, $sortedColumn, $order);
        if (!empty($userList)) {
            return $userList;
        } else {
            return ['code'=> 300 ,'data'=>'','title'=>'User List','message'=>'Something went wrong'];
        }
    }
    /**
     * [getHiringManagerSelectList - get hiring manager list for participant]
     * @return [type] [description]
     */
    public function getHiringManagerSelectList()
    {
        $hmuList=$this->userObj->getHiringManagerSelectList();
        if (!empty($hmuList)) {
            return ['code' => 200 ,'data'=>$hmuList,'title'=>'User','message'=>'Getting case type successfully.'];
        } else {
            return ['code'=> 300 ,'data'=>'','title'=>'User','message'=>'Something went wrong'];
        }
    }
    /**
     * [createUser - add user]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function createUser(Request $request)
    {
        $params=$request->userData;
        $rules = array(
            'userTypeId'=>'required',
            'userTypeName'=>'required',
            'givenName'=>'required|max:55',
            'surName'=>'required|max:55',
            'contactNoPrefix'=>'required|numeric|digits:2',
            'contactPhone'=>'required|numeric|digits_between:10,15',
            'dateOfBirth'=>'required|date_format:"d/m/Y"',
            'email' => 'required|email',
            'status' => 'required',
        );
        if ($params['userTypeName']==='Participant') {
            $rules['userCategoryId']='required';
            $rules['departmentId']='required';
            $rules['hiringManager']='required';
            $rules['role']='required|max:55';
            $rules['levelOfContact']='required';
            //$rules['confirm']='required';
        } /*else {
            $rules['password']='required|max:64|required_with:confirmPassword|same:confirmPassword';
            $rules['confirmPassword']='required|max:64';
        }*/
        
        $validator = Validator::make($params, $rules);
        if ($validator->fails()) {
            //print_r($validator->errors()->messages());
            return ['code' => 300 ,'data'=>$validator->errors()->messages(),'title'=>'','message'=>'Please verify all fields'];
        } else {
            $addUser=$this->userObj->createUser($params);

            if (!empty($addUser)) {
                return $addUser;
            } else {
                return ['code'=> 300 ,'data'=>'','title'=>'User Add','message'=>'Something went wrong'];
            }
        }
    }
    /**
     * [getUserDetailsByUserId - get user details for edit page]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function getUserDetailsByUserId(Request $request)
    {
        $userId = $request->userId;
        $userDetails=$this->userObj->getUserDetailsByUserId($userId);
        if (!empty($userDetails)) {
            return ['code' => 200 ,'data'=>$userDetails,'title'=>'User Details','message'=>'User successfully displayed.'];
        } else {
            return ['code'=> 300 ,'data'=>'','title'=>'User Details','message'=>'Something went wrong'];
        }
    }
    /**
     * [editUser - update user]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function editUser(Request $request)
    {
        $params=$request->userData;
        
        $rules = array(
            'userTypeId'=>'required',
            'userTypeName'=>'required',
            'givenName'=>'required|max:55',
            'surName'=>'required|max:55',
            'contactNoPrefix'=>'required|numeric|digits:2',
            'contactPhone'=>'required|numeric|digits_between:10,15',
            'dateOfBirth'=>'required|date_format:"d/m/Y"',
            'password' => 'max:64',
            'status' => 'required',
        );
        if ($params['userTypeName']==='Participant') {
            $rules['userCategoryId']='required';
            $rules['departmentId']='required';
            $rules['hiringManager']='required';
            $rules['role']='required|max:55';
            $rules['levelOfContact']='required';
            //$rules['confirm']='required';
        }
        $validator = Validator::make($params, $rules);
        if ($validator->fails()) {
            //print_r($validator->errors()->messages());
            return ['code' => 300 ,'data'=>$validator->errors()->messages(),'title'=>'','message'=>'Please verify all fields'];
        } else {
            $editUser=$this->userObj->editUser($params);
            if (!empty($editUser)) {
                return $editUser;
            } else {
                return ['code'=> 300 ,'data'=>'','title'=>'User Update','message'=>'Something went wrong'];
            }
        }
    }
    /**
     * [editUser - update user]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function setPassword(Request $request)
    {
        $userData=$request->userData;
        $editUser=$this->userObj->editUser($userData);
        if ($editUser===1) {
            return $editUser;
        } else {
            return ['code'=> 300 ,'data'=>'','title'=>'User Update','message'=>'Something went wrong'];
        }
    }
    /**
     * [deleteUserByUserId - delete user by user id]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function deleteUserByUserId(Request $request)
    {
        $userId = $request->userId;
        $deleteUser=$this->userObj->deleteUserByUserId($userId);
        if ($deleteUser!==0) {
            return ['code' => 200 ,'data'=>$deleteUser,'title'=>'User Delete','message'=>'User successfully deleted.'];
        } else {
            return ['code'=> 300 ,'data'=>'','title'=>'User Delete','message'=>'Something went wrong'];
        }
    }
    /**
     * [deleteMultipleUserByUserId - delete multiple user by user id]
     * @param  Request $request [userIds]
     * @return [type]           [Array]
     */
    public function deleteMultipleUserByUserId(Request $request)
    {
        $userIdArray = $request->userId;
        $deleteUser=$this->userObj->deleteMultipleUserByUserId($userIdArray);
        if ($deleteUser!==0) {
            return ['code' => 200 ,'data'=>$deleteUser,'title'=>'User Delete','message'=>'User successfully deleted.'];
        } else {
            return ['code'=> 300 ,'data'=>'','title'=>'User Delete','message'=>'Something went wrong'];
        }
    }
    /**
     * [activeMultipleUserByUserId - status change for multiple user]
     * @param  Request $request [userIds]
     * @return [type]           [Array]
     */
    public function updateMultipleUserStatusByUserId(Request $request)
    {
        $userData = $request->toArray()['userData'];
        // dd($request->toArray()['userData']);
        // $userIdArray = $userData->userIds;
        $activeUser=$this->userObj->updateMultipleUserStatusByUserId($userData);
        if ($activeUser!==0) {
            return ['code' => 200 ,'data'=>$activeUser,'title'=>'Update user status','message'=>'User status udpated successfully.'];
        } else {
            return ['code'=> 300 ,'data'=>'','title'=>'Update user status','message'=>'Something went wrong'];
        }
    }
    /**
     * [updatePassword - change password for user]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function updatePassword(Request $request)
    {
        $userData = $request->toArray()['userData'];
        // dd($request->toArray()['userData']);
        // $userIdArray = $userData->userIds;
        $active_user=$this->userObj->updateMultipleUserStatusByUserId($userData);
        if ($active_user) {
            return ['code' => 200 ,'data'=>$active_user,'title'=>'Update user status','message'=>'User status udpated successfully.'];
        } else {
            return ['code'=> 300 ,'data'=>'','title'=>'Update user status','message'=>'Something went wrong'];
        }
    }
}
