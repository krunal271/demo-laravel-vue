<?php

namespace App\Api\Admin\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Mail\AdminResetPassword;
use App\Models\AdminUser;
use Carbon\Carbon;
use DB;
use Illuminate\Foundation\Auth\SendsPasswordResetEmails;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\URL;
use JWTAuth;

/**
 * Here goes the description of the class. It should explain what the main
 * purpose of this class is to forgot password
*/
class ForgotPasswordController extends Controller
{
    /*
            |--------------------------------------------------------------------------
            | Password Reset Controller
            |--------------------------------------------------------------------------
            |
            | This controller is responsible for handling password reset emails and
            | includes a trait which assists in sending these notifications from
            | your application to your users. Feel free to explore this trait.
            |
    */

    use SendsPasswordResetEmails;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }
    /**
     * [resetLink - reset link]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function resetLink(Request $request)
    {
        // dd('test');
        $this->validate($request, ['email' => 'required|email']);
        $token = JWTAuth::getToken();
        $reseEmail = $request->input('email');

        if ($user = AdminUser::where('email', $reseEmail)->first()) {
            // dd($user);
            $token = str_random(64);

            $password_reset = \DB::table('password_reset')->where('email', $user->email)->first();

            if (empty($password_reset)) {
                DB::table('password_reset')->insert([
                    'email' => $user->email,
                    'token' => $token,
                    'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                    'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
                ]);
            } else {
                DB::table(config('auth.passwords.users.table'))->where('email', $user->email)->
                    update([
                    'token' => $token,
                    'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
                    'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
                ]);
            }
            $url = '';
            $url =  config('app.url');
            //$url = URL::current();
            //$url = url('/');
            // dd($url);
            $resetLink = '';
            $resetLink =  $url. '/admin-reset-password/' . $token . '/' . $user->email;
            $type = 'password_reset_link';
            $emailData = [
                'NAME' => $user->prefferedName,
                'EMAIL' => $user->email,
                'LINK' => $resetLink,
                'SUBJECT' => 'Reset Password',
                'WITH-ATTECHMENT' => 'no',
            ];
            // dd($type,$emailData);
            $mailstatus = Mail::to($user->email)->send(new AdminResetPassword($emailData));
            // } else {
            // $mailstatus = Mail::to($user->email)->send(new AdminResetPassword($emailData));
            // dd($mailstatus);
            // if($mailstatus == 1){
            $data = [
                'status' => 200,
                'token' => $token,
                'email' => $user->email,
                'link' => $resetLink,
                'msg' => 'Reset password link send to your mail id',
            ];
        //  $data = ['status' =>301 ,  'email' =>  $request->input('email')];

            // }
        } else {
            $data = ['status' => 404, 'email' => $request->input('email')];
        }
        return $data;
    }
}
