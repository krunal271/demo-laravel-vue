<?php

namespace App\Api\Admin\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\AdminUser;
use DB;
use Hash;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Password;
use Illuminate\Support\Str;

/**
 * Here goes the description of the class. It should explain what the main
 * purpose of this class is to CRUD of reset password 
*/
class ResetPasswordController extends Controller {
	/*
		    |--------------------------------------------------------------------------
		    | Password Reset Controller
		    |--------------------------------------------------------------------------
		    |
		    | This controller is responsible for handling password reset requests
		    | and uses a simple trait to include this behavior. You're free to
		    | explore this trait and override any methods you wish to tweak.
		    |
	*/

	use ResetsPasswords;

	/**
	 * Where to redirect users after resetting their password.
	 *
	 * @var string
	 */
	protected $redirectTo = '/home';

	/**
	 * Create a new controller instance.
	 *
	 * @return void
	 */
	public function __construct() {
		$this->middleware('guest');
	}
	/**
	 * [checkPasswordReset - check reset password]
	 * @param  Request $request [description]
	 * @param  [type]  $token   [description]
	 * @return [type]           [description]
	 */
	public function checkPasswordReset(Request $request, $token = null) {
		$emalId = Input::get('email');
		$password_reset = \DB::table('password_reset')->where('token', $token)->where('email', $emalId)->first();
		//  dd($emalId,$token);
		// $password_reset ='';
		if (empty($password_reset)) {
			// throw new NotFoundHttpException('invalide request');
			// return view('error.error');
			return ['code' => 400, 'data' => $password_reset];
		}
		return ['code' => 200, 'data' => $password_reset];

	}
	/**
	 * [reset - reset password]
	 * @param  Request $request [description]
	 * @return [type]           [description]
	 */
	public function reset(Request $request) {

		// Here we will attempt to reset the user's password. If it is successful we
		// will update the password on an actual user model and persist it to the
		// database. Otherwise we will parse the error and return the response.
		// here we check for Mobile user
		$data = $request->all()['data'];
		$userData = AdminUser::where(['email' => $data['email']])->first();
		$userPassword = Hash::make(trim(html_entity_decode($data['password'])));
		$token = str_random(30);
		AdminUser::where('email', $data['email'])->update(['password' => $userPassword]);
		//DB::table(config('auth.passwords.users.table'))->where('email', $data['email'])->delete();

		// If the password was successfully reset, we will redirect the user back to
		// the application's home authenticated view. If there is an error we can
		// response them back to where they came from with their error message.

		// return redirect('/login');
		return ['code' => 200, 'data' => ''];

	}
}
