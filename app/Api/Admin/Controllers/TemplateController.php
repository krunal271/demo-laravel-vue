<?php

namespace App\Api\Admin\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use App\Models\Template;
use App\Api\Admin\Repositories\TemplateRepository;


use DB;
use Carbon\Carbon;

/**
 * Here goes the description of the class. It should explain what the main
 * purpose of this class is to CRUD of template
*/
class TemplateController extends Controller
{
    /**
     * [__construct - initialization of template repository]
     */
    public function __construct()
    {
        $this->templateObj = new TemplateRepository();
    }

  
    /**
     * [getDepartmentListBy - to get template list]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function getTemplateList(Request $request)
    {
        $noOfPage = $request->noofRecord;
        $searchData = $request->searchData;
        $templateList = $this->templateObj->getTemplateList($noOfPage, $searchData);
        if ($templateList) {
            return ['code' => 200 ,'data'=>$templateList,'title'=>'Template','message'=>'Getting case type successfully.'];
        } else {
            return ['code'=> 300 ,'data'=>'','title'=>'Template','message'=>'Something went wrong'];
        }
    }
    /**
     * [addTemplate - to add template]
     * @param Request $request [description]
     */
    public function addTemplate(Request $request)
    {
        $templateStatus = $this->templateObj->addTemplate($request->all());

        if ($templateStatus) {
            return ['code' => 200 ,'data'=>$templateStatus,'title'=>'Template','message'=>'Template added successfully.'];
        } else {
            return ['code'=> 300 ,'data'=>'','title'=>'Template','message'=>'Something went wrong'];
        }
    }
    /**
     * [deleteTemplate - to delete template]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function deleteTemplate(Request $request)
    {
        $templateStatus = $this->templateObj->deleteTemplate($request->input('templateId'));

        if ($templateStatus) {
            return ['code' => 200 ,'data'=>$templateStatus,'title'=>'Template','message'=>'Template removed successfully.'];
        } else {
            return ['code'=> 300 ,'data'=>'','title'=>'Template','message'=>'Something went wrong'];
        }
    }
    /**
     * [deleteMultipleTemplateById - to delete multiple template]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function deleteMultipleTemplateById(Request $request)
    {
        $templateIdArray = $request->templateId;
        $deleteTemplate = $this->templateObj->deleteMultipleTemplateById($templateIdArray);

        if ($deleteTemplate) {
            return ['code' => 200 ,'data'=>$deleteTemplate,'title'=>'Template','message'=>'Template successfully deleted.'];
        } else {
            return ['code'=> 300 ,'data'=>'','title'=>'Template','message'=>'Something went wrong'];
        }
    }
    /**
     * [getTemplateById - get template by template id]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function getTemplateById(Request $request)
    {
        $deptData = $this->templateObj->getTemplateById($request->all());

        if ($deptData) {
            return ['code' => 200 ,'data'=>$deptData,'title'=>'Template','message'=>'Template details'];
        } else {
            return ['code'=> 300 ,'data'=>'','title'=>'Template','message'=>'Something went wrong'];
        }
    }
    /**
     * [editTemplateById - edit template by template id]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function editTemplateById(Request $request)
    {
        $templateData = $this->templateObj->editTemplateById($request->input('templateData'));
        if ($templateData) {
            return ['code' => 200 ,'data'=>$templateData,'title'=>'Template','message'=>'Template details updated successfully'];
        } else {
            return ['code'=> 300 ,'data'=>'','title'=>'Template','message'=>'Something went wrong'];
        }
    }
    /**
     * [updateMultipleTemplateStatusByTemplateId - change status of multiple 
     * template]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function updateMultipleTemplateStatusByTemplateId(Request $request)
    {
        $templateData = $request->toArray()['templateId'];
        $active_template=$this->templateObj->updateMultipleTemplateStatusByTemplateId($templateData);

        if ($active_template) {
            return ['code' => 200 ,'data'=>$active_template,'title'=>'Update Template status','message'=>'Template status udpated successfully.'];
        } else {
            return ['code'=> 300 ,'data'=>'','title'=>'Update Template status','message'=>'Something went wrong'];
        }
    }
}
