<?php

namespace App\Api\Admin\Controllers;

use App\Api\Admin\Repositories\AdminUserRepository;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Validator;

/**
 * Here goes the description of the class. It should explain what the main
 * purpose of this class is to add admin user ,update admin user and delete
 * admin.
*/
class AdminController extends Controller
{
    /**
     * [__construct -Admin constructor which is used to initialize object of admin repository.]
     */
    public function __construct()
    {
        $this->adminObj = new AdminUserRepository();
    }
    /**
     * [getAdminList - for getting admin list with pagination
     * @param  Request $request - for getting search data and no of records
     * per page.
     * @return Array - with code and description
     */
    public function getAdminList(Request $request)
    {
        $noOfPage = $request->noofRecord;
        $searchData = $request->searchData;
        $sortedColumn = $request->sortedColumn;
        $order = $request->order;
        $adminList = $this->adminObj->getAdminList($noOfPage, $searchData, $sortedColumn, $order);

        if (count($adminList)>0) {
            return ['code' => 200, 'data' => $adminList, 'title' => 'Admin', 'message' => 'Getting case type successfully.'];
        } else {
            return ['code' => 300, 'data' => '', 'title' => 'Admin', 'message' => 'Something went wrong'];
        }
    }
    /**
     * [getDashboardData - getting dashboard data of admin user]
     * @return Array - return with code and dashboard data
     */
    public function getDashboardData()
    {
        $dashboardData = $this->adminObj->getDashboardData();

        if (count($dashboardData)) {
            return ['code' => 200, 'data' => $dashboardData, 'title' => 'Admin', 'message' => 'Dashboard details.'];
        } else {
            return ['code' => 300, 'data' => '', 'title' => 'Admin', 'message' => 'Something went wrong.'];
        }
    }

    public function getmoduleDashboardData(Request $request)
    {
        $data = $request->input('data');
        $dashboardData = $this->adminObj->getModuleDashboardData($data);

        if (count($dashboardData)) {
            return ['code' => 200, 'data' => $dashboardData, 'title' => 'Admin', 'message' => 'Dashboard details.'];
        } else {
            return ['code' => 300, 'data' => '', 'title' => 'Admin', 'message' => 'Something went wrong.'];
        }
    }

    public function getParticipantDashboardData(Request $request) 
    {
        $data = $request->input('data');
        $dashboardData = $this->adminObj->getParticipantDashboardData($data);

        if (count($dashboardData)) {
            return ['code' => 200, 'data' => $dashboardData, 'title' => 'Admin', 'message' => 'Dashboard details.'];
        } else {
            return ['code' => 300, 'data' => '', 'title' => 'Admin', 'message' => 'Something went wrong.'];
        }
    }
    /**
    * [addAdmin - add admin user ]
    * @param Request $request - Formdata of add user
    * @return Array - return with code admin id
    */
    public function addAdmin(Request $request)
    {
        $validator = Validator::make($request->all(), [
                        'contactPhone'=>'required|numeric|digits:10',
                        'contactPhonePrefix'=>'required|numeric|digits:2',
                        'email'=>'required|email|min:3|max:55',
                        'givenName'=>'required|min:3|max:55|regex:/^[\pL\s]+$/u',
                        'password'=>'required|min:6|max:64',
                        'status'=>'required',
                        'surName'=>'required|min:3|max:55|regex:/^[\pL\s]+$/u',
                        'userTypeId'=>'required|numeric',
                    ]);
                    
        if ($validator->fails()) {
            return ['code' => 300 ,'data'=>$validator->errors()->messages(),'title'=>'','message'=>'Please verify all fields'];
        } else {
            $mailExist = $this->adminObj->checkAdminEmail($request->input('email'));
            if ($mailExist===0) {
                $adminStatus = $this->adminObj->addAdmin($request->all());
                if ($adminStatus===true) {
                    return ['code' => 200, 'data' => $adminStatus, 'title' => 'Admin', 'message' => 'Admin added successfully.'];
                } else {
                    return ['code' => 300, 'data' => '', 'title' => 'Admin', 'message' => 'Something went wrong'];
                }
            } else {
                return ['code' => 300, 'data' => '', 'title' => 'Admin', 'message' => 'Email already in use'];
            }
        }
    }
    /**
    * [deleteAdmin - delete admin by admin id]
    * @param  Request $request - adminId pass inrequest
    * @return [Array] - return with code and admin id.
    */
    public function deleteAdmin(Request $request)
    {
        $adminStatus = $this->adminObj->deleteAdmin($request->input('adminId'));
        if ($adminStatus===1) {
            return ['code' => 200, 'data' => $adminStatus, 'title' => 'Admin', 'message' => 'Admin removed successfully.'];
        } else {
            return ['code' => 300, 'data' => '', 'title' => 'Admin', 'message' => 'Something went wrong'];
        }
    }
    /**
     * [deleteMultipleAdminById - delete multiple admin user by multiple ids]
     * @param  Request $request - multiple admin id in array
     * @return [Array] - code with delete status
     */
    public function deleteMultipleAdminById(Request $request)
    {
        $adminIdArray = $request->adminId;
        $deleteAdmin = $this->adminObj->deleteMultipleAdminById($adminIdArray);
        if ($deleteAdmin===1) {
            return ['code' => 200, 'data' => $deleteAdmin, 'title' => 'Module', 'message' => 'Admin successfully deleted.'];
        } else {
            return ['code' => 300, 'data' => '', 'title' => 'Admin', 'message' => 'Something went wrong'];
        }
    }
    /**
     * [getAdminById - get admin details by id]
     * @param  Request $request - pass admin id in request
     * @return [Array] - with code and admin details.
     */
    public function getAdminById(Request $request)
    {
        $adminData = $this->adminObj->getAdminById($request->all());
        if (!empty($adminData)) {
            return ['code' => 200, 'data' => $adminData, 'title' => 'Admin', 'message' => 'Admin'];
        } else {
            return ['code' => 300, 'data' => '', 'title' => 'Admin', 'message' => 'Something went wrong'];
        }
    }
    /**
     * [editAdminById -edit admin user by id]
     * @param  Request $request - form data for admin user to edit.
     * @return [Array]   - with code and admin details array
     */
    public function editAdminById(Request $request)
    {
        $validator = Validator::make($request->input('adminData'), [
                        'contactPhone'=>'required|numeric|digits:10',
                        'contactPhonePrefix'=>'required|numeric|digits:2',
                        'email'=>'required|email|min:3|max:55',
                        'givenName'=>'required|min:3|max:55|regex:/^[\pL\s]+$/u',
                        'status'=>'required',
                        'surName'=>'required|min:3|max:55|regex:/^[\pL\s]+$/u',
                    ]);
        if ($validator->fails()) {
            return ['code' => 300 ,'data'=>$validator->errors()->messages(),'title'=>'','message'=>'Please verify all fields'];
        } else {
            $adminData = $this->adminObj->editAdminById($request->input('adminData'));
            if ($adminData===1) {
                return ['code' => 200, 'data' => $adminData, 'title' => 'Admin', 'message' => 'Admin details updated successfully'];
            } else {
                return ['code' => 300, 'data' => '', 'title' => 'Admin', 'message' => 'Something went wrong'];
            }
        }
    }
    /**
     * [updateMultipleAdminStatusByAdminId - update status active or inactive
     * for multiple admin user.]
     * @param  Request $request - multiple admin user id for changing status
     * @return [Array]  - code with status.
     */
    public function updateMultipleAdminStatusByAdminId(Request $request)
    {
        $adminData = $request->toArray()['adminId'];
        $activeAdmin = $this->adminObj->updateMultipleAdminStatusByAdminId($adminData);
        if ($activeAdmin===1) {
            return ['code' => 200, 'data' => $activeAdmin, 'title' => 'Update admin status', 'message' => 'Admin status udpated successfully.'];
        } else {
            return ['code' => 300, 'data' => '', 'title' => 'Update admin status', 'message' => 'Something went wrong'];
        }
    }
}
