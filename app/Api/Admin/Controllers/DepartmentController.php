<?php

namespace App\Api\Admin\Controllers;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use App\Models\Department;
use App\Api\Admin\Repositories\DepartmentRepository;
use DB;
use Carbon\Carbon;
use Validator;

/**
 * Here goes the description of the class. It should explain what the main
 * purpose of this class is to CRUD of department
*/
class DepartmentController extends Controller
{
    /**
     * [__construct - intialize department repository]
     */
    public function __construct()
    {
        $this->deptObj = new DepartmentRepository();
    }
    /**
     * [getDepartmentSelectListByUserTypeId - department list for slect box]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function getDepartmentSelectListByUserTypeId(Request $request)
    {
        $userTypeId = $request->userTypeId;
        $deptList=$this->deptObj->getDepartmentSelectListByUserTypeId($userTypeId);
        if (!empty($deptList)) {
            return ['code' => 200 ,'data'=>$deptList,'title'=>'Department','message'=>'Getting case type successfully.'];
        } else {
            return ['code'=> 300 ,'data'=>'','title'=>'Department','message'=>'Something went wrong'];
        }
    }
    /**
     * [getDepartmentList - get department list by pagination]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function getDepartmentList(Request $request)
    {
        $noOfPage = $request->noofRecord;
        $searchData = $request->searchData;
        $sortData = $request->sortData;
        $departmentList = $this->deptObj->getDepartmentList($noOfPage, $searchData, $sortData);
        
        if (!empty($departmentList)) {
            return ['code' => 200 ,'data'=>$departmentList,'title'=>'Department','message'=>'Getting case type successfully.'];
        } else {
            return ['code'=> 300 ,'data'=>'','title'=>'Department','message'=>'Something went wrong'];
        }
    }
    /**
     * [addDepartment - add department by form data]
     * @param Request $request [description]
     */
    public function addDepartment(Request $request)
    {
        $validator = Validator::make($request->all(), [
                        'userTypeId'=>'required|numeric',
                        'department'=>'required|min:3|max:55',
                        'status'=>'required',
                    ]);
        if ($validator->fails()) {
            return ['code' => 300 ,'data'=>$validator->errors()->messages(),'title'=>'','message'=>'Please verify all fields'];
        } else {
            $deptStatus = $this->deptObj->addDepartment($request->all());
            if ($deptStatus===true) {
                return ['code' => 200 ,'data'=>$deptStatus,'title'=>'Department','message'=>'Department added successfully.'];
            } else {
                return ['code'=> 300 ,'data'=>'','title'=>'Department','message'=>'Something went wrong'];
            }
        }
    }
    /**
     * [deleteDepartment - delete department by department id]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function deleteDepartment(Request $request)
    {
        $deptStatus = $this->deptObj->deleteDepartment($request->input('departmentId'));

        if ($deptStatus===1) {
            if ($deptStatus === 2) {
                return ['code' => 300 ,'data'=>$deptStatus,'title'=>'Department','message'=>'Department associated with user.'];
            } else {
                return ['code' => 200 ,'data'=>$deptStatus,'title'=>'Department','message'=>'User type removed successfully.'];
            }
        } else {
            return ['code'=> 300 ,'data'=>'','title'=>'Department','message'=>'Something went wrong'];
        }
    }
    /**
     * [deleteMultipleDepartmentById - delete multiple department by multiple
     * array department ids.]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function deleteMultipleDepartmentById(Request $request)
    {
        $departmentIdArray = $request->departmentId;
        $deleteDepartment = $this->deptObj->deleteMultipleDepartmentById($departmentIdArray);

        if ($deleteDepartment===2) {
            return ['code' => 200 ,'data'=>$deleteDepartment,'title'=>'Department','message'=>'Department not associated with any user successfully deleted.'];
        } else {
            return ['code'=> 300 ,'data'=>'','title'=>'Department','message'=>'Something went wrong'];
        }
    }
    /**
     * [getDepartmentById - get department details by department id]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function getDepartmentById(Request $request)
    {
        $deptData = $this->deptObj->getDepartmentById($request->all());

        if (!empty($deptData)) {
            return ['code' => 200 ,'data'=>$deptData,'title'=>'Department','message'=>'Department details'];
        } else {
            return ['code'=> 300 ,'data'=>'','title'=>'Department','message'=>'Something went wrong'];
        }
    }
    /**
     * [editDepartmentById - edit department by form data]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function editDepartmentById(Request $request)
    {
        $validator = Validator::make($request->input('departmentData'), [
                        'userTypeId'=>'required|numeric',
                        'name'=>'required|min:3|max:55',
                        'status'=>'required',
                    ]);
        if ($validator->fails()) {
            return ['code' => 300 ,'data'=>$validator->errors()->messages(),'title'=>'','message'=>'Please verify all fields'];
        } else {
            $deptData = $this->deptObj->editDepartmentById($request->input('departmentData'));
            if ($deptData===1) {
                return ['code' => 200 ,'data'=>$deptData,'title'=>'Department','message'=>'Department details updated successfully.'];
            } else {
                return ['code'=> 300 ,'data'=>'','title'=>'Department','message'=>'Something went wrong'];
            }
        }
    }
    /**
     * [updateMultipleDepartmentStatusByDepartmentId - update multiple status
     * of department]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function updateMultipleDepartmentStatusByDepartmentId(Request $request)
    {
        $departmentData = $request->toArray()['departmentId'];
        
        $activeDepartment=$this->deptObj->updateMultipleDepartmentStatusByDepartmentId($departmentData);

        if ($activeDepartment!==0) {
            return ['code' => 200 ,'data'=>$activeDepartment,'title'=>'Update department status','message'=>'Department status udpated successfully.'];
        } else {
            return ['code'=> 300 ,'data'=>'','title'=>'Update department status','message'=>'Something went wrong'];
        }
    }
}
