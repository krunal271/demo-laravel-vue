<?php

namespace App\Api\Admin\Controllers;

use App\Api\Admin\Repositories\ModuleRepository;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Api\Admin\Requests\Module;
use Validator;

/**
 * Here goes the description of the class. It should explain what the main
 * purpose of this class is to CRUD of module
*/
class ModuleController extends Controller
{
    /**
     * [__construct - initialization of module repository]
     */
    public function __construct()
    {
        $this->moduleObj = new ModuleRepository();
    }
    /**
     * [getModuleList - get module list by pagination]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function getModuleList(Request $request)
    {
        $noOfPage = $request->noofRecord;
        $searchData = $request->searchData;
        $sortData = $request->sortData;
        $moduleList = $this->moduleObj->getModuleList($noOfPage, $searchData, $sortData);

        if (!empty($moduleList)) {
            return ['code' => 200, 'data' => $moduleList, 'title' => 'Module', 'message' => 'Getting case type successfully.'];
        } else {
            return ['code' => 300, 'data' => '', 'title' => 'Module', 'message' => 'Something went wrong'];
        }
    }
    /**
     * [addModule - add module]
     * @param Request $request [description]
     */
    public function addModule(Request $request)
    {
        $validator = Validator::make($request->all(), [
                        'allowTotalAttempt'=>'required|numeric',
                        'description'=>'required',
                        'moduleName'=>'required|min:3|max:55',
                        'status'=>'required',
                    ]);
        if ($validator->fails()) {
            return ['code' => 300 ,'data'=>$validator->errors()->messages(),'title'=>'','message'=>'Please verify all fields'];
        } else {
            $moduleStatus = $this->moduleObj->addModule($request);
            if ($moduleStatus===true) {
                return ['code' => 200, 'data' => $moduleStatus, 'title' => 'Module', 'message' => 'Module added successfully.'];
            } else {
                return ['code' => 300, 'data' => '', 'title' => 'Module', 'message' => 'Something went wrong'];
            }
        }
    }
    /**
     * [deleteModule - delete module by module id]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function deleteModule(Request $request)
    {
        $moduleStatus = $this->moduleObj->deleteModule($request->input('moduleId'));

        if ($moduleStatus===1) {
            return ['code' => 200, 'data' => $moduleStatus, 'title' => 'Module', 'message' => 'Module removed successfully.'];
        } else {
            return ['code' => 300, 'data' => '', 'title' => 'Module', 'message' => 'Something went wrong'];
        }
    }
    /**
     * [deleteMultipleModuleById - delete multiple module by module id]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function deleteMultipleModuleById(Request $request)
    {
        $moduleIdArray = $request->moduleId;
        $deleteModule = $this->moduleObj->deleteMultipleModuleById($moduleIdArray);

        if ($deleteModule!==0) {
            return ['code' => 200, 'data' => $deleteModule, 'title' => 'Module', 'message' => 'Module successfully deleted.'];
        } else {
            return ['code' => 300, 'data' => '', 'title' => 'Module', 'message' => 'Something went wrong'];
        }
    }
    /**
     * [getModuleById - get module details by module id]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function getModuleById(Request $request)
    {
        $moduleData = $this->moduleObj->getModuleById($request->all());

        if (!empty($moduleData)) {
            return ['code' => 200, 'data' => $moduleData, 'title' => 'Module', 'message' => 'Module details'];
        } else {
            return ['code' => 300, 'data' => '', 'title' => 'Module', 'message' => 'Something went wrong'];
        }
    }
    /**
     * [editModuleById - edit module by module id]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function editModuleById(Request $request)
    {
        $validator = Validator::make($request->input('moduleData'), [
                        'allowTotalAttempt'=>'required|numeric',
                        'description'=>'required',
                        'moduleName'=>'required|min:3|max:55',
                        'status'=>'required',
                    ]);
        if ($validator->fails()) {
            return ['code' => 300 ,'data'=>$validator->errors()->messages(),'title'=>'','message'=>'Please verify all fields'];
        } else {
            $moduleData = $this->moduleObj->editModuleById($request->input('moduleData'));
            if ($moduleData===1) {
                return ['code' => 200, 'data' => $moduleData, 'title' => 'module', 'message' => 'Module details updated successfully'];
            } else {
                return ['code' => 300, 'data' => '', 'title' => 'module', 'message' => 'Something went wrong'];
            }
        }
    }
    /**
     * [updateMultipleModuleStatusByModuleId - update multiple module status
     * by module id]
     * @param  Request $request [description]
     * @return [type]           [description]
     */
    public function updateMultipleModuleStatusByModuleId(Request $request)
    {
        $moduleData = $request->toArray()['moduleId'];
        $activeModule = $this->moduleObj->updateMultipleModuleStatusByModuleId($moduleData);
        if ($activeModule) {
            return ['code' => 200, 'data' => $activeModule, 'title' => 'Update module status', 'message' => 'Module status udpated successfully.'];
        } else {
            return ['code' => 300, 'data' => '', 'title' => 'Update module status', 'message' => 'Something went wrong'];
        }
    }
}
