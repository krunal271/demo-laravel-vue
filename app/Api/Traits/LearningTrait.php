<?php 
	
namespace App\Api\Traits;

use App\Models\Go1Client;
use App\Models\Go1User;
use App\Models\Go1Collection;
use App\Models\UserEnrollment;

/**
 * Purpose of this trait is to define Go1 feaures
 */
trait LearningTrait
{
	/**
     * [retrieveAdminAccessToken - get access_token from Go1]
     * @param  [description]
     * @return [type]           [description]
     */
	public function retrieveAdminAccessToken()
	{
		$clientDetails = Go1Client::first()->toArray();
		$clientId = $clientDetails['client'];
		$secret = $clientDetails['secret'];

		$client = new \GuzzleHttp\Client();
		$res = $client->post('https://auth.go1.com/oauth/token',[ 
					'form_params' => [
				    'client_id' => $clientId,
				    'client_secret' => $secret,
				    'grant_type' => 'client_credentials',
				]
			]
		);
		
		$contents = json_decode($res->getBody()->getContents());
		if ($res->getStatusCode() === 200) {
			session(['admin_token'=> $contents->access_token]);
		}
	}

	/**
     * [retrieveAdminAccessToken - get access_token from Go1]
     * @param  [description]
     * @return [type]           [description]
     */
	public function retrieveFrontAccessToken()
	{
		$clientDetails = Go1Client::first()->toArray();
		$clientId = $clientDetails['client'];
		$secret = $clientDetails['secret'];

		$client = new \GuzzleHttp\Client();
		$res = $client->post('https://auth.go1.com/oauth/token',[ 
					'form_params' => [
				    'client_id' => $clientId,
					'client_secret' => $secret,
					'jwt'=> '',
					'portal_name'=> '',
					'grant_type' => 'client_credentials',
				]
			]
		);
		
		$contents = json_decode($res->getBody()->getContents());
		if ($res->getStatusCode() === 200) {
			session(['front_token'=> $contents->access_token]);
		}
	}
	
	/**
     * [retrieveAdminAccessToken - create user on Go1]
     * @param  [description]
     * @return [type]           [description]
     */
	public function createGo1User($userId,$email,$firstName,$lastName)
	{

		$client = new \GuzzleHttp\Client();
        $headers = ['Accept'=> 'application/json', 'Content-Type'=> 'application/x-www-form-urlencoded', 'Authorization'=> session('front_token'), 'Cache-Control'=> 'no-cache'];
		try {
			$res = $client->request('POST','https://api.go1.com/v2/users',
								[ 
									'headers'=> $headers,
									'form_params'=> ["email"=> $email, "first_name"=> $firstName, "last_name"=> $lastName]
								]
			);  
			$contents = json_decode($res->getBody()->getContents());
			return $contents->id;
		} catch (\GuzzleHttp\Exception\RequestException $e) {
			return false;
		}
	}	

	/**
     * [getLearningObjectDetails - get learning content details from Go1]
     * @param  [description]
     * @return [type]           [description]
     */
	public function getLearningObjectDetails($learningObjectId)
    {
        $client = new \GuzzleHttp\Client();

        $headers = ['Accept'=> 'application/json', 'Content-Type'=> 'application/json', 'Authorization'=> session('front_token')];
        $response = $client->request('GET', 'https://api.go1.com/v2/learning-objects/'.$learningObjectId, 
            ['headers'=> $headers]);

        if ($response->getStatusCode() === 200) {
            return json_decode($response->getBody()->getContents());
        } else {
            return false;
        }   
	}
	
	/**
     * [getEnrollementList - get enrollments list by user from Go1]
     * @param  [description]
     * @return [type]           [description]
     */
	public function getEnrollementList($userId,$page)
	{
		$client = new \GuzzleHttp\Client();

		$user = Go1User::where('userId', $userId)->first();
		if ($user) {
			$go1User = $user->toArray();
		} else {
			$go1User = false;
		}
		$headers = ['Accept'=> 'application/json', 'Content-Type'=> 'application/x-www-form-urlencoded', 'Authorization'=> session('front_token'), 'Cache-Control'=> 'no-cache'];
		if (is_array($go1User)) {
			$go1UserId = $go1User['go1UserId'];
			//echo 'https://api.go1.com/v2/enrollments?user_id='.$go1UserId.'&offset='.$page.'&limit=5&include=lo';
			$res = $client->request('GET','https://api.go1.com/v2/enrollments?user_id='.$go1UserId.'&offset='.$page.'&limit=5&include=lo',
								[ 
									'headers'=> $headers,
								]
			);  
			$contents = json_decode($res->getBody()->getContents());
			return $contents;
		} else {
			return ['hits'=>[]];
		}
	}

	/**
     * [generateLoginLink - generate login link for user]
     * @param  [description]
     * @return [type]           [description]
     */
	public function generateLoginLink($userId)
	{
		//echo session('front_token');die;
		$client = new \GuzzleHttp\Client();
		$user = Go1User::where('userId', $userId)->first();
		$go1User = $user->toArray();
		$headers = ['Accept'=> 'application/json', 'Content-Type'=> 'application/x-www-form-urlencoded', 'Authorization'=> session('front_token'), 'Cache-Control'=> 'no-cache'];
		
		if (is_array($go1User)) {
			$go1UserId = $go1User['go1UserId'];
			$req = $client->request('POST','https://api.go1.com/v2/users/'.$go1UserId.'/login',
								[ 
									'headers'=> $headers,
									'form_params'=> ['user-id'=> $go1UserId]
								]
			);  
			if ($req->getStatusCode() === 200) {                
				$response = json_decode($req->getBody()->getContents());
				return $response->url;
			} else {
				return false;
			}		
		} else {
			return false;
		}
	}

	/**
     * [enrollToDefaultCourses - enroll user all default courses]
     * @param  [description]
     * @return [type]           [description]
     */
	public function enrollToDefaultCourses($userId)
	{
		$client = new \GuzzleHttp\Client();
		$user = Go1User::where('userId', $userId)->first();
		if ($user) {
			$go1User = $user->toArray();
		} else {
			$go1User = false;
		}
		$headers = ['Accept'=> 'application/json', 'Content-Type'=> 'application/json', 'Authorization'=> session('front_token'), 'Cache-Control'=> 'no-cache'];
		if (is_array($go1User)) {
			$go1UserId = $go1User['go1UserId'];
			$enrollments = UserEnrollment::select('loId')->where('go1UserId','=',$go1UserId)->pluck('loId');
			if ($enrollments) {
				$userEnrollments = $enrollments->toArray();
				$getEnrolledCourses = Go1Collection::whereNotIn('loId', $userEnrollments)->get();
				if ($getEnrolledCourses->count() > 0) {
					try {
						foreach ($getEnrolledCourses->toArray() as $loDetails) {
							$res = $client->request('POST','https://api.go1.com/v2/enrollments',
													[
														'headers'=> $headers,
														'json'=> ["lo_id"=> $loDetails['loId'], "parent_lo_id"=> 0,"parent_enrollment_id"=> 0, "user_id"=> $go1UserId]
													]
												);
							if ($res->getStatusCode() === 201) {                                            
								$enrollDetails = json_decode($res->getBody()->getContents());
								UserEnrollment::create(['enrollmentId'=> $enrollDetails->id, 'go1UserId'=> $go1UserId, 'loId'=>$enrollDetails->lo_id, 'parentLoId'=>$enrollDetails->parent_lo_id, 'parentEnrollmentId'=>$enrollDetails->parent_enrollment_id, 'status'=>$enrollDetails->status]);
							}  
						}
					} catch(Exception $e) {
					}
				}
			}
		}
	}

	/**
     * [updateUserDetails - update user details]
     * @param  [description]
     * @return [type]           [description]
     */
	public function updateUserDetails($userId)
	{
		$client = new \GuzzleHttp\Client();
		$headers = ['Accept'=> 'application/json', 'Content-Type'=> 'multipart/form-data', 'Authorization'=> session('front_token'), 'Cache-Control'=> 'no-cache'];
		$user = Go1User::where('userId', $userId)->first();
		if ($user) {
			$go1User = $user->toArray();
		} else {
			$go1User = false;
		}
		$go1UserId = $go1User['go1UserId'];
		
		if ($go1UserId) {
			$res = $client->request('PATCH','https://api.go1.com/v2/users/'.$go1UserId,
										[
											'headers'=> $headers,
											'form_params' => ['status'=> false]
										]
									);
		}
	}
}	

?>