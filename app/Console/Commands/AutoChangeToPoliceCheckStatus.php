<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Api\Front\Repositories\CommandRepository;

class AutoChangeToPoliceCheckStatus extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'change:police-check-status';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
          $com = new CommandRepository();
          $com->changePoliceCheckStatusExpired();
          $com->changePoliceCheckStatusDue();
    }
}
