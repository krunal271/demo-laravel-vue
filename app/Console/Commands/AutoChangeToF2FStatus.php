<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Api\Front\Repositories\CommandRepository;


class AutoChangeToF2FStatus extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'change:f2f-status';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Auto change to F2F status based on expity date';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $com = new CommandRepository();
        $com->changeF2FStatusExpired();
        $com->changeF2FStatusOverDue();
        $com->changeF2FStatusDue();

    }
}
