<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;
use App\Api\Front\Repositories\CommandRepository;

class AutoChangeToIdStatus extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'change:id-status';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Auto change to Id status based on expity date';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $com = new CommandRepository();
        $com->changeIdStatusExpired();
        $com->changeIdStatusDue();
    }
}
