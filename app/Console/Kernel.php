<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        'App\Console\Commands\AutoChangeToContractStatus',
        'App\Console\Commands\AutoChangeToF2FStatus',
        'App\Console\Commands\AutoChangeToIdStatus',
        'App\Console\Commands\AutoChangeToPoliceCheckStatus',
        'App\Console\Commands\AutoChangeToWwcStatus'
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('change:contract-status')->daily();
        $schedule->command('change:f2f-status')->daily();
        $schedule->command('change:id-status')->daily();
        $schedule->command('change:police-check-status')->daily();
        $schedule->command('change:wwc-status')->daily();
    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}
