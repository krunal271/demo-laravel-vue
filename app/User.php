<?php
namespace App;

use App\Models\Documents;
use App\Models\Booking;
use App\Models\UserDetail;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

/**
 * Here goes the description of the class. It should explain what the main
 * purpose of this class is to user
*/
class User extends Authenticatable
{
    use Notifiable;
    use SoftDeletes;
    /**
     * [$table - set table name]
     * @var string
     */
    protected $table = 'user';
    /**
     * [$primaryKey - set primary key]
     * @var string
     */
    protected $primaryKey = 'userId';
    /**
     * [$fillable - set table field]
     * @var [type]
     */
    protected $fillable = [
        'userTypeId',
        'userCategoryId',
        'departmentId',
        'hiringManager',
        'email',
        'password',
        'orgEmail',
        'role',
        'engagementStatus',
        'activeDate',
        'profileCompleteDate',
        'profileCompleteTime',
        'participantConfirmed',
        'confirmPolicy',
        'inviteUser',
        'status',
        'createdBy',
        'updatedBy',
        'deletedBy',
    ];
    /**
     * [$dates - set date field]
     * @var [type]
     */
    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * [userType - relationship with user type]
     * @return [type] [description]
     */
    public function userType()
    {
        return $this->belongsTo('App\Models\UserType', 'userTypeId', 'userTypeId');
    }
    /**
     * [department - relationship with department]
     * @return [type] [description]
     */
    public function department()
    {
        return $this->belongsTo('App\Models\Department', 'departmentId', 'departmentId');
    }
    /**
     * [UserDetail define entity]
     */
    public function UserDetail()
    {
        return $this->hasOne('App\Models\UserDetail', 'userId', 'userId');
    }
    /**
     * [userDetails - define entity]
     * @return [type] [description]
     */
    public function userDetails()
    {
        return $this->hasOne('App\Models\UserDetail', 'userId', 'userId');
    }
    /**
     * [userCategory - relationship with user category]
     * @return [type] [description]
     */
    public function userCategory()
    {
        return $this->belongsTo('App\Models\UserCategory', 'userCategoryId', 'userCategoryId');
    }
    /**
     * [documents - define entity ]
     * @return [type] [description]
     */
    public function documents()
    {
        return $this->hasMany('App\Models\Documents', 'userId', 'userId');
    }
    /**
     * [bookings - define entity ]
     * @return [type] [description]
     */
    public function bookings()
    {
        return $this->hasMany('App\Models\Booking', 'userId', 'userId');
    }
    /**
     * [getProfileCompleteDateAttribute - get profile complete date]
     * @param  [type] $value [description]
     * @return [type]        [description]
     */
    public function getProfileCompleteDateAttribute($value)
    {
        return Carbon::parse($value)->format('d/m/Y');
    }
    
    /**
     * [setProfileCompleteDateAttribute - set profile complete date]
     * @param [type] $value [description]
     */
    public function setProfileCompleteDateAttribute($value)
    {
        return $this->attributes['profileCompleteDate'] = Carbon::createFromFormat('d/m/Y', $value);
    }
    /**
     * [hmuDetail - relationship for hmu user]
     * @return [type] [description]
     */
    public function hmuDetail()
    {
        return $this->belongsTo('App\User', 'hiringManager', 'userId');
    }
}
