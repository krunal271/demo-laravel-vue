<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class UserEnrollment extends Model
{
    use SoftDeletes;
    /**
     * [$table - table name]
     * @var string
     */
    protected $table = 'user_enrollment';
    /**
     * [$primaryKey - primary key]
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * [$fillable - filed name]
     * @var [type]
     */
    protected $fillable=[
      'enrollmentId',
      'go1UserId',
      'loId',
      'parentLoId',
      'parentEnrollmentId',
      'status',
      'created_at',
      'updated_at',
      'deleted_at'
    ];

    /**
     * [$hidden - hidden field name]
     * @var [type]
     */
    protected $hidden = [
      'created_at',
      'updated_at',
      'deleted_at',
    ];

    /**
     * [$dates -date field name]
     * @var [type]
     */
    protected $dates = [
            'created_at',
            'updated_at',
            'deleted_at'
        ];
}
