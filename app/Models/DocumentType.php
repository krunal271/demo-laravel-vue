<?php

namespace App\Models;

use App\Models\Documents;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Here goes the description of the class. It should explain what the main
 * purpose of this class is to database field of document type.
*/
class DocumentType extends Model
{
    use SoftDeletes;
    protected $table = 'user_document_type';
    //set primary key
    protected $primaryKey = 'userDocumentTypeId';
    protected $fillable=[
      'slug',
      'type',
      'status',
      'helpText',
      'createdBy',
      'updatedBy',
      'deletedBy'
    ];
    protected $dates = [
            'created_at',
            'updated_at',
            'deleted_at'
        ];

    /**
     * [documents - relationship with user document]
     * @return [type] [description]
     */
    public function documents()
    {
        return $this->hasMany('App\Models\Documents', 'userDocumentTypeId', 'userDocumentTypeId');
    }
}
