<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use App\User;

/**
 * Here goes the description of the class. It should explain what the main
 * purpose of this class is to database field of booking.
*/
class Booking extends Model
{
    use SoftDeletes;
    protected $table = 'booking';
    protected $primaryKey = 'bookingId';
    protected $fillable=[
      'userId',
      'bookingDate',
      'bookingTime',
      'attend',
      'status',
      'bookingStatus',
      'createdBy',
      'updatedBy',
      'deletedBy'
    ];
    protected $dates = [
            'created_at',
            'updated_at',
            'deleted_at'
        ];

    /**
     * [user - relationship with user]
     * @return [type] [description]
     */
    public function user()
    {
        return $this->belongsTo('App\User', 'userId', 'userId');
    }

    /* public function getBookingDateAttribute($value)
     {

       if($value != null && $value != ''){
           return Carbon::parse($value)->format('d/m/Y');
       }else{
         return null;
       }
     }

     public function setBookingDateAttribute($value)

     {
       if($value != ''){
         return $this->attributes['bookingDate'] =   Carbon::createFromFormat('d/m/Y', $value);
       }else{
          return $this->attributes['bookingDate'] = null;
       }

     } */
}
