<?php
namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Here goes the description of the class. It should explain what the main
 * purpose of this class is to user type model
*/
class Go1Client extends Model
{
    use SoftDeletes;
    /**
     * [$table - table name]
     * @var string
     */
    protected $table = 'go1_client';
    /**
     * [$primaryKey - primary key]
     * @var string
     */
    protected $primaryKey = 'clientId';

    /**
     * [$fillable - filed name]
     * @var [type]
     */
    protected $fillable=[
      'clientId',
      'client',
      'secret',
      'created_at',
      'updated_at',
      'deleted_at'
    ];

    /**
     * [$hidden - hidden field name]
     * @var [type]
     */
    protected $hidden = [
      'clientId',
      'created_at',
      'updated_at',
      'deleted_at',
    ];

    /**
     * [$dates -date field name]
     * @var [type]
     */
    protected $dates = [
            'created_at',
            'updated_at',
            'deleted_at'
        ];
}
