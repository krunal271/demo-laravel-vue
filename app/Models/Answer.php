<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
//use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Here goes the description of the class. It should explain what the main
 * purpose of this class is to database field of answer.
*/
class Answer extends Model
{

    //use SoftDeletes;
    /**
     * [$table - table name]
     * @var string
     */
    protected $table = 'answer';
    /**
     * [$primaryKey - primary key]
     * @var string
     */
    protected $primaryKey = 'answerId';
    
    protected $fillable=[
      'interviewAndReferenceId',
      'questionId',
      'userId',
      'answer',
      'correctAns',
      'status',
      'createdBy',
      'updatedBy',
      'deletedBy'
    ];

    protected $hidden = [
      'createdBy',
      'created_at',
      'deletedBy',
      'deleted_at',
      'updatedBy',
      'updated_at'
    ];

    protected $dates = [
            'created_at',
            'updated_at',
            'deleted_at'
        ];

    /**
     * [interviewAndReference - relationship with interviewAndReference table]
     * @return [type] [description]
     */
    public function interviewAndReference()
    {
        return $this->belongsTo('App\Models\InterviewAndReference', 'interviewAndReferenceId', 'interviewAndReferenceId');
    }
}
