<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Here goes the description of the class. It should explain what the main
 * purpose of this class is to database table of question.
*/
class Question extends Model
{
    use SoftDeletes;
    protected $table = 'question';
    protected $primaryKey = 'questionId';

    protected $fillable=[
      'questionTypeId',
      'question',
      'ansType',
      'allowComment',
      'status',
      'createdBy',
      'updatedBy',
      'deletedBy'
    ];

    protected $hidden = [
      'createdBy',
      'created_at',
      'deletedBy',
      'deleted_at',
      'updatedBy',
      'updated_at'
    ];

    protected $dates = [
            'created_at',
            'updated_at',
            'deleted_at'
        ];

    /**
     * [questionType - relationship with quetion type]
     * @return [type] [description]
     */
    public function questionType()
    {
        return $this->belongsTo('App\Models\QuestionType', 'questionTypeId', 'questionTypeId');
    }
}
