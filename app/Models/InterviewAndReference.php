<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class InterviewAndReference extends Model
{
    use SoftDeletes;
    protected $table = 'interview_and_reference';
    protected $primaryKey = 'interviewAndReferenceId';

    protected $fillable=[
      'userId',
      'interviewDate',
      'referenceDate',
      'interviewStatus',
      'referenceStatus',
      'createdBy',
      'updatedBy',
      'deletedBy'
    ];

    protected $hidden = [
      'createdBy',
      'created_at',
      'deletedBy',
      'deleted_at',
      'updatedBy',
      'updated_at'
    ];

    protected $dates = [
            'created_at',
            'updated_at',
            'deleted_at'
        ];

  
    /* public function questionType() {
       return $this->belongsTo('App\Models\QuestionType', 'questionTypeId', 'questionTypeId');
     } */

    /**
     * [getReferenceDateAttribute - referance date get]
     * @param  [type] $value - referance date value
     * @return [type]        get referance date in d/m/y format
     * CreatedBy - Richa
     */
    public function getReferenceDateAttribute($value)
    {
        if ($value != null) {
            return Carbon::parse($value)->format('d/m/Y');
        } else {
            return null;
        }
    }
    /**
     * [getReferenceDateAttribute - referance date set]
     * @param  [type] $value - referance date value
     * @return [type]        set referance date in d/m/y format
     * CreatedBy - Richa
     */
    public function setReferenceDateAttribute($value)
    {
        if ($value != '') {
            return $this->attributes['referenceDate'] = Carbon::createFromFormat('d/m/Y', $value);
        } else {
            return $this->attributes['referenceDate'] = null;
        }
    }
    /**
     * [getInterviewDateAttribute - interview date get]
     * @param  [type] $value - interview date value
     * @return [type]        set interview date in d/m/y format
     * CreatedBy - Richa
     */
    public function getInterviewDateAttribute($value)
    {
        if ($value != null) {
            return Carbon::parse($value)->format('d/m/Y');
        } else {
            return null;
        }
    }
    /**
    * [setInterviewDateAttribute - interview date set]
    * @param  [type] $value - interview date value
    * @return [type]        set interview date in d/m/y format
    * @CreatedBy - Richa
    */
    public function setInterviewDateAttribute($value)
    {
        if ($value != '') {
            return $this->attributes['interviewDate'] = Carbon::createFromFormat('d/m/Y', $value);
        } else {
            return $this->attributes['interviewDate'] = null;
        }
    }

    /**
     * [answer description]
     * @return [type] [description]
     * @CreatedBy - Richa
     */
    public function answer()
    {
        return $this->hasMany('App\Models\Answer', 'interviewAndReferenceId', 'interviewAndReferenceId');
    }
}
