<?php
namespace App\Models;

use App\Models\User;
use App\Models\UserType;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Here goes the description of the class. It should explain what the main
 * purpose of this class is to database table of User Category.
*/
class UserCategory extends Model
{
    use SoftDeletes;
    /**
     * [$table - table name]
     * @var string
     */
    protected $table = 'user_category';
    /**
     * [$primaryKey - primary key name]
     * @var string
     */
    protected $primaryKey = 'userCategoryId';
    /**
     * [$fillable - table field name]
     * @var [type]
     */
    protected $fillable=[
      'userTypeId',
      'type',
      'status',
      'createdBy',
      'updatedBy',
      'deletedBy'
    ];
    /**
     * [$dates - date field name]
     * @var [type]
     */
    protected $dates = [
            'created_at',
            'updated_at',
            'deleted_at'
    ];

    /**
     * [userType - relationship with user type]
     * @return [type] [description]
     *
     * Author : Mital
     */
    public function userType()
    {
        return $this->hasMany('App\Models\UserType', 'userTypeId', 'userTypeId');
    }
    /**
     * [user - relationship with user]
     * @return [type] [description]
     */
    public function user()
    {
        return $this->hasMany('App\User', 'userCategoryId', 'userCategoryId');
    }
    /**
     * [userTypeCategory - relationship woth user type category]
     * @return [type] [description]
     */
    public function userTypeCategory()
    {
        return $this->belongsTo('App\Models\UserType', 'userTypeId', 'userTypeId');
    }
}
