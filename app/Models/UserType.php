<?php
namespace App\Models;

use App\User;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Here goes the description of the class. It should explain what the main
 * purpose of this class is to user type model
*/
class UserType extends Model
{
    use SoftDeletes;
    /**
     * [$table - table name]
     * @var string
     */
    protected $table = 'user_type';
    /**
     * [$primaryKey - primary key]
     * @var string
     */
    protected $primaryKey = 'userTypeId';

    /**
     * [$fillable - filed name]
     * @var [type]
     */
    protected $fillable=[
      'userTypeId',
      'type',
      'status',
      'createdBy',
      'updatedBy',
      'deletedBy'
    ];

    /**
     * [$hidden - hidden field name]
     * @var [type]
     */
    protected $hidden = [
      'createdBy',
      'created_at',
      'deletedBy',
      'deleted_at',
      'updatedBy',
      'updated_at'
    ];

    /**
     * [$dates -date field name]
     * @var [type]
     */
    protected $dates = [
            'created_at',
            'updated_at',
            'deleted_at'
        ];
    /**
     * [user relationship with user table]
     * @return relationship
     */
    public function user()
    {
        return $this->hasMany('App\User', 'userTypeId', 'userTypeId');
    }
}
