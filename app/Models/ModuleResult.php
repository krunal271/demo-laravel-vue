<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Here goes the description of the class. It should explain what the main
 * purpose of this class is to database table of module result.
*/
class ModuleResult extends Model
{
    use SoftDeletes;
    protected $table = 'module_result';
    protected $primaryKey = 'moduleResultId';

    protected $fillable=[
      'userId',
      'moduleId',
      'moduleName',
      'passingScore',
      'resultDate',
      'resultTime',
      'score',
      'bookmark',
      'noOfAttemt',
      'moduleResultStatus',
      'status',
      'createdBy',
      'updatedBy',
      'deletedBy'
    ];
    protected $dates = [
            'created_at',
            'updated_at',
            'deleted_at'
    ];
}
