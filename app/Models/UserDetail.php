<?php

namespace App\Models;

use App\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Here goes the description of the class. It should explain what the main
 * purpose of this class is to database table of user detail.
*/
class UserDetail extends Model
{
    use SoftDeletes;

    protected $table = 'user_detail';
    protected $primaryKey = 'userDetailId';
    protected $fillable=[
      'userDetailId',
      'userId',
      'givenName',
      'surName',
      'prefferedName',
      'occupation',
      'levelOfContact',
      'pacificBasedRole',
      'pacificBaseCountry',
      'policeCheque',
      'dateOfBirth',
      'contactPhone',
      'contactPhone2',
      'address',
      'aboriginal',
      'disability',
      'gender',
      'selfDescribe',
      'emergencyContactPrimary',
      'relationshipPrimary',
      'contactPhonePrimary',
      'emergencyContactSecondary',
      'relationshipSecondary',
      'contactPhoneSecondary',
      'candidateLegalWorkAustralia',
      'residentialStatus',
      'childSafeguardRating',
      'visaAttachments',
      'speakOtherLanguage',
      'parentsOutsideAustralia',
      'bornOutsideustralia',
      'otherResidentialStatus',
      'confirmInformation',
      'createdBy',
      'updatedBy',
      'deletedBy'
    ];
    protected $dates = [
            'created_at',
            'updated_at',
            'deleted_at'
        ];

    /**
     * [user - relationship with user]
     * @return [type] [description]
     */
    public function user()
    {
        return $this->belongsTo('App\User', 'userId', 'userId');
    }
    /**
     * [getDateOfBirthAttribute - get date of birth]
     * @param  [type] $value [description]
     * @return [type]        [description]
     */
    public function getDateOfBirthAttribute($value)
    {
        if ($value != null) {
            return Carbon::parse($value)->format('d/m/Y');
        } else {
            return null;
        }
    }
    /**
     * [setDateOfBirthAttribute - set date of birth]
     * @param [type] $value [description]
     * @return [type]        [description]
     */
    public function setDateOfBirthAttribute($value)
    {
        if ($value != '') {
            return $this->attributes['dateOfBirth'] = Carbon::createFromFormat('d/m/Y', $value);
        } else {
            return $this->attributes['dateOfBirth'] = null;
        }
    }
}
