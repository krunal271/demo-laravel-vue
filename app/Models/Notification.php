<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Here goes the description of the class. It should explain what the main
 * purpose of this class is to database table of notification.
*/
class Notification extends Model
{
    use SoftDeletes;
    
    protected $table = 'notification';
    protected $primaryKey = 'notificationId';

    protected $fillable=[
      'notificationId',
      'userId',
      'viewerId',
      'type',
      'name',
      'slug',
      'template',
      'notificationStatus',
      'participantRead',
      'hmuRead',
      'adminRead',
      'createdBy'
    ];
    protected $dates = [
            'created_at',
            'updated_at',
            'deleted_at'
    ];
    /**
     * [user -relationship with user table]
     * @return [type] [description]
     */
    public function user()
    {
        return $this->belongsTo('App\User', 'userId', 'userId');
    }
}
