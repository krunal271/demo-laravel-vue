<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Here goes the description of the class. It should explain what the main
 * purpose of this class is to database table of module.
*/
class Module extends Model
{
    use SoftDeletes;
    /**
     * [$table - table name]
     * @var string
     */
    protected $table = 'module';
    /**
     * [$primaryKey - primary key name]
     * @var string
     */
    protected $primaryKey = 'moduleId';
    /**
     * [$fillable - fillable field name]
     * @var [type]
     */
    protected $fillable=[
      'moduleId',
      'moduleName',
      'allowTotalAttemt',
      'passingScore',
      'moduleDate',
      'status',
      'createdBy',
      'updatedBy',
      'deletedBy'
    ];
    /**
     * [$dates - date fields name]
     * @var [type]
     */
    protected $dates = [
            'created_at',
            'updated_at',
            'deleted_at'
    ];
    /**
     * [moduleResult - relationship with module result]
     * @return [type] [description]
     */
    public function moduleResult()
    {
        return $this->hasMany('App\Models\ModuleResult', 'moduleId', 'moduleId');
    }
}
