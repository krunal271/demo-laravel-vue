<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Here goes the description of the class. It should explain what the main
 * purpose of this class is to database table of user statutory declaration.
*/
class UserStatutoryDeclaration extends Model
{
    use SoftDeletes;
    
    protected $table = 'user_statutory_declaration';
    protected $primaryKey = 'statutoryDeclarationId';
    protected $fillable=[
      'userId',
      'userDocumentId',
      'fullName',
      'address',
      'occupation',
      'declaration',
      'witnessFullName',
      'statutoryImage',
      'createdBy',
      'updatedBy',
      'deletedBy'
    ];
    protected $dates = [
            'created_at',
            'updated_at',
            'deleted_at'
    ];

    /**
     * [document - relation ship with user document]
     * @return [type] [description]
     */
    public function document()
    {
        return $this->belongsTo('App\Models\Documents', 'userDocumentId', 'userDocumentId');
    }
}
