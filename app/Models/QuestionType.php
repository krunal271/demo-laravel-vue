<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Here goes the description of the class. It should explain what the main
 * purpose of this class is to database table of question type.
*/
class QuestionType extends Model
{
    use SoftDeletes;

    protected $table = 'question_type';
    protected $primaryKey = 'questionTypeId';

    protected $fillable=[
      'type',
      'slug',
      'status',
      'createdBy',
      'updatedBy',
      'deletedBy'
    ];

    protected $hidden = [
      'createdBy',
      'created_at',
      'deletedBy',
      'deleted_at',
      'updatedBy',
      'updated_at'
    ];

    protected $dates = [
            'created_at',
            'updated_at',
            'deleted_at'
        ];

    /**
     * [question - relationship with question]
     * @return [type] [description]
     */
    public function question()
    {
        return $this->hasMany('App\Models\Question', 'questionTypeId', 'questionTypeId');
    }
}
