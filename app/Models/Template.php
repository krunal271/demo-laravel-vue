<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Here goes the description of the class. It should explain what the main
 * purpose of this class is to database table of template.
*/
class Template extends Model
{
    use SoftDeletes;
    protected $table = 'email_template';
    protected $primaryKey = 'emailTemplateId';

    protected $fillable=[
      'emailTemplateId',
      'type',
      'name',
      'template',
      'subject',
      'status',
      'createdBy',
      'updatedBy',
      'deletedBy'
    ];
    protected $dates = [
            'created_at',
            'updated_at',
            'deleted_at'
    ];
}
