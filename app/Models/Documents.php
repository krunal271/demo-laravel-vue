<?php

namespace App\Models;

use App\Models\DocumentType;
use App\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Here goes the description of the class. It should explain what the main
 * purpose of this class is to database field of user document.
*/
class Documents extends Model
{
    use SoftDeletes;
    protected $table = 'user_document';
    protected $primaryKey = 'userDocumentId';
    protected $fillable=[
      'userId',
      'userDocumentTypeId',
      'expiryDate',
      'expiryDateStatus',
      'documentNumber',
      'verifiedStatus',
      'verifiedBy',
      'createdBy',
      'updatedBy',
      'deletedBy'
    ];
    protected $dates = [
            'created_at',
            'updated_at',
            'deleted_at'
        ];

    /**
     * [documentType -relationship with document type]
     * @return [type] [description]
     */
    public function documentType()
    {
        return $this->belongsTo('App\Models\DocumentType', 'userDocumentTypeId', 'userDocumentTypeId');
    }
    /**
     * [user - relationship with user]
     * @return [type] [description]
     */
    public function user()
    {
        return $this->belongsTo('App\User', 'userId', 'userId');
    }

    public function userDetail()
    {
        return $this->belongsTo('App\Models\UserDetail', 'userId', 'userId');
    }
    /**
     * [getExpiryDateAttribute - expiry date get]
     * @param  [type] $value [description]
     * @return [type]        [description]
     */
    public function getExpiryDateAttribute($value)
    {
        return Carbon::parse($value)->format('d/m/Y');
    }
    /**
     * [setExpiryDateAttribute - expiry date set]
     * @param [type] $value [description]
     */
    public function setExpiryDateAttribute($value)
    {
        return $this->attributes['expiryDate'] =   Carbon::createFromFormat('d/m/Y', $value);
    }
    /**
     * [userSatutoryDeclaration description]
     * @return [type] [description]
     */
    public function userSatutoryDeclaration()
    {
        return $this->hasOne('App\Models\UserStatutoryDeclaration', 'userDocumentId', 'userDocumentId');
    }
}
