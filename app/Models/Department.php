<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

/**
 * Here goes the description of the class. It should explain what the main
 * purpose of this class is to maintain relationship with department.
*/
class Department extends Model
{
    use SoftDeletes;
    /**
     * [$table - table name]
     * @var string
     */
    protected $table = 'department';
    /**
     * [$primaryKey - primary key name]
     * @var string
     */
    protected $primaryKey = 'departmentId';
    /**
     * [$fillable - field name]
     * @var [type]
     */
    protected $fillable=[
      'departmentId',
      'userTypeId',
      'name',
      'status',
      'createdBy',
      'updatedBy',
      'deletedBy'
    ];
    /**
     * [$dates - dates field name]
     * @var [type]
     */
    protected $dates = [
            'created_at',
            'updated_at',
            'deleted_at'
    ];
    /**
     * [userType - relationship with user type]
     * @return [type] [description]
     *
     * Author : Mital
     */
    public function userType()
    {
        return $this->hasMany('App\Models\UserType', 'userTypeId', 'userTypeId');
    }
    /**
     * [userDepartment - relationship with user department]
     * @return [type] [description]
     */
    public function userDepartment()
    {
        return $this->belongsTo('App\Models\UserType', 'userTypeId', 'userTypeId');
    }
}
