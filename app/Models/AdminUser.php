<?php

namespace App\Models;

use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Foundation\Auth\User as Authenticatable;

/**
 * Here goes the description of the class. It should explain what the main
 * purpose of this class is to database field of admin user.
*/
class AdminUser extends Authenticatable
{
    use SoftDeletes;
    /**
     * [$guard - guard name]
     * @var string
     */
    protected $guard = 'admin';
    /**
     * [$table - table name]
     * @var string
     */
    protected $table = 'admin_user';
    /**
     * [$primaryKey - primary key]
     * @var string
     */
    protected $primaryKey = 'adminUserId';
    /**
     * [$fillable - field name]
     * @var [type]
     */
    protected $fillable = [
        'userTypeId',
        'userCategoryId',
        'departmentId',
        'hiringManager',
        'email',
        'password',
        'orgEmail',
        'role',
        'engagementStatus',
        'profileCompleteDate',
        'participantConfirmed',
        'status',
        'createdBy',
        'updatedBy',
        'deletedBy',
    ];
    /**
     * [$dates - date field name]
     * @var [type]
     */
    protected $dates = [
        'created_at',
        'updated_at',
        'deleted_at',
    ];
    /**
     * [usertype - relationship function]
     * @return - relation ship between usertype and admin user.
     */
    public function usertype()
    {
        return $this->belongsTo('App\Models\UserType', 'userTypeId', 'userTypeId');
    }
}
