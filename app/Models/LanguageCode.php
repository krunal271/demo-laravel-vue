<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class LanguageCode extends Model
{
    /**
     * [$table - table name]
     * @var string
     */
    protected $table = 'language_code';
    /**
     * [$primaryKey - primary key]
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * [$fillable - filed name]
     * @var [type]
     */
    protected $fillable=[
      'language',
      'code',
      'created_at',
      'updated_at',
    ];

    /**
     * [$hidden - hidden field name]
     * @var [type]
     */
    protected $hidden = [
      'created_at',
      'updated_at',
      'deleted_at',
    ];

    /**
     * [$dates -date field name]
     * @var [type]
     */
    protected $dates = [
            'created_at',
            'updated_at',
        ];
}
