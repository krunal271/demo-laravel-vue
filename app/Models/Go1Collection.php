<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Go1Collection extends Model
{
    use SoftDeletes;
    /**
     * [$table - table name]
     * @var string
     */
    protected $table = 'go1_collection';
    /**
     * [$primaryKey - primary key]
     * @var string
     */
    protected $primaryKey = 'id';

    /**
     * [$fillable - filed name]
     * @var [type]
     */
    protected $fillable=[
      'loId',
      'title',
      'image',
      'type',
      'created_at',
      'updated_at',
      'deleted_at'
    ];

    /**
     * [$hidden - hidden field name]
     * @var [type]
     */
    protected $hidden = [
      'created_at',
      'updated_at',
      'deleted_at',
    ];

    /**
     * [$dates -date field name]
     * @var [type]
     */
    protected $dates = [
            'created_at',
            'updated_at',
            'deleted_at'
        ];
}
