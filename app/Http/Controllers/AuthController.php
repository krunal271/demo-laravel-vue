<?php

namespace App\Http\Controllers;

use App\User;
use Config;
use Illuminate\Http\Request;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use App\Api\Traits\LearningTrait;

class AuthController extends Controller {
	
	use LearningTrait;

	public function __construct() {
		Config::set('jwt.user', 'App\User');
		Config::set('jwt.identifier', 'userId');
		Config::set('auth.defaults.guard', 'web');
	}

	public function authenticate(Request $request) {

		// grab credentials from the request
		$credentials = $request->only('email', 'password');
		try {

			// attempt to verify the credentials and create a token for the user
			if (!$token = JWTAuth::attempt($credentials)) {
				return response()->json(['error' => 'The login details entered are invalid. If you have forgotten your password please follow the forgot password process.'], 401);
			}
		} catch (JWTException $e) {
			// something went wrong whilst attempting to encode the token
			return response()->json(['error' => 'could_not_create_token'], 500);
		}
		$userData = JWTAuth::toUser($token);
		// all good so return the token
		//return response()->json(compact('token'));
		//$token = response()->json(compact('token'));
		// $token = compact('token');
		if ($userData->userTypeId === 4) {
			$this->retrieveFrontAccessToken();
			$this->enrollToDefaultCourses($userData->userId);
		}
		return response()->json(compact('token', 'userData'));

	}

	public function check() {
		// dd(config('auth.defaults.guard'),config('jwt.identifier'),config('jwt.user'));
		\Log::info('Check Method is called');
		try {
			JWTAuth::parseToken()->authenticate();
		} catch (JWTException $e) {
			return response(['authenticated' => false]);
		}
		\Log::info('After Authenticate');

		// Here Add Functionality if use is Active then allowed to login
		$token = JWTAuth::getToken();

		\Log::info('After Getting token');
		if ($token) {
			$userData = JWTAuth::toUser($token);

			// dd($userData);
			if ($userData == false) {
				return response(['authenticated' => false, 'userData' => $userData]);

			}
			$userData = User::with('UserDetail')->where('userId', $userData->userId)->first();
			// dd($userData, 'userdata');
			// \Log::info('Success');
			return response(['authenticated' => true, 'userData' => $userData]);
		}

		\Log::info('NOT GETTING TOKEN');
	}

	public function logout() {
		try {
			$token = JWTAuth::getToken();

			if ($token) {

				JWTAuth::invalidate($token);
			}

		} catch (JWTException $e) {
			return response()->json($e->getMessage(), 401);
		}
		session(['admin_token'=> '']);
		return response()->json(['message' => 'Log out success'], 200);
	}
}
