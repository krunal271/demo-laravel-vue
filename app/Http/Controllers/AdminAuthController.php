<?php

namespace App\Http\Controllers;

use Config;
use Illuminate\Http\Request;
use JWTAuth;
use Tymon\JWTAuth\Exceptions\JWTException;
use App\Models\AdminUser;
use App\Api\Traits\LearningTrait;

class AdminAuthController extends Controller {
	
	use LearningTrait;

	public function __construct() {
		Config::set('jwt.user', 'App\Models\AdminUser');
		Config::set('jwt.identifier', 'adminUserId');
		Config::set('auth.defaults.guard', 'admin');
	}
	public function authenticate(Request $request) {
		// dd(config('jwt.identifier'),config('auth.providers.users.model'),'test');
		// grab credentials from the request

		$credentials = $request->only('email', 'password');
		// dd(JWTAuth::attempt($credentials));
		try {

			// attempt to verify the credentials and create a token for the user
			if (!$token = JWTAuth::attempt($credentials)) {
				return response()->json(['error' => 'The login details entered are invalid. If you have forgotten your password please follow the forgot password process.'], 401);
			}
		} catch (JWTException $e) {
			// something went wrong whilst attempting to encode the token
			return response()->json(['error' => 'could_not_create_token'], 500);
		}
		$userData = JWTAuth::toUser($token);
		return response()->json(compact('token', 'userData'));
	}

	public function check() {

		\Log::info('Check Method is called');
		try {
			JWTAuth::parseToken()->authenticate();
		} catch (JWTException $e) {
			return response(['authenticated' => false]);
		}
		\Log::info('After Authenticate');

		// Here Add Functionality if use is Active then allowed to login
		$token = JWTAuth::getToken();

		\Log::info('After Getting token');
		if ($token) {
			$userData = JWTAuth::toUser($token);
			// dd($userData);
			if ($userData == false) {
				return response(['authenticated' => false, 'userData' => $userData]);
			}
			\Log::info('Success');
			return response(['authenticated' => true, 'userData' => $userData]);
		}

		\Log::info('NOT GETTING TOKEN');
	}

	public function logout() {
		try {
			$token = JWTAuth::getToken();

			if ($token) {

				JWTAuth::invalidate($token);
			}

		} catch (JWTException $e) {
			return response()->json($e->getMessage(), 401);
		}

		return response()->json(['message' => 'Log out success'], 200);
	}

}
