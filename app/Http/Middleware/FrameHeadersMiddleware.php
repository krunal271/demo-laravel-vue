<?php

namespace App\Http\Middleware;

use Closure;

class FrameHeadersMiddleware {
	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request  $request
	 * @param  \Closure  $next
	 * @return mixed
	 */
	public function handle($request, Closure $next) {
		$response = $next($request);
		$response->header('x-content-type-options', 'nosniff');
		$response->header('X-Frame-Options', 'sameorigin');
		$response->header('x-xss-protection', '1; mode=block');
		$response->header('x-permitted-cross-domain-policies', 'none');
		$response->header('Strict-Transport-Security', 'max-age=16070400');
	return $response;
	}
}