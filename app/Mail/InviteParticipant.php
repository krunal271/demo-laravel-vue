<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class InviteParticipant extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data)
    {
        $this->mailData = $data;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from($this->mailData['FROM'],$this->mailData['TITLE'])->view('email-page.invite-participant')->subject($this->mailData['SUBJECT'])->with([
            'name'=> $this->mailData['NAME'],
            'first_name'=> $this->mailData['FIRST_NAME'],
            'last_name'=> $this->mailData['LAST_NAME'],
            'resetLink' => $this->mailData['LINK'],
        ]);
    }
}
