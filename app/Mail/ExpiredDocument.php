<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ExpiredDocument extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    protected $mailData;

    public function __construct($data)
    {
        $this->mailData = $data;
        //
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from($this->mailData['EMAIL'],'DEEP')->view('email-page.document-expired')->subject($this->mailData['SUBJECT'])->with([
            'name'=> $this->mailData['NAME'],
            'docName' => $this->mailData['DOCNAME'],
        ]);
    }
}
