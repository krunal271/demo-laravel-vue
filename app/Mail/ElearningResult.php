<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Contracts\Queue\ShouldQueue;

class ElearningResult extends Mailable
{
    use Queueable, SerializesModels;

    private $user;
    private $type;

    public function __construct($user,$type)
    {
        $this->user = $user;
        $this->type = $type;
    }

    
    public function build()
    {
        return $this->from($this->user['email'],'DEEP')->subject('eLearning Successful')->view('emails.eLearningResult',['user'=> $this->user,'type'=> $this->type]);
    }
}
