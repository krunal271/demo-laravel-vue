const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */
var pluginPath =  'resources/plugins/';
//Admin Plugins
mix.combine([
    // ** Required Plugins **
    //for jQuery plugins
    pluginPath + 'jquery/jquery.js',
    //for bootstrap 
    pluginPath + 'bootstrap/tether.js',
    pluginPath + 'popper.min.js',
    pluginPath + 'bootstrap.min.js',
    //for datatable bootstrap 
    // pluginPath + 'datatables/dataTables.bootstrap4.js',
    pluginPath + 'datepicker/dist/js/bootstrap-datepicker.js',
    //for select 2 js
    pluginPath + 'select2/dist/js/select2.full.min.js',

    pluginPath + 'toastr/toastr.js',
    pluginPath + 'icheck/icheck.min.js',

],'public/assets/js/core/admin/plugins.js')

//Front Plugins
mix.combine([
    // ** Required Plugins **
    pluginPath + 'jquery/jquery-3.3.1.js',
    pluginPath + 'datepicker/dist/js/bootstrap-datepicker.js',
    pluginPath + 'bootstrap/tether.js',
    pluginPath + 'popper.min.js',
    pluginPath + 'bootstrap.min.js',
    pluginPath + 'fileupload.js',
    pluginPath + 'jquery.fileuploader.min.js',
    pluginPath + 'moment.min.js',
    pluginPath + 'select2/dist/js/select2.full.min.js',
    pluginPath + 'calendar/dist/fullcalendar.js',
    pluginPath + 'bootstrap-datetimepicker.min.js',
    pluginPath + 'custom/custom.js',
    pluginPath + 'toastr/toastr.js',
    pluginPath + 'fullcalendar/lib/moment.min.js',
    pluginPath + 'icheck/icheck.min.js',
    pluginPath + 'custom/axios.min.js',
],'public/assets/js/core/front/plugins.js')

.js('resources/js/app.js', 'public/js')
.sass('resources/sass/admin/app.scss', 'public/css/admin')
.sass('resources/sass/front/app.scss', 'public/css/front')
.version();
 