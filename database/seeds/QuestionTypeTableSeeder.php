<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class QuestionTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::statement('SET FOREIGN_KEY_CHECKS=0');
		DB::table('question_type')->truncate();
		DB::statement('SET FOREIGN_KEY_CHECKS=1');
		DB::table('question_type')->insert([
			[
				'type' => 'General questions',
				'slug' => 'gen_que',
				'status' => 'active',
				'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
				'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
			],
			[
				'type' => 'Programs specific questions',
				'slug' => 'prog_que',
				'status' => 'active',
				'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
				'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
			],
			[
				'type' => 'Volunteer questions',
				'slug' => 'volunt_que',
				'status' => 'active',
				'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
				'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
			],
			[
				'type' => 'Child Safeguarding Reference Check Questions',
				'slug' => 'child_que',
				'status' => 'active',
				'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
				'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
			],
		]);
    }
}
