<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder {
	/**
	 * Seed the application's database.
	 *
	 * @return void
	 */
	public function run() {

		$this->call(UserTypeTableSeeder::class);
		$this->call(AdminTableSeeder::class);
		$this->call(UserCategoryTableSeeder::class);
		$this->call(DepartmentTableSeeder::class);
		$this->call(UserTableSeeder::class);
		$this->call(UserDetailTableSeeder::class);
		$this->call(UserDocumentTypeTableSeeder::class);
		$this->call(QuestionTypeTableSeeder::class);
		$this->call(QuestionTableSeeder::class);
	}
}
