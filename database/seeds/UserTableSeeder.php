<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder {
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run() {
		// Foreign key checks disable for truncate table
		// * @author     Krunal
		DB::statement('SET FOREIGN_KEY_CHECKS=0');
		DB::table('user')->truncate();
		DB::statement('SET FOREIGN_KEY_CHECKS=1');

		DB::table('user')->insert([
			[
				'userTypeId' => 2,
				'email' => 'admin1@gmail.com',
				'password' => Hash::make(trim('12345678')),
				'orgEmail' => 'corigami_admin@gmail.com',
				'departmentId' => null,
				'userCategoryId'=>null,
				'status' => 'active',
				'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
				'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
			],
			[
				'userTypeId' => 3,
				'email' => 'hmu@gmail.com',
				'password' => Hash::make(trim('12345678')),
				'orgEmail' => 'hmu@gmail.com',
				'departmentId' => null,
				'userCategoryId'=>null,
				'status' => 'active',
				'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
				'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
			],
			[
				'userTypeId' => 4,
				'email' => 'participant@gmail.com',
				'password' => Hash::make(trim('12345678')),
				'orgEmail' => 'participant@gmail.com',
				'departmentId' => '1',
				'userCategoryId'=> '1',
				'status' => 'active',
				'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
				'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
			],
			[
				'userTypeId' => 2,
				'email' => 'krunal.parikh271@gmail.com',
				'password' => Hash::make(trim('12345678')),
				'orgEmail' => 'krunal.parikh271@gmail.com',
				'departmentId' => '1',
				'userCategoryId'=> '1',
				'status' => 'active',
				'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
				'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
			],

		]);
	}
}
