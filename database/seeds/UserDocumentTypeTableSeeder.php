<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class UserDocumentTypeTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::statement('SET FOREIGN_KEY_CHECKS=0');
		DB::table('user_document_type')->truncate();
		DB::statement('SET FOREIGN_KEY_CHECKS=1');

		DB::table('user_document_type')->insert([
			[
				'slug' => 'policeCheck',
				'type' => 'Police Check',
				'helpText' => 'A \'nationally coordinated criminal history check\' describes both: the checking process undertaken by the ACIC and police, and the result received by the accredited body.
					Commonly known as a ‘police check’.',
				'status' => 'active',
				'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
				'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
				
			],
			[
				'slug' => 'workingWithChildrenCheck',
				'type' => 'Working with Children Check',
				'helpText' => 'A working with children\'s check is a state based screening clearance process for those working or volunteering with children.  Individual states have their own requirements and check/card names.',
				'status' => 'active',
				'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
				'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
			],
			[
				'slug' => 'idDocument',
				'type' => 'ID Document',
				'helpText' => 'Upload documents like Drivers licence , Passport,Birth Certificate,Certificate of identity,Document of identity,Evidence of resident status.',
				'status' => 'active',
				'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
				'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
			],
			[
				'slug' => 'statutoryDeclaration',
				'type' => 'Statutory Declaration',
				'helpText' => '', 
				'status' => 'active',
				'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
				'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
			],
			[
				'slug' => 'contract',
				'type' => 'Contract',
				'helpInfo' => '', 
				'status' => 'active',
				'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
				'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
			]
			

			
		]);
    }
}
