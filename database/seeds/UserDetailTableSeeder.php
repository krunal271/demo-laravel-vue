<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;

class UserDetailTableSeeder extends Seeder {
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run() {
		// Foreign key checks disable for truncate table
		// * @author     Krunal
		DB::statement('SET FOREIGN_KEY_CHECKS=0');
		DB::table('user_detail')->truncate();
		DB::statement('SET FOREIGN_KEY_CHECKS=1');

		DB::table('user_detail')->insert([
			[
				'userId' => 1,
				'givenName' => 'Admin',
				'surName' => 'Admin',
				'prefferedName' => 'Admin',
				'dateOfBirth' => '1990-11-02',
				'contactPhone' => '61|2345678901',
				'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
				'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
			],
			[
				'userId' => 2,
				'givenName' => 'HMU',
				'surName' => 'HMU',
				'prefferedName' => 'HMU',
				'dateOfBirth' => '1990-11-03',
				'contactPhone' => '61|2345678902',
				'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
				'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
			],
			[
				'userId' => 3,
				'givenName'=>'Participant',
				'surName'=>'Participant',
				'prefferedName'=>'Participant',
				'dateOfBirth' => '1990-11-04',
				'contactPhone' => '61|2345678903',
				'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
				'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
			],

		]);
	}
}
