<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;

class UserCategoryTableSeeder extends Seeder {
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run() {
		// Foreign key checks disable for truncate table
		// * @author     Krunal
		DB::statement('SET FOREIGN_KEY_CHECKS=0');
		DB::table('user_category')->truncate();
		DB::statement('SET FOREIGN_KEY_CHECKS=1');

		DB::table('user_category')->insert([
			[
				'userTypeId' => 4,
				'type' => 'Employee (Ongoing)',
				'status' => 'active',
				'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
				'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
			],
			[
				'userTypeId' => 4,
				'type' => 'Employee (Fixed Term)',
				'status' => 'active',
				'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
				'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
			],
			[
				'userTypeId' => 4,
				'type' => 'Employee (Casual)',
				'status' => 'active',
				'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
				'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
			],
			[
				'userTypeId' => 4,
				'type' => 'Volunteers',
				'status' => 'active',
				'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
				'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
			],
			[
				'userTypeId' => 4,
				'type' => 'Contractor',
				'status' => 'active',
				'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
				'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
			],
		]);
	}
}
