<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;

class AdminTableSeeder extends Seeder {
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run() {
		// Foreign key checks disable for truncate table
		// * @author     Krunal
		DB::statement('SET FOREIGN_KEY_CHECKS=0');
		DB::table('admin_user')->truncate();
		DB::statement('SET FOREIGN_KEY_CHECKS=1');

		DB::table('admin_user')->truncate();

		DB::table('admin_user')->insert([
			[
				'userTypeId' => 1,
				'email' => 'super_admin@gmail.com',
				'givenName' => 'Admin',
				'surName' => 'Admin',
				'prefferedName' => 'Admin',
				'password' => Hash::make(trim('12345678')),
				'status' => 'active',
				'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
				'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
			],

		]);
	}
}
