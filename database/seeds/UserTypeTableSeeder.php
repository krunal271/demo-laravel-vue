<?php

use Carbon\Carbon;
use Illuminate\Database\Seeder;

class UserTypeTableSeeder extends Seeder {
	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */

	public function run() {
		// Foreign key checks disable for truncate table
		// * @author     Krunal
		DB::statement('SET FOREIGN_KEY_CHECKS=0');
		DB::table('user_type')->truncate();
		DB::statement('SET FOREIGN_KEY_CHECKS=1');

		DB::table('user_type')->insert([
			[
				'type' => 'SuperAdmin',
				'status' => 'active',
				'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
				'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
			],
			[
				'type' => 'Admin',
				'status' => 'active',
				'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
				'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
			],
			[
				'type' => 'HMU',
				'status' => 'active',
				'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
				'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
			],
			[
				'type' => 'Participant',
				'status' => 'active',
				'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
				'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
			],
		]);
	}
}
