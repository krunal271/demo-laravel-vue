<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class QuestionTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::statement('SET FOREIGN_KEY_CHECKS=0');
		DB::table('question')->truncate();
		DB::statement('SET FOREIGN_KEY_CHECKS=1');
		DB::table('question')->insert([
			[
				'questionTypeId' => 1,
				'question' => 'How do your professional values align with working in a child rights organisation?',
				/*'ansType' => '',*/
				'allowComment' => 'Y',
				'status' => 'active',
				'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
				'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
			],
			[
				'questionTypeId' => 1,
				'question' => 'What steps would you take if you became aware of a breach of the Safeguarding Children and Young People policy at work?',
				/*'ansType' => '',*/
				'allowComment' => 'Y',
				'status' => 'active',
				'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
				'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
			],
			[
				'questionTypeId' => 1,
				'question' => 'Have you ever worked for an organisation with a  child safeguarding policy in place? If so, What were the requirements/responsibilities placed on staff?',
				/*'ansType' => '',*/
				'allowComment' => 'Y',
				'status' => 'active',
				'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
				'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
			],
			[
				'questionTypeId' => 1,
				'question' => 'Give me an example of how you have improved/implemented child safe practices/child safeguarding procedures where you work/in your previous employment.',
				/*'ansType' => '',*/
				'allowComment' => 'Y',
				'status' => 'active',
				'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
				'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
			],
			[
				'questionTypeId' => 1,
				'question' => 'Can you give me an example of when you had a concern about the welfare/safety of a child?  (Follow up with: What steps did you take to protect the child? Who did you involve? If you didn’t take any steps, what prevented you? In retrospect what could you have done differently)',
				/*'ansType' => '',*/
				'allowComment' => 'Y',
				'status' => 'active',
				'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
				'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
			],
			[
				'questionTypeId' => 1,
				'question' => 'What would you do if a member of staff, child or community member reported concerns relating to a child safeguarding issue?',
				/*'ansType' => '',*/
				'allowComment' => 'Y',
				'status' => 'active',
				'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
				'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
			],
			[
				'questionTypeId' => 2,
				'question' => 'Based on your experience of work overseas [in context of post being interviewed for e.g. emergency context], in what ways do you think children are vulnerable to abuse and exploitation?',
				/*'ansType' => '',*/
				'allowComment' => 'Y',
				'status' => 'active',
				'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
				'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
			],
			[
				'questionTypeId' => 2,
				'question' => 'You are being posted to a location where there is an emergency situation. During such time children are much more vulnerable to risks of harm and abuse even from aid workers/peacekeepers. If you got the job what steps would be important for you to take to ensure that children are not at risk by PIA’s staff, partners or other aid workers?',
				/*'ansType' => '',*/
				'allowComment' => 'Y',
				'status' => 'active',
				'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
				'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
			],
			[
				'questionTypeId' => 2,
				'question' => 'What is your experience of child safeguarding in [context of post being interviewed for e.g. an emergency situation]?',
				/*'ansType' => '',*/
				'allowComment' => 'Y',
				'status' => 'active',
				'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
				'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
			],
			[
				'questionTypeId' => 2,
				'question' => 'What are your particular strengths in relation to working with children in general and/or in an emergency context?',
				/*'ansType' => '',*/
				'allowComment' => 'Y',
				'status' => 'active',
				'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
				'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
			],
			[
				'questionTypeId' => 3,
				'question' => 'If you were reading a letter from a sponsor to a sponsor child and checking it for child safeguarding concerns, what topics or words/phrases would be red flags?',
				/*'ansType' => '',*/
				'allowComment' => 'Y',
				'status' => 'active',
				'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
				'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
			],
			[
				'questionTypeId' => 3,
				'question' => 'As a volunteer you may be required to read letters from a sponsor to a sponsor child before they are forwarded, why do you think it’s important for us to check those letters?',
				/*'ansType' => '',*/
				'allowComment' => 'Y',
				'status' => 'active',
				'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
				'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
			],
			[
				'questionTypeId' => 3,
				'question' => 'What safeguards/stopgaps do you think PIA need to put in place to protect children when working with sponsors? Why?',
				/*'ansType' => '',*/
				'allowComment' => 'Y',
				'status' => 'active',
				'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
				'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
			],
			[
				'questionTypeId' => 3,
				'question' => 'What are your particular strengths in relation to working with children in general and/or in an emergency context?',
				/*'ansType' => '',*/
				'allowComment' => 'Y',
				'status' => 'active',
				'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
				'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
			],
			[
				'questionTypeId' => 4,
				'question' => 'Did you ever have any reason to be concerned about this person’s behaviour with children?',
				/*'ansType' => '',*/
				'allowComment' => 'Y',
				'status' => 'active',
				'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
				'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
			],
			[
				'questionTypeId' => 4,
				'question' => 'Were there ever any complaints from staff, children or parents about this person? (Regardless of whether they were substantiated)',
				/*'ansType' => '',*/
				'allowComment' => 'Y',
				'status' => 'active',
				'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
				'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
			],
			[
				'questionTypeId' => 4,
				'question' => 'Do you know of any issues or incidents involving the candidate and their contact with children?',
				/*'ansType' => '',*/
				'allowComment' => 'Y',
				'status' => 'active',
				'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
				'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
			],
			[
				'questionTypeId' => 4,
				'question' => 'Are you comfortable knowing the applicant could be working alone with children at times?',
				/*'ansType' => '',*/
				'allowComment' => 'Y',
				'status' => 'active',
				'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
				'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
			],
			[
				'questionTypeId' => 4,
				'question' => 'Why would this person be a good candidate for working with children? Is there any reason this person should not work with children?',
				/*'ansType' => '',*/
				'allowComment' => 'Y',
				'status' => 'active',
				'created_at' => Carbon::now()->format('Y-m-d H:i:s'),
				'updated_at' => Carbon::now()->format('Y-m-d H:i:s'),
			],
		]);
    }
}
