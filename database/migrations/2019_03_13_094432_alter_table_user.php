<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableUser extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user', function (Blueprint $table) {
            $table->enum('isHmu',['0','1'])->after('inviteUser')->nullable();
            $table->timestamp('hmuDate')->after('isHmu')->nullable();
            $table->enum('isAdmin',['0','1'])->after('hmuDate')->nullable();
            $table->timestamp('adminDate')->after('isAdmin')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
