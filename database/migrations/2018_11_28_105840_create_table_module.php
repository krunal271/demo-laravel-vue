<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableModule extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('module', function (Blueprint $table) {
            $table->increments('moduleId');
            $table->string('moduleName',200)->nullable();
            $table->integer('allowTotalAttempt')->length(5)->nullable();
            $table->integer('passingScore')->length(5)->nullable();
            $table->string('image',255)->nullable();
            $table->text('description')->nullable();
            $table->enum('status', ['active', 'inactive'])->nullable();
            $table->integer('createdBy')->nullable();
            $table->integer('updatedBy')->nullable();
            $table->integer('deletedBy')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('module');
    }
}
