<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Eloquent\SoftDeletes;

class CreateUserEnrollment extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_enrollment', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('enrollmentId');
            $table->string('go1UserId', 255);
            $table->string('loId', 255)->nullable();
            $table->string('parentLoId', 255)->nullable();
            $table->string('parentEnrollmentId', 255)->nullable();
            $table->enum('status', ['assigned','completed','expired','in-progress','not-started','pending'])->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_enrollment');
    }
}
