<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableSystemLog extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('system_log', function (Blueprint $table) {
            $table->increments('systemLogId');
            $table->integer('userId')->unsigned();
            $table->text('details')->nullable();
            $table->string('tableName',30)->nullable();
            $table->string('fieldName',20)->nullable();
            $table->text('oldValue')->nullable();
            $table->text('newValue')->nullable();
            $table->integer('createdBy')->nullable();
            $table->integer('updatedBy')->nullable();
            $table->integer('deletedBy')->nullable();
            $table->timestamps();
            $table->softDeletes();
            //for foreign keys fields
            $table->foreign('userId')->references('userId')->on('user')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        Schema::dropIfExists('system_log');
        DB::statement('SET FOREIGN_KEY_CHECKS = 1');
    }
}
