<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableModuleResult extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('module_result', function (Blueprint $table) {
            $table->increments('moduleResultId');
            $table->integer('userId')->unsigned();
            $table->integer('moduleId')->unsigned();
            $table->integer('passing_score_percentage')->length(5)->nullable();
            $table->integer('gained_score_percentage')->length(5)->nullable();
            $table->date('resultDate')->nullable();
            $table->time('resultTime')->nullable();
            $table->string('bookmark',200)->nullable();
            $table->integer('noOfAttempt')->length(5)->nullable();
            $table->integer('total_points')->unsigned()->nullable();
            $table->integer('passing_points')->unsigned()->nullable();
            $table->integer('gained_points')->unsigned()->nullable();
            $table->enum('moduleResultStatus', ['in_progress', 'satisfactory','reattempt'])->nullable();
            $table->integer('createdBy')->nullable();
            $table->integer('updatedBy')->nullable();
            $table->integer('deletedBy')->nullable();
            $table->timestamps();
            $table->softDeletes();
            //for foreign keys fields
            $table->foreign('userId')->references('userId')->on('user')->onDelete('cascade');
            $table->foreign('moduleId')->references('moduleId')->on('module')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        Schema::dropIfExists('module_result');
        DB::statement('SET FOREIGN_KEY_CHECKS = 1');
    }
}
