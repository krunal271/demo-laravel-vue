<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableQuestion extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('question', function (Blueprint $table) {
            $table->increments('questionId');
            $table->integer('questionTypeId')->unsigned();
            $table->text('question')->nullable(); 
            $table->enum('ansType', ['multiple_option', 'text','single_option'])->nullable();
            $table->enum('allowComment', ['Y', 'N'])->nullable();
            $table->enum('status', ['active', 'inactive'])->nullable();
            $table->integer('createdBy')->nullable();
            $table->integer('updatedBy')->nullable();
            $table->integer('deletedBy')->nullable();
            $table->timestamps();
            $table->softDeletes();
            //for foreign keys fields
            $table->foreign('questionTypeId')->references('questionTypeId')->on('question_type')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        Schema::dropIfExists('question');
        DB::statement('SET FOREIGN_KEY_CHECKS = 1');
    }
}
