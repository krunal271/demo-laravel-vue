<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class UserStatutoryDeclaration extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_statutory_declaration', function (Blueprint $table) {
            $table->increments('statutoryDeclarationId');
            $table->integer('userId')->length(11);
            $table->string('fullName',255)->nullable();
            $table->string('address',255)->nullable();
            $table->string('occupation',255)->nullable();
            $table->string('declaration',255)->nullable();
            $table->string('witnessFullName',255)->nullable();
            $table->string('declarationDocument',255)->nullable();
            $table->integer('createdBy')->nullable();
            $table->integer('updatedBy')->nullable();
            $table->integer('deletedBy')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });    
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_statutory_declaration');
    }
}
