<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableNotification extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('notification', function (Blueprint $table) {
            $table->enum('participantRead', ['read', 'unread'])->after('notificationStatus')->nullable();
            $table->enum('hmuRead', ['read', 'unread'])->after('participantRead')->nullable();
            $table->enum('adminRead', ['read', 'unread'])->after('hmuRead')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
