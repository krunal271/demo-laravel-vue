<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableUserDetail extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('user_detail', function (Blueprint $table) {
            $table->string('speakOtherLanguageComment')->after('speakOtherLanguage')->nullable();
            $table->string('parentsOutsideAustraliaComment')->after('parentsOutsideAustralia')->nullable();
            $table->string('bornOutsideAustraliaComment')->after('bornOutsideustralia')->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
