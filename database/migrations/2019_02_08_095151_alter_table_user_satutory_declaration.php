<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class AlterTableUserSatutoryDeclaration extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
         Schema::table('user_statutory_declaration', function (Blueprint $table) {
            $table->integer('userDocumentId')->after('declarationDocument')->unsigned();
            $table->foreign('userDocumentId')->references('userDocumentId')->on('user_document')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
