<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableAdminUser extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('admin_user', function (Blueprint $table) {
            $table->increments('adminUserId');
            $table->integer('userTypeId')->unsigned();
            $table->string('username',50)->nullable();
            $table->string('email',55)->nullable();
            $table->string('password',64)->nullable();
            $table->string('givenName',55)->nullable();
            $table->string('surName',55)->nullable();
            $table->string('prefferedName',55)->nullable();
            $table->string('contactPhone',20)->nullable();
            $table->enum('status', ['active', 'inactive'])->nullable();
            $table->integer('createdBy')->nullable();
            $table->integer('updatedBy')->nullable();
            $table->integer('deletedBy')->nullable();
            $table->timestamps();
            $table->softDeletes();
            //for foreign keys fields
            $table->foreign('userTypeId')->references('userTypeId')->on('user_type')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        Schema::dropIfExists('admin_user');
        DB::statement('SET FOREIGN_KEY_CHECKS = 1');
    }
}
