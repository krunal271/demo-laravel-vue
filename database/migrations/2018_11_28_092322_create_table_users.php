<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user', function (Blueprint $table) {
            $table->increments('userId');
            $table->integer('userTypeId')->unsigned()->nullable();
            $table->integer('userCategoryId')->unsigned()->nullable();
            $table->integer('departmentId')->unsigned()->nullable();
            $table->integer('hiringManager')->unsigned()->nullable();
            $table->string('email',55)->nullable();
            $table->char('password',128)->nullable();
            $table->string('orgEmail',55)->nullable();
            $table->string('role',55)->nullable();
            $table->enum('engagementStatus', ['applicationInProgress','active', 'inactive','not_compliant'])->nullable();
            $table->date('profileCompleteDate')->nullable();
            $table->string('profileCompleteTime',20)->nullable();
            $table->enum('participantConfirmed', ['Y', 'N'])->nullable();
            $table->enum('confirmPolicy', ['Y','N'])->nullable();
            $table->enum('inviteUser', ['Y', 'N'])->nullable();
            $table->enum('status', ['active', 'inactive'])->nullable();
            $table->integer('createdBy')->nullable();
            $table->integer('updatedBy')->nullable();
            $table->integer('deletedBy')->nullable();
            $table->timestamps();
            $table->softDeletes();
            //for foreign keys fields
            $table->foreign('userTypeId')->references('userTypeId')->on('user_type')->onDelete('cascade');
            $table->foreign('userCategoryId')->references('userCategoryId')->on('user_category')->onDelete('cascade');
            $table->foreign('departmentId')->references('departmentId')->on('department')->onDelete('cascade');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        Schema::dropIfExists('user');
        DB::statement('SET FOREIGN_KEY_CHECKS = 1');
    }
}
