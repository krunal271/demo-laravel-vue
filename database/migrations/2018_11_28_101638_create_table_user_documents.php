<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableUserDocuments extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_document', function (Blueprint $table) {
            $table->increments('userDocumentId');
            $table->integer('userId')->unsigned();
            $table->integer('userDocumentTypeId')->unsigned();
            $table->date('expiryDate')->nullable();
            $table->string('name',60)->nullable();
            $table->string('documentNumber',20)->nullable();
            $table->enum('verifiedStatus', ['in_progress', 'not_required','satisfactory','not_satisfactory','due','expired','uploaded'])->default('in_progress')->nullable();
            $table->integer('verifiedBy')->nullable();
            $table->integer('createdBy')->nullable();
            $table->integer('updatedBy')->nullable();
            $table->integer('deletedBy')->nullable();
            $table->timestamps();
            $table->softDeletes();
            //for foreign keys fields
            $table->foreign('userId')->references('userId')->on('user')->onDelete('cascade');
            $table->foreign('userDocumentTypeId')->references('userDocumentTypeId')->on('user_document_type')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        Schema::dropIfExists('user_document');
        DB::statement('SET FOREIGN_KEY_CHECKS = 1');
    }
}
