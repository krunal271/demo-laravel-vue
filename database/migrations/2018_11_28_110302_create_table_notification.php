<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableNotification extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('notification', function (Blueprint $table) {
            $table->increments('notficationId');
            $table->integer('userId')->unsigned();
            $table->integer('viewerId')->unsigned();
            $table->string('type',20)->nullable();
            $table->string('name',60)->nullable();
            $table->string('slug',70)->nullable();
            $table->text('template')->nullable();
            $table->enum('notificationStatus', ['read', 'unread'])->nullable();
            $table->integer('createdBy')->nullable();
            $table->integer('updatedBy')->nullable();
            $table->integer('deletedBy')->nullable();
            $table->timestamps();
            $table->softDeletes();
            //for foreign keys fields
            // $table->foreign('userId')->references('userId')->on('user')->onDelete('cascade');
            $table->foreign('viewerId')->references('userId')->on('user')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        Schema::dropIfExists('notification');
        DB::statement('SET FOREIGN_KEY_CHECKS = 1');
    }
}
