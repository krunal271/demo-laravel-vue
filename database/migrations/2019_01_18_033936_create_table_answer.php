<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableAnswer extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('answer', function (Blueprint $table) {
            $table->increments('answerId');
            $table->integer('interviewAndReferenceId')->unsigned();
            $table->integer('questionId')->unsigned();
            $table->integer('userId')->unsigned();
            $table->text('answer')->nullable();
            $table->enum('correctAns', ['Y', 'N'])->nullable();
            $table->enum('interviewStatus', ['in_progress','satisfactory','not_satisfactory'])->default('in_progress')->nullable();
            $table->enum('referanceStatus', ['in_progress','satisfactory','not_satisfactory'])->default('in_progress')->nullable();
            $table->enum('status', ['active', 'inactive'])->nullable();
            $table->integer('createdBy')->nullable();
            $table->integer('updatedBy')->nullable();
            $table->integer('deletedBy')->nullable();
            $table->timestamps();
            $table->softDeletes();
            //for foreign keys fields
            $table->foreign('questionId')->references('questionId')->on('question')->onDelete('cascade');
            $table->foreign('userId')->references('userId')->on('user')->onDelete('cascade');
            $table->foreign('interviewAndReferenceId')->references('interviewAndReferenceId')->on('interview_and_reference')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        Schema::dropIfExists('answer');
        DB::statement('SET FOREIGN_KEY_CHECKS = 1');
    }
}
