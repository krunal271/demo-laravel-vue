<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTableBooking extends Migration {
	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up() {
		Schema::create('booking', function (Blueprint $table) {
			$table->increments('bookingId');
			$table->integer('userId')->unsigned();
			$table->date('bookingDate')->nullable();
			$table->string('bookingTime', 20)->nullable();
			$table->enum('attend', ['Y', 'N'])->nullable();
			$table->enum('status', ['active', 'inactive'])->nullable();
			$table->enum('bookingStatus', ['in_progress', 'satisfactory', 'not_required', 'due', 'overdue', 'expired'])->default('in_progress');
			$table->integer('createdBy')->nullable();
			$table->integer('updatedBy')->nullable();
			$table->integer('deletedBy')->nullable();
			$table->timestamps();
			$table->softDeletes();
			//for foreign keys fields
			$table->foreign('userId')->references('userId')->on('user')->onDelete('cascade');
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
        public function down()
        {
            DB::statement('SET FOREIGN_KEY_CHECKS = 0');
            Schema::dropIfExists('booking');
            DB::statement('SET FOREIGN_KEY_CHECKS = 1');
        }
    
}
