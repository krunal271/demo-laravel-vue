<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableInterviewAndReference extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('interview_and_reference', function (Blueprint $table) {
            $table->increments('interviewAndReferenceId');
            $table->integer('userId')->unsigned();
            $table->date('interviewDate')->nullable();
            $table->date('referenceDate')->nullable();
            $table->enum('interviewStatus', ['in_progress','satisfactory','not_satisfactory'])->default('in_progress')->nullable();
            $table->enum('referenceStatus', ['in_progress','satisfactory','not_satisfactory'])->default('in_progress')->nullable();
            $table->integer('createdBy')->nullable();
            $table->integer('updatedBy')->nullable();
            $table->integer('deletedBy')->nullable();
            $table->timestamps();
            $table->softDeletes();
             //for foreign keys fields
            $table->foreign('userId')->references('userId')->on('user')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        Schema::dropIfExists('interview_and_reference');
        DB::statement('SET FOREIGN_KEY_CHECKS = 1');
    }
}
