<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableUserDetail extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_detail', function (Blueprint $table) {
            $table->increments('userDetailId');
            $table->integer('userId')->unsigned();
            $table->string('givenName',55)->nullable();
            $table->string('surName',55)->nullable();
            $table->string('prefferedName',55)->nullable();

            $table->enum('levelOfContact', ['work_with_child','cont_with_child'])->nullable();
            //$table->enum('pacificBasedRole', ['Y', 'N'])->nullable();
            $table->enum('policeCheque', ['Y', 'N'])->nullable();
            $table->date('dateOfBirth')->nullable();
            $table->string('contactPhone',20)->nullable();
            $table->string('contactPhone2',20)->nullable();
            $table->string('address',120)->nullable();

            $table->enum('speakOtherLanguage', ['Y', 'N'])->nullable();
            $table->enum('parentsOutsideAustralia', ['Y', 'N','prefer_not_to_disclose'])->nullable();
            $table->enum('bornOutsideustralia', ['Y', 'N','prefer_not_to_disclose'])->nullable();
            $table->enum('pacificBasedRole', ['Y', 'N'])->nullable();
            $table->enum('aboriginal', ['no','yes_aborig','yes_islander','yes_aborig_islander','pre_not_dis'])->nullable();
            $table->enum('disability', ['Y', 'N','pre_not_dis'])->nullable();
            $table->enum('gender', ['female', 'male','pre_not_dis','self_desc'])->nullable();

            $table->string('selfDescribe',20)->nullable();
            $table->string('emergencyContactPrimary',55)->nullable();
            $table->string('relationshipPrimary',20)->nullable();
            $table->string('contactPhonePrimary',20)->nullable();
            $table->string('emergencyContactSecondary',55)->nullable();
            $table->string('relationshipSecondary',20)->nullable();
            $table->string('contactPhoneSecondary',20)->nullable();
            $table->enum('candidateLegalWorkAustralia', ['Y', 'N'])->nullable();
            $table->enum('residentialStatus', ['australian_citizen', 'resident','work_visa','other'])->nullable();
            $table->integer('childSafeguardRating')->length(6)->nullable();
            $table->text('visaAttachments')->nullable();
            $table->string('otherResidentialStatus',120)->nullable();
            $table->enum('confirmInformation', ['Y', 'N'])->nullable();
            $table->integer('createdBy')->nullable();
            $table->integer('updatedBy')->nullable();
            $table->integer('deletedBy')->nullable();
            $table->timestamps();
            $table->softDeletes();
            //for foreign keys fields
            $table->foreign('userId')->references('userId')->on('user')->onDelete('cascade');
            
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        Schema::dropIfExists('user_detail');
        DB::statement('SET FOREIGN_KEY_CHECKS = 1');
    }
}
