<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableReferenceCheck extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('reference_check', function (Blueprint $table) {
            $table->increments('referenceCheckId');
            $table->integer('userId')->unsigned();
            $table->integer('questionId')->unsigned();
            $table->integer('answerId')->unsigned();
            $table->text('comment')->nullable();
            $table->enum('refferanceCheckstatus', ['in_progress', 'not_satisfactory','satisfactory'])->nullable();
            $table->date('refferanceCheckDate')->nullable();
            $table->enum('recordInterviewStatus', ['in_progress', 'not_satisfactory','satisfactory'])->nullable();
            $table->date('recordInterviewDate')->nullable();
            $table->integer('createdBy')->nullable();
            $table->integer('updatedBy')->nullable();
            $table->integer('deletedBy')->nullable();
            $table->timestamps();
            $table->softDeletes();
            //for foreign keys fields
            $table->foreign('userId')->references('userId')->on('user')->onDelete('cascade');
            $table->foreign('questionId')->references('questionId')->on('question')->onDelete('cascade');
            $table->foreign('answerId')->references('answerId')->on('answer')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        Schema::dropIfExists('reference_check');
        DB::statement('SET FOREIGN_KEY_CHECKS = 1');
    }
}
