<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableEmailManagement extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('email_management', function (Blueprint $table) {
            $table->increments('emailManagmentId');
            $table->integer('emailTemplateId')->unsigned();
            $table->string('type',50)->nullable();
            $table->string('to',55)->nullable();
            $table->string('from',55)->nullable();
            $table->enum('send', ['Y', 'N'])->nullable();
            $table->text('systemError')->nullable();
            $table->enum('resend', ['Y', 'N'])->nullable();
            $table->enum('status', ['active', 'inactive'])->nullable();
            $table->integer('createdBy')->nullable();
            $table->integer('updatedBy')->nullable();
            $table->integer('deletedBy')->nullable();
            $table->timestamps();
            $table->softDeletes();
            //for foreign keys fields
            $table->foreign('emailTemplateId')->references('emailTemplateId')->on('email_template')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');
        Schema::dropIfExists('email_management');
        DB::statement('SET FOREIGN_KEY_CHECKS = 1');
    }
}
