<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::group(['prefix' => 'auth'], function () {
    Route::post('login','AuthController@authenticate');
    Route::get('logout','AuthController@logout');
    Route::get('check','AuthController@check');

//admin Auth check
    Route::post('admin/login','AdminAuthController@authenticate');

    Route::get('admin/check','AdminAuthController@check');
});
Route::post('password/reset/link', '\App\Api\Admin\Controllers\Auth\ForgotPasswordController@resetLink');
Route::post('front/password/sendreset/link', '\App\Api\Front\Controllers\Auth\ForgotPasswordController@resetLink');

Route::post('front/password/update', '\App\Api\Front\Controllers\Auth\ResetPasswordController@reset');
Route::post('admin/password/update', '\App\Api\Admin\Controllers\Auth\ResetPasswordController@reset');


// Route::get('admin/password/reset/{token}', '\App\Api\Admin\Controllers\Auth\ResetPasswordController@showResetForm')->name('password.reset');

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

//for api calling
$api = app('Dingo\Api\Routing\Router');


$api->version('v1', function ($api) {
    $api->post('/elearning/geteLearningModules','\App\Api\Front\Controllers\ModuleController@getModuleList');    
    $api->post('/elearning/module-complete/{hiddenName}','\App\Api\Front\Controllers\ModuleController@completeModule');
    $api->post('/elearning/startModule','\App\Api\Front\Controllers\ModuleController@startModule');    
    $api->post('/elearning/check-attempt','\App\Api\Front\Controllers\ModuleController@checkAttempt');    
});

$api->version('v1', function ($api) {

     // for localization
    $locale = \Request::header('locale');

    if($locale != '') {
        App::setLocale($locale);
    }
    
    $api->post('admin/password/reset/{token}','\App\Api\Admin\Controllers\Auth\ResetPasswordController@checkPasswordReset');

    $api->post('front/password/reset/{token}','\App\Api\Front\Controllers\Auth\ResetPasswordController@checkPasswordReset');
       

    //for user type list in select box
    $api->post('userType/getUserTypeSelectList','\App\Api\Admin\Controllers\UserTypeController@getUserTypeSelectList');
    //$api->post('userType/getUserTypeSelectList','\App\Api\Admin\Controllers\UserTypeController@getUserTypeSelectList');
    //for user category list by user type id in select box
    $api->post('userCategory/getUserCategorySelectListByUserTypeId','\App\Api\Admin\Controllers\UserCategoryController@getUserCategorySelectListByUserTypeId');
    //for depart ment list by usertype id in select box 
    $api->post('department/getDepartmentSelectListByUserTypeId','\App\Api\Admin\Controllers\DepartmentController@getDepartmentSelectListByUserTypeId');
    //get hireing manager user in select box
    $api->post('user/getHiringManagerSelectList','\App\Api\Admin\Controllers\UserController@getHiringManagerSelectList');
    //create user
    $api->post('user/createUser','\App\Api\Admin\Controllers\UserController@createUser');

    //for user list
    $api->post('user/getUserListPagination','\App\Api\Admin\Controllers\UserController@getUserListPagination');
    //for getting userinfo by user Id
    $api->post('user/getUserDetailsByUserId','\App\Api\Admin\Controllers\UserController@getUserDetailsByUserId');
    //edit user
    $api->post('user/editUser','\App\Api\Admin\Controllers\UserController@editUser');
    //delete user
    $api->post('user/deleteUserByUserId','\App\Api\Admin\Controllers\UserController@deleteUserByUserId');

    //Delete multiple user by userIds
    $api->post('user/deleteMultipleUserByUserId','\App\Api\Admin\Controllers\UserController@deleteMultipleUserByUserId');
    //Active multiple user by userIds
    $api->post('user/updateMultipleUserStatusByUserId','\App\Api\Admin\Controllers\UserController@updateMultipleUserStatusByUserId');

    //Inactive multiple user by userIds
    $api->post('user/inactiveMultipleUserByUserId','\App\Api\Admin\Controllers\UserController@inactiveMultipleUserByUserId');

    /* User type routes */

    //add user type    
    $api->post('usertype/addUserType','\App\Api\Admin\Controllers\UserTypeController@addUserType');
    //get user types
    $api->post('usertype/getUserTypes','\App\Api\Admin\Controllers\UserTypeController@getUserTypes');
    //delete user type
    $api->post('usertype/removeUserType','\App\Api\Admin\Controllers\UserTypeController@removeUserType');
    //get user type by id
    $api->post('usertype/getUserTypeById','\App\Api\Admin\Controllers\UserTypeController@getUserTypeById');
    //edit user type
    $api->post('usertype/editUserType','\App\Api\Admin\Controllers\UserTypeController@editUserType');
    //search user type
    $api->post('usertype/getSearchUserType','\App\Api\Admin\Controllers\UserTypeController@getSearchUserType');
    // delete multiple user type by id
    $api->post('usertype/deleteMultipleUserTypeById','\App\Api\Admin\Controllers\UserTypeController@deleteMultipleUserTypeById');
    // update multiple user type by id
    $api->post('usertype/updateMultipleUserTypeStatusById','\App\Api\Admin\Controllers\UserTypeController@updateMultipleUserTypeStatusById');
    
    /* User type routes end*/

    /* Department routes */
    $api->post('department/getDepartmentList','\App\Api\Admin\Controllers\DepartmentController@getDepartmentList');
    $api->post('department/addDepartment','\App\Api\Admin\Controllers\DepartmentController@addDepartment');
    $api->post('department/deleteDepartment','\App\Api\Admin\Controllers\DepartmentController@deleteDepartment');
    $api->post('department/deleteMultipleDepartmentById','\App\Api\Admin\Controllers\DepartmentController@deleteMultipleDepartmentById');
    $api->post('department/getDepartmentById','\App\Api\Admin\Controllers\DepartmentController@getDepartmentById');
    $api->post('department/editDepartmentById','\App\Api\Admin\Controllers\DepartmentController@editDepartmentById');
    $api->post('department/updateMultipleDepartmentStatusByDepartmentId','\App\Api\Admin\Controllers\DepartmentController@updateMultipleDepartmentStatusByDepartmentId');
    /* Department end */

    /* Module routes */
    $api->post('module/getModuleList','\App\Api\Admin\Controllers\ModuleController@getModuleList');
    $api->post('module/addModule','\App\Api\Admin\Controllers\ModuleController@addModule');
    $api->post('module/deleteModule','\App\Api\Admin\Controllers\ModuleController@deleteModule');
    $api->post('module/deleteMultipleModuleById','\App\Api\Admin\Controllers\ModuleController@deleteMultipleModuleById');
    $api->post('module/getModuleById','\App\Api\Admin\Controllers\ModuleController@getModuleById');
    $api->post('module/editModuleById','\App\Api\Admin\Controllers\ModuleController@editModuleById');
    $api->post('module/updateMultipleModuleStatusByModuleId','\App\Api\Admin\Controllers\ModuleController@updateMultipleModuleStatusByModuleId');
    /* Module end */

    /* UserCategory routes */
    $api->post('usercategory/getUserCategoryList','\App\Api\Admin\Controllers\UserCategoryController@getUserCategoryList');
    $api->post('usercategory/addUserCategory','\App\Api\Admin\Controllers\UserCategoryController@addUserCategory');
    $api->post('usercategory/deleteUserCategory','\App\Api\Admin\Controllers\UserCategoryController@deleteUserCategory');
    $api->post('usercategory/deleteMultipleUserCategoryById','\App\Api\Admin\Controllers\UserCategoryController@deleteMultipleUserCategoryById');
    $api->post('usercategory/getUserCategoryById','\App\Api\Admin\Controllers\UserCategoryController@getUserCategoryById');
    $api->post('usercategory/editUserCategoryById','\App\Api\Admin\Controllers\UserCategoryController@editUserCategoryById');
    $api->post('usercategory/updateMultipleUserCategoryStatusByUserCategoryId','\App\Api\Admin\Controllers\UserCategoryController@updateMultipleUserCategoryStatusByUserCategoryId');
    /* UserCategory end */

    /* Template routes */
    $api->post('template/getTemplateList','\App\Api\Admin\Controllers\TemplateController@getTemplateList');
    $api->post('template/addTemplate','\App\Api\Admin\Controllers\TemplateController@addTemplate');
    $api->post('template/deleteTemplate','\App\Api\Admin\Controllers\TemplateController@deleteTemplate');
    $api->post('template/deleteMultipleTemplateById','\App\Api\Admin\Controllers\TemplateController@deleteMultipleTemplateById');
    $api->post('template/getTemplateById','\App\Api\Admin\Controllers\TemplateController@getTemplateById');
    $api->post('template/editTemplateById','\App\Api\Admin\Controllers\TemplateController@editTemplateById');
    $api->post('template/updateMultipleTemplateStatusByTemplateId','\App\Api\Admin\Controllers\TemplateController@updateMultipleTemplateStatusByTemplateId');
    /* Template end */

    /* Admin routes */
    $api->post('admin/getAdminList','\App\Api\Admin\Controllers\AdminController@getAdminList');
    $api->post('admin/addAdmin','\App\Api\Admin\Controllers\AdminController@addAdmin');
    $api->post('admin/deleteAdmin','\App\Api\Admin\Controllers\AdminController@deleteAdmin');
    $api->post('admin/deleteMultipleAdminById','\App\Api\Admin\Controllers\AdminController@deleteMultipleAdminById');
    $api->post('admin/getAdminById','\App\Api\Admin\Controllers\AdminController@getAdminById');
    $api->post('admin/editAdminById','\App\Api\Admin\Controllers\AdminController@editAdminById');
    $api->post('admin/updateMultipleAdminStatusByAdminId','\App\Api\Admin\Controllers\AdminController@updateMultipleAdminStatusByAdminId');
    /* Admin end */

    //for Getting booking list with pagination
    $api->post('booking/getBookingListPagination','\App\Api\Front\Controllers\BookingController@getBookingListPagination');

    // for getting participant list in select box
    $api->post('user/getParticipantSelectList','\App\Api\Front\Controllers\UserController@getParticipantSelectList');

    //for add booking data 
    $api->post('booking/createBooking','\App\Api\Front\Controllers\BookingController@createBooking');
    //for delete booking
     $api->post('booking/deleteBookingByBookingId','\App\Api\Front\Controllers\BookingController@deleteBookingByBookingId');
     //for getting info of bookings by booking id 
     $api->post('booking/getBookingDetailsByBookingId','\App\Api\Front\Controllers\BookingController@getBookingDetailsByBookingId');
     //for confirm booking 
     $api->post('booking/addConfirmAttendance','\App\Api\Front\Controllers\BookingController@addConfirmAttendance');
     //for changing status of confirm
     $api->post('booking/changeBookingAttend','\App\Api\Front\Controllers\BookingController@changeBookingAttend');
     //for getting userinfo by user Id
     $api->post('user/getFrontUserDetailsByUserId','\App\Api\Front\Controllers\UserController@getFrontUserDetailsByUserId');
     //for edit participant's personal details
    $api->post('user/participantPersonalDetails','\App\Api\Front\Controllers\UserController@participantPersonalDetails');
    //for getting list of participant document
    $api->post('documents/getDocumentsListByparticipantId','\App\Api\Front\Controllers\DocumentsController@getDocumentsListByparticipantId');
    //for uploading document
    $api->post('documents/addDocumentsUpload','\App\Api\Front\Controllers\DocumentsController@addDocumentsUpload');
    //for uploading multiple document
    $api->post('documents/upload-documents','\App\Api\Front\Controllers\DocumentsController@uploadMultipleDocument');
    //for delete document
    $api->post('document/remove','\App\Api\Front\Controllers\DocumentsController@deleteDocument');
    //update document expiry
    $api->post('document/update','\App\Api\Front\Controllers\DocumentsController@updateDocumentExpiry');
    $api->post('document/admin-update-document','\App\Api\Front\Controllers\DocumentsController@adminUpdateDocumentExpiry');
    //update document status
    $api->post('document/update-status','\App\Api\Front\Controllers\DocumentsController@updateDocumentExpiryStatus');
    // Form Front create participany by different user type  Auth : Mital 
    $api->post('participant/create','\App\Api\Front\Controllers\ParticipantController@create');    

    // Form Front get user list by type  Auth : Mital 
    $api->post('user/list','\App\Api\Front\Controllers\UserController@list');    

    // Form Front get user category list by type  Auth : Mital 
    $api->post('userCategory/list','\App\Api\Front\Controllers\UserCategoryController@list'); 

     // Form Front get user department list by type  Auth : Mital 
    $api->post('userDepartment/list','\App\Api\Front\Controllers\DepartmentController@list');

    //for uploading document
    $api->post('documents/getDocumentsDetailsByDocumentId','\App\Api\Front\Controllers\DocumentsController@getDocumentsDetailsByDocumentId');   

    //get department list
   // $api->post('booking/createBooking','\App\Api\Front\Controllers\DepartmentController@getDepartmentList');    

    /* Front: eLearning modules services */
    //get elearning modules list
    
    //Set module status to in-progress
    
    //Set module status to complete
    
    /* eLearning modules services end */

    //change confirm policy satus for user by user login id
    $api->post('user/changeConfirmPolicy','\App\Api\Front\Controllers\UserController@changeConfirmPolicy');

    //get policy details of user 
    $api->post('user/getPolicyDetailByUserId','\App\Api\Front\Controllers\UserController@getPolicyDetailByUserId');

    // Form Front list participany by different user type  Auth : Mital 
    //$api->post('participant/list','\App\Api\Front\Controllers\ParticipantController@list');    
    $api->post('participant/list','\App\Api\Front\Controllers\UserController@searchQueryData');    
    

    //check registration key for confirm account
    $api->post('user/checkConfirmAccountRegistrationKey','\App\Api\Front\Controllers\UserController@checkConfirmAccountRegistrationKey');

    //get details of user for confirm account
     $api->post('user/getDetailsOfConfirmAccount','\App\Api\Front\Controllers\UserController@getDetailsOfConfirmAccount');

     //get change password and participant confirm account
     $api->post('user/addConfirmAccount','\App\Api\Front\Controllers\UserController@addConfirmAccount');

     // delete participnat from by  different user type  Auth : Mital 
    $api->post('participant/delete','\App\Api\Front\Controllers\ParticipantController@delete');    

     //upload visa attachments file By Richa
      $api->post('user/uploadVisaAttachments','\App\Api\Front\Controllers\UserController@uploadVisaAttachments');

      //remove visa attachments file By Richa
      $api->post('user/removeVisaAttachments','\App\Api\Front\Controllers\UserController@removeVisaAttachments');


      // check already exist email id for user  Auth : Mital 
      $api->post('user/alreadyExistEmail','\App\Api\Front\Controllers\UserController@checkExistEmailId');    

      // search module perticuler page url    Auth : Mital 
      $api->post('user/search','\App\Api\Front\Controllers\UserController@searchQueryData');    


      //change expiry date of documents
      $api->post('documents/changeExpiryDate','\App\Api\Front\Controllers\DocumentsController@changeExpiryDate');

    // check already exist email id for user  Auth : Mital 
    $api->post('user/alreadyExistEmail','\App\Api\Front\Controllers\UserController@checkExistEmailId'); 

    //invite participant to deep 

    $api->post('participant/inviteParticipant','\App\Api\Front\Controllers\ParticipantController@inviteParticipant');
    ////get interview quetion for interview and referances 
    $api->post('interviewAndReferance/getQuestionList','\App\Api\Front\Controllers\InterviewAndReferencesController@getQuestionList');  
    //get dashboard data
    $api->post('admin/dashboard','\App\Api\Admin\Controllers\AdminController@getDashboardData');
    $api->post('admin/module/dashboard','\App\Api\Admin\Controllers\AdminController@getmoduleDashboardData');
    $api->post('admin/participant/dashboard','\App\Api\Admin\Controllers\AdminController@getParticipantDashboardData');
    // get user type name by id  Auth : Mital 
      $api->post('user/get-type-name','\App\Api\Front\Controllers\UserController@getTypeName');    
    //get notification by user 
      $api->post('notification/get-notification-list','\App\Api\Front\Controllers\NotificationController@getAllNotification');    
      

       // add interview and referance
      $api->post('interviewAndReferance/addInterviewAndReferance','\App\Api\Front\Controllers\InterviewAndReferencesController@addInterviewAndReferance');    

    // get participnat's document  detail  Auth : Mital 
    $api->post('documents/list','\App\Api\Front\Controllers\DocumentsController@list');    

    // get document type  list data  Auth : Mital 
    $api->post('documents-type/list','\App\Api\Front\Controllers\DocumentTypeController@list');    
    //get interview and referance info by user id
    $api->post('interviewAndReferance/getInterviewAndReferenceDetailByUserId','\App\Api\Front\Controllers\InterviewAndReferencesController@getInterviewAndReferenceDetailByUserId');

    //get answer list for user
    $api->post('interviewAndReferance/getAnswerListByInterviewAndReferenceId','\App\Api\Front\Controllers\InterviewAndReferencesController@getAnswerListByInterviewAndReferenceId');

    // update status of document manualy  Auth : Mital 
    $api->post('documents/update-status','\App\Api\Front\Controllers\DocumentsController@updateStatus');    

    // update participnat basic detail by HMU or ADMIN  Auth : Mital 
    $api->post('participant/update','\App\Api\Front\Controllers\ParticipantController@update'); 

    // gete participnat basic detail   Auth : Mital 
    $api->post('participant/get-data','\App\Api\Front\Controllers\ParticipantController@getData');


    // get participant e-learning module list 
    $api->post('elearning/participant-module-list','\App\Api\Front\Controllers\ModuleController@getParticipantModuleList');    

    // get participant overall e-learning list
    $api->post('elearning/elearning-status','\App\Api\Front\Controllers\ModuleController@elearningStatus');    

     // get participnat's document  detail 
    $api->post('documents/getDocumentInfoByUserId','\App\Api\Front\Controllers\DocumentsController@getDocumentInfoByUserId');

    //get booking details by participant id
     $api->post('booking/getParticipantBookingDetailByUserId','\App\Api\Front\Controllers\BookingController@getParticipantBookingDetailByUserId');

     //get elearning status for participant login
      $api->post('/elearning/getELearningStatusByParticipantId','\App\Api\Front\Controllers\ModuleController@getELearningStatusByParticipantId');

     // get document status by doc type slug  Auth : Mital 
    $api->post('documents/get-document-status','\App\Api\Front\Controllers\DocumentsController@getdocumentStatus'); 

    // get interview and reference check status by doc type slug  Auth : Mital 
     $api->post('interview-and-referance/status','\App\Api\Front\Controllers\InterviewAndReferencesController@getstatus');
      //get notification by user  for pagination
      $api->post('notification/getNotificationListByPagination','\App\Api\Front\Controllers\NotificationController@getNotificationListByPagination');

    $api->post('participant/engagement-status','\App\Api\Front\Controllers\ParticipantController@updateEngagementStatus');
    $api->post('participant/elearning-invitation','\App\Api\Front\Controllers\ParticipantController@sendeLearningInvition');

    $api->post('notification/read-notification', '\App\Api\Front\Controllers\NotificationController@readNotificationByUser');

        // get user name by id  Auth : Mital 
        $api->post('user/get-name','\App\Api\Front\Controllers\UserController@getName');  

        // get user rating data by id  Auth : Mital 
        $api->post('user/get-rating','\App\Api\Front\Controllers\UserController@getChildSafeGaurdingRating');  

    $api->post('participant/confirm-statutory-declaration', '\App\Api\Front\Controllers\ParticipantController@confirmStatutoryDeclaration');    
    $api->post('participant/get-statutory-declaration', '\App\Api\Front\Controllers\ParticipantController@getStatutoryDeclaration');    
    // get user name by id  Auth : Mital 
    $api->post('participant/statutory-declaration-download','\App\Api\Front\Controllers\ParticipantController@downloadPDF');  

    // get participnat's document  detail  Auth : Mital 
    $api->post('documents/list','\App\Api\Front\Controllers\DocumentsController@list');    

    /**
     * Go1 API routes
     */
    //Get learning content list
    $api->post('learning/learning-contents','\App\Api\Admin\Controllers\LearningController@getLearningContents');
    //Get more details about learning object
    $api->post('learning/learning-details','\App\Api\Admin\Controllers\LearningController@getLearningObjectDetails');
    //Get Default collection learning contents
    $api->post('learning/default-collection','\App\Api\Admin\Controllers\LearningController@getDefaultCollection');
    //Add new content to and enroll users to that course
    $api->post('learning/add-default-collection','\App\Api\Admin\Controllers\LearningController@addToDefaultCollection');
    //Create new user on Go1 Platform
    $api->post('learning/create-user','\App\Api\Front\Controllers\LearningController@createGo1User');
    //Get user enrollments
    $api->post('learning/user-enrollments','\App\Api\Front\Controllers\LearningController@getUserEnrollmentsList');
    //Get user login link
    $api->post('learning/get-user-in','\App\Api\Front\Controllers\LearningController@getUserLoginLink');
    //Get providers list
    $api->post('learning/providers','\App\Api\Admin\Controllers\LearningController@getProviders');
    
    //Get in touch
    $api->post('home/get-in-touch','\App\Api\Front\Controllers\UserController@getInTouch');      
});

