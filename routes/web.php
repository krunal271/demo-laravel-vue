<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });
// Route::get('/{any}', 'WelcomePageController@index')->where('any', '.*');

Route::post('password/email', '\App\Api\Admin\Controllers\Auth\ForgotPasswordController@resetlink' ); 

Route::get('/admin/{vue?}', function () {
    return view('welcome');

})->where('vue', '[\/\w\.-]*')->name('home');

Route::get('/admin-login/{vue?}', function () {
    return view('welcome');

})->where('vue', '[\/\w\.-]*')->name('home');

Route::get('/admin-forgot-password/{vue?}', function () {
    return view('welcome');

})->where('vue', '[\/\w\.-]*')->name('home');

Route::get('/admin-reset-password/{vue?}/{email}', function () {
    return view('admin-reset');

})->where('vue', '[\/\w\.-]*')->name('home');

// Route::get('/front/{vue?}', function () {
//     return view('front');
// })->where('vue', '[\/\w\.-]*')->name('front');

// Route::get('/login/{vue?}', function () {
//     return view('front-temp');
// })->where('vue', '[\/\w\.-]*')->name('front');

Route::get('/{vue?}', function () {
    return view('front-temp');

})->where('vue', '[\/\w\.-]*')->name('home');

Route::get('/confirm-account/{vue?}/{email}', function () {
    return view('front-confirm-account');
})->where('vue', '[\/\w\.-]*')->name('confirm');



// Auth::routes();

// Route::get('/home', 'HomeController@index')->name('home');

//Route::get('/emailShow', '\App\Api\Front\Controllers\BookingController@emailShow')->name('emailShow');