(function(){var loadHandler=window['sl_{DADD072C-B683-451D-A1EB-64F0725ADFA1}'];loadHandler&&loadHandler(13, '<div id="spr0_2142493"><div id="spr1_2142493" class="kern slide"><img id="img0_2142493" src="data/img3.png" width="720px" height="540px" alt="" style="left:0px;top:0px;"/><div id="spr3_2142493" style="left:538.863px;top:507.402px;"><div style="width:0px;"><span id="txt0_2142493" class="nokern relpos" data-width="64.199219" style="left:79.875px;top:-0.844px;">Plan International </span><span id="txt1_2142493" class="nokern relpos" style="left:79.87px;top:-0.844px;">©</span></div></div><div id="svg3_2142493" style="left:27.25px;top:501.833px;"><svg width="665" height="2" viewBox="-0.75 -0.375 665 2"><g fill="#878787" stroke="none"><path stroke="#878787" stroke-width="0.8" stroke-linecap="butt" transform="translate(-28,-502.207703)" d="M28,502.583 H691.25"/></g></svg></div></div><div id="spr2_2142493" class="kern slide"><div id="spr4_2142493" style="left:54.021px;top:126.683px;"><div style="width:0px;"><span id="txt2_2142493" data-width="393.178711" style="top:-2.414px;">Violence against a child or young person includes</span></div><div style="width:0px;"><span id="txt3_2142493" data-width="383.897461" style="top:20.266px;">all forms of physical or mental violence, injury or</span></div><div style="width:0px;"><span id="txt4_2142493" data-width="388.248047" style="top:42.946px;">abuse, neglect or negligent treatment, emotional</span></div><div style="width:0px;"><span id="txt5_2142493" data-width="360.843750" style="top:65.626px;">ill-treatment or psychological violence, sexual</span></div><div style="width:0px;"><span id="txt6_2142493" data-width="319.605469" style="top:88.306px;">abuse and exploitation, harassment, and</span></div><div style="width:0px;"><span id="txt7_2142493" data-width="359.490234" style="top:110.986px;">commercial or other exploitation of a child or</span></div><div style="width:0px;"><span id="txt8_2142493" data-width="398.065430" style="top:133.666px;">young person. Acts of violence can also take place</span></div><div style="width:0px;"><span id="txt9_2142493" data-width="400.579102" style="top:156.346px;">online through, for example, the web, social media</span></div><div style="width:0px;"><span id="txt10_2142493" data-width="143.885742" style="top:179.026px;">or mobile phones.</span></div></div><div id="spr5_2142493" style="left:54.021px;top:39.431px;"><div style="width:0px;"><span id="txt11_2142493" data-width="291.785156" style="top:-4.69px;">Categories of violence</span></div></div><div id="spr6_2142493" style="left:54.021px;top:126.683px;"><div style="width:0px;"><span id="txt12_2142493" data-width="377.463867" style="top:-2.414px;">It may be an intentional act involving the use of</span></div><div style="width:0px;"><span id="txt13_2142493" data-width="388.898438" style="top:20.266px;">physical force or power or it may be failing to act</span></div><div style="width:0px;"><span id="txt14_2142493" data-width="348.161133" style="top:42.946px;">to prevent violence against a child or young</span></div><div style="width:0px;"><span id="txt15_2142493" data-width="345.093750" style="top:65.626px;">person. Violence consists of anything which</span></div><div style="width:0px;"><span id="txt16_2142493" data-width="405.140625" style="top:88.306px;">individuals, groups, institutions or organisations do</span></div><div style="width:0px;"><span id="txt17_2142493" data-width="399.708984" style="top:110.986px;">or fail to do, intentionally or unintentionally, which</span></div><div style="width:0px;"><span id="txt18_2142493" data-width="333.448242" style="top:133.666px;">either results in or has a high likelihood of</span></div><div style="width:0px;"><span id="txt19_2142493" data-width="400.983398" style="top:156.346px;">resulting in actual or potential harm to the child or</span></div><div style="width:0px;"><span id="txt20_2142493" data-width="400.385742" style="top:179.026px;">young person’s wellbeing, dignity and survival and</span></div><div style="width:0px;"><span id="txt21_2142493" data-width="108.641602" style="top:201.706px;">development.</span></div></div><div id="spr7_2142493" style="left:430.8px;top:342.715px;"><div id="svg9_2142493"><svg width="194" height="130" viewBox="-0.133 -0.177 194 130"><g fill="none"><g transform="translate(-0.132904,-0.17688)"><path fill="#d9d9d6" d="M4.466,124.554 C4.466,125.692 3.615,125.692 3.615,126.545 C3.615,126.687 3.403,126.829 3.296,126.687 C1.701,126.118 1.063,124.839 0.851,121.995 C0.851,121.568 1.063,120.857 0.425,121.142 C0,121.142 0.106,120.147 0.319,120.004 C1.063,119.578 1.17,118.156 1.914,117.587 C2.339,117.161 2.02,117.161 1.701,116.876 C0.319,115.454 0.213,113.89 1.489,112.184 C2.02,111.473 2.127,110.62 1.595,110.336 C0.851,109.767 1.063,108.914 1.17,108.487 C1.382,107.776 1.701,107.492 1.17,106.781 C1.063,106.639 1.17,106.212 1.489,106.07 C2.552,105.644 2.552,104.506 1.808,103.084 C1.489,102.373 0.957,101.236 1.808,100.241 C2.127,99.956 2.127,99.53 2.127,99.103 C2.127,97.397 1.701,95.833 1.808,94.127 C1.808,93.842 1.808,93.7 1.701,93.558 C0.638,92.42 1.17,91.567 1.808,90.714 C0.851,89.435 0.744,87.586 1.382,86.164 C1.489,86.022 1.489,86.022 1.489,86.022 C1.063,83.889 2.127,82.183 2.02,80.192 C2.02,78.202 0.851,76.78 1.17,74.789 C1.17,74.221 1.063,73.51 1.595,73.225 C1.808,73.083 1.914,72.657 1.701,72.23 C1.382,71.235 1.595,70.524 2.339,70.24 C1.489,69.386 1.382,68.391 1.914,67.254 C2.233,66.827 2.233,66.116 1.914,65.69 C1.17,64.41 1.17,63.415 1.808,62.277 C1.808,62.135 2.02,62.135 1.914,61.851 C1.382,59.149 2.127,56.305 2.233,53.604 C2.233,51.187 0.957,49.48 1.382,47.206 C1.382,46.921 1.276,46.637 0.957,46.637 C0.744,46.637 0.532,46.21 0.638,46.068 C1.382,45.641 1.17,44.22 1.701,43.793 C2.765,42.798 2.871,42.087 2.02,40.807 C1.701,40.238 1.276,40.096 2.127,39.527 C3.084,38.817 3.084,36.115 2.339,34.835 C1.914,34.124 2.02,33.271 2.233,32.703 C2.658,31.849 2.765,31.281 2.233,30.57 C2.127,30.57 2.02,30.428 1.914,30.285 C2.871,28.864 1.17,27.3 2.127,25.736 C2.871,24.456 1.808,22.181 2.658,20.617 C2.977,19.906 2.446,20.048 2.339,19.622 C2.127,18.768 1.808,17.915 2.765,17.631 C2.977,17.631 3.403,18.058 3.403,17.204 C3.403,16.636 3.403,15.925 3.084,15.925 C2.446,15.783 2.552,15.072 2.552,14.503 C2.658,13.65 3.19,13.934 3.403,13.934 C4.572,14.076 5.529,14.076 5.742,11.801 C5.742,11.09 6.167,11.375 6.486,11.375 C10.314,11.09 14.142,10.948 17.97,10.948 C19.352,11.09 20.628,10.237 22.011,10.806 C22.436,10.948 22.968,9.526 23.818,9.953 C24.244,10.095 24.669,9.526 24.988,10.664 C25.201,11.375 26.477,11.233 26.583,10.806 C27.008,9.811 27.54,10.237 28.072,10.237 C29.241,10.237 30.411,10.237 31.687,10.237 C31.9,10.38 32.006,10.38 32.112,9.953 C32.431,9.242 32.963,9.1 33.282,10.095 C33.495,10.806 33.814,10.806 34.026,10.664 C35.621,9.242 37.429,10.237 39.13,9.953 C39.874,9.811 40.725,10.237 41.363,9.242 C41.576,8.958 42.001,8.958 42.426,9.384 C42.852,9.953 43.277,10.664 43.702,9.384 C43.809,8.815 44.34,9.1 44.447,9.669 C44.766,10.522 45.191,10.522 45.51,9.811 C45.829,9.384 46.042,9.526 46.361,9.526 C47.53,9.526 48.7,9.526 49.87,9.526 C50.189,9.526 50.508,10.095 50.614,9.1 C50.614,8.673 51.039,8.673 51.146,8.815 C52.528,10.38 53.804,8.105 55.08,8.673 C56.569,9.384 58.164,9.242 59.652,9.1 C60.29,9.1 61.034,9.526 61.566,8.389 C61.672,8.247 62.098,8.247 62.204,8.389 C63.161,9.953 64.118,7.962 65.181,8.389 C66.245,8.815 67.521,8.673 68.69,8.815 C69.116,8.815 69.647,9.1 69.86,7.962 C69.86,7.394 71.987,7.82 72.306,8.389 C72.731,9.242 73.794,9.242 74.326,8.389 C74.539,7.962 74.858,8.105 75.07,8.105 C76.134,8.105 77.197,7.962 78.26,8.247 C78.898,8.531 79.643,9.1 80.174,7.536 C80.493,6.825 81.557,7.109 82.301,7.394 C82.832,7.536 83.258,7.678 83.789,7.678 C86.873,7.394 89.957,8.247 93.04,7.962 C93.466,7.962 93.997,8.389 94.104,7.251 C94.21,6.825 94.423,6.825 94.635,7.251 C95.273,8.389 95.592,8.389 96.124,7.394 C96.337,7.109 96.337,7.251 96.549,7.251 C97.4,7.536 98.57,6.541 99.314,8.389 C99.314,8.531 99.527,8.673 99.633,8.531 C100.696,6.541 102.185,7.962 103.567,7.678 C104.418,7.536 105.694,8.247 106.225,7.678 C107.076,7.109 107.395,7.251 108.033,7.962 C108.139,8.105 108.458,8.105 108.565,7.82 C108.884,6.825 109.096,6.256 109.415,7.82 C109.415,7.962 109.841,8.105 109.841,7.962 C110.691,5.83 112.074,6.825 113.031,6.967 C116.327,7.536 119.517,7.536 122.813,7.82 C123.877,7.962 125.046,8.105 126.11,7.962 C126.641,7.962 127.173,8.389 127.492,7.109 C127.492,6.825 128.236,6.398 128.555,7.536 C128.768,8.389 129.299,8.389 129.725,7.82 C130.575,6.825 130.788,6.825 131.426,8.389 C131.958,7.678 132.489,5.972 133.234,8.247 C133.234,8.531 133.765,8.815 134.191,8.389 C135.041,7.394 135.998,6.967 136.743,8.531 C136.955,8.815 137.168,8.815 137.274,8.673 C138.444,7.251 140.252,7.251 141.209,8.531 C141.528,8.815 141.74,8.673 142.059,8.389 C142.485,7.962 143.229,7.394 143.442,8.958 C143.442,9.669 143.867,9.526 144.186,9.526 C146.844,9.526 149.609,9.526 152.267,9.526 C152.374,9.526 152.48,9.526 152.48,9.526 C153.224,8.389 153.968,9.1 154.713,9.1 C155.032,8.958 155.67,9.526 155.67,8.247 C155.67,7.962 156.414,7.82 156.627,8.105 C157.477,8.958 158.222,8.673 158.966,7.678 C159.072,7.536 159.285,7.251 159.498,7.678 C159.923,8.389 160.561,8.247 160.88,7.394 C160.88,7.109 161.943,6.825 162.05,7.109 C162.156,7.678 162.581,7.678 162.581,7.82 C163.538,10.38 163.751,7.678 163.857,7.251 C164.283,5.687 165.133,6.256 165.665,6.541 C166.197,6.967 166.728,7.109 167.047,6.683 C168.642,4.976 170.45,5.403 172.151,5.119 C175.767,4.692 179.275,3.981 182.891,3.555 C183.103,3.555 183.422,3.412 183.422,3.412 C183.741,0.853 185.124,2.417 185.974,1.848 C186.4,1.422 186.931,1.137 187.463,0.853 C188.845,0 190.547,0.142 192.142,0.284 C192.567,0.284 192.248,1.137 192.354,1.564 C192.567,1.991 192.992,0.995 192.992,1.848 C193.099,2.559 192.461,2.275 192.461,2.559 C192.354,4.123 191.291,4.692 191.078,5.83 C190.972,6.967 190.866,8.673 191.397,9.669 C192.142,10.948 192.035,12.37 191.716,13.508 C191.61,14.361 191.716,14.929 191.61,15.783 C191.504,17.204 192.142,18.768 191.397,20.332 C191.291,20.617 190.653,22.607 192.035,22.75 C192.035,22.75 192.142,23.318 192.035,23.603 C191.823,24.882 191.716,26.304 191.61,27.584 C191.397,29.717 191.185,29.859 192.035,31.707 C192.142,31.992 192.248,32.703 192.035,32.987 C191.504,33.84 191.929,35.262 191.185,36.115 C191.185,36.115 191.185,36.115 191.185,36.257 C190.653,37.253 192.035,37.537 191.61,38.532 C191.291,39.385 191.185,40.238 191.929,40.807 C192.035,40.949 192.142,41.234 192.035,41.376 C191.078,42.087 192.142,44.077 191.291,44.931 C191.185,44.931 191.185,45.215 191.185,45.357 C192.354,45.641 191.61,47.348 191.929,48.201 C192.035,48.627 192.035,49.196 191.61,49.48 C191.397,49.623 191.291,50.191 191.397,50.618 C191.61,52.466 191.823,54.173 191.61,56.021 C191.504,56.59 191.929,57.727 192.035,58.58 C192.354,60.855 192.035,63.13 191.929,65.405 C191.823,66.827 192.035,68.391 192.142,69.813 C192.248,70.666 192.567,71.377 191.716,71.661 C191.504,71.804 191.61,72.514 191.61,72.941 C191.929,75.785 192.567,78.913 192.248,81.757 C191.929,83.747 192.673,86.164 191.61,88.013 C191.504,88.297 191.397,89.719 191.823,90.003 C192.354,90.288 192.248,90.714 192.354,91.283 C192.567,92.42 192.461,93.416 191.716,94.127 C191.397,94.411 191.078,94.695 191.716,94.98 C192.142,95.122 192.035,95.406 192.142,95.691 C192.142,96.828 191.397,98.108 192.354,99.245 C192.461,99.245 192.461,100.098 192.142,100.098 C191.078,100.241 191.291,101.236 191.397,102.373 C191.397,103.084 191.397,103.795 191.078,104.506 C190.759,105.217 190.759,106.355 191.291,107.35 C191.929,108.203 192.035,109.909 191.716,111.047 C191.504,111.758 191.397,112.469 191.397,113.037 C191.397,115.028 190.866,116.876 191.291,118.867 C191.291,119.151 190.228,120.004 189.909,119.72 C189.271,119.151 188.207,119.578 188.101,120.289 C187.888,121.568 187.569,121.284 187.038,121.426 C185.762,121.426 184.592,121.426 183.316,121.711 C182.04,121.995 180.658,121.142 179.275,121.284 C178.531,121.426 177.68,121.426 177.042,122.422 C177.042,122.422 176.511,123.132 176.192,122.279 C175.979,121.711 173.853,121.853 173.427,122.279 C172.47,123.559 172.258,123.417 170.875,122.706 C170.45,122.564 169.387,122.137 169.068,123.559 C168.961,124.128 168.749,124.412 168.323,124.27 C167.685,124.27 167.047,124.554 166.516,124.696 C165.984,124.696 165.559,124.981 165.133,123.843 C164.708,122.848 163.645,123.843 163.007,124.128 C162.156,124.412 161.837,124.554 161.837,122.99 C161.837,122.137 161.412,122.137 160.986,122.137 C156.839,122.137 152.799,122.137 148.758,122.137 C148.439,122.137 147.908,121.995 147.908,122.848 C148.014,124.128 147.482,123.986 146.951,123.986 C141.634,123.559 136.211,123.559 130.788,122.848 C130.363,122.848 129.725,122.279 129.299,123.701 C129.193,124.27 128.236,124.27 127.811,123.701 C127.173,122.422 126.429,122.706 125.578,123.275 C125.472,123.417 125.365,123.559 125.259,123.417 C123.877,122.137 123.877,122.279 122.388,123.417 C121.856,123.843 121.112,123.986 120.687,123.132 C120.368,122.279 119.836,122.279 119.623,122.564 C118.666,123.843 117.603,122.848 116.646,123.275 C116.008,123.417 116.327,122.137 115.902,121.995 C115.264,121.853 114.838,122.848 114.413,122.848 C112.286,122.848 110.16,122.848 108.033,122.706 C107.501,122.706 106.97,122.848 106.544,121.853 C106.225,121.142 106.013,122.422 105.588,122.422 C104.205,122.422 102.717,122.706 101.441,122.422 C99.633,121.853 97.825,121.995 96.124,122.279 C95.38,122.422 94.529,122.848 93.678,122.848 C93.253,122.848 92.721,122.99 92.509,122.137 C92.402,121.568 91.871,121.711 91.871,121.995 C91.977,123.701 91.02,122.848 90.807,122.706 C89.425,122.422 88.149,122.564 86.767,122.422 C86.554,122.422 86.235,122.279 86.022,122.848 C85.81,123.417 85.597,123.559 85.491,122.706 C85.491,122.564 85.172,122.422 85.065,122.564 C83.683,123.986 82.195,122.848 80.812,123.132 C80.6,123.275 80.387,123.275 80.281,122.706 C80.281,121.853 79.643,121.995 79.536,122.137 C78.473,123.417 77.41,122.706 76.346,122.848 C75.921,122.848 75.389,122.564 75.177,123.559 C75.07,123.701 74.858,123.843 74.858,123.843 C73.263,122.137 71.561,124.412 69.966,123.559 C69.754,123.417 69.541,123.701 69.541,124.128 C69.328,124.981 69.116,124.696 68.903,124.27 C68.371,123.132 68.265,123.132 68.052,124.128 C67.946,124.981 67.627,124.554 67.308,124.554 C65.181,124.128 63.055,123.843 61.034,124.554 C59.546,124.981 58.164,125.265 56.781,124.981 C55.08,124.696 53.379,124.412 51.784,124.839 C50.401,125.265 49.019,125.407 47.637,125.407 C46.999,125.407 46.254,125.692 45.51,125.123 C45.404,124.981 44.978,125.123 44.766,125.265 C43.809,127.114 42.533,125.976 41.363,126.118 C40.725,126.261 39.981,125.407 39.449,126.403 C39.236,126.687 39.024,126.687 38.917,126.403 C38.598,125.55 38.279,125.55 38.067,126.403 C37.96,126.545 37.748,126.687 37.641,126.545 C36.259,125.692 34.983,126.971 33.601,126.545 C32.431,126.118 31.368,125.123 29.986,125.55 C27.115,126.261 24.244,126.971 21.266,126.971 C19.671,128.962 17.545,128.109 15.737,128.535 C13.823,129.104 11.803,128.962 9.889,129.104 C9.357,129.246 7.975,127.825 7.55,127.114 C7.018,126.261 6.167,126.261 5.423,125.976 C4.891,125.692 4.785,125.265 4.466,124.554 Z" fill-rule="evenodd"/></g></g></svg></div><div id="svg10_2142493" style="left:10.85px;top:15.991px;"><svg width="183" height="33" viewBox="0 0 183 33"><path fill="none" d="M0,0 h182.248 v33 h-182.248 Z"/></svg></div><div id="spr8_2142493" style="left:10.85px;top:15.991px;"><div style="width:0px;"><span id="txt22_2142493" data-width="156.210938" style="left:7.199px;top:5.932px;">Read through the following</span></div><div style="width:0px;"><span id="txt23_2142493" data-width="146.958984" style="left:7.199px;top:20.332px;">cards carefully and ensure</span></div><div style="width:0px;"><span id="txt24_2142493" data-width="137.654297" style="left:7.199px;top:34.732px;">you understand the four</span></div><div style="width:0px;"><span id="txt25_2142493" data-width="158.003906" style="left:7.199px;top:49.132px;">main categories of violence.</span></div><div style="width:0px;"><span id="txt26_2142493" data-width="145.494141" style="left:7.199px;top:77.932px;">There is a quiz at the end!</span></div></div></div><div id="spr9_2142493" style="left:54.021px;top:361.621px;"><div id="svg14_2142493" style="left:-0px;"><svg width="148" height="31" viewBox="-0.102 -0.041 148 31"><g fill="none"><g transform="translate(-0.101597,-0.041168)"><path fill="#d9d9d6" d="M3.414,28.993 C3.414,29.258 2.763,29.258 2.763,29.456 C2.763,29.489 2.601,29.522 2.52,29.489 C1.3,29.357 0.813,29.059 0.65,28.397 C0.65,28.298 0.813,28.132 0.325,28.199 C0,28.199 0.081,27.967 0.244,27.934 C0.813,27.834 0.894,27.503 1.463,27.371 C1.788,27.272 1.544,27.272 1.3,27.206 C0.244,26.875 0.163,26.511 1.138,26.113 C1.544,25.948 1.626,25.749 1.219,25.683 C0.65,25.551 0.813,25.352 0.894,25.253 C1.057,25.087 1.3,25.021 0.894,24.856 C0.813,24.823 0.894,24.723 1.138,24.69 C1.951,24.591 1.951,24.326 1.382,23.995 C1.138,23.83 0.731,23.565 1.382,23.333 C1.626,23.267 1.626,23.168 1.626,23.069 C1.626,22.671 1.3,22.307 1.382,21.91 C1.382,21.844 1.382,21.811 1.3,21.778 C0.488,21.513 0.894,21.314 1.382,21.116 C0.65,20.818 0.569,20.388 1.057,20.057 C1.138,20.024 1.138,20.024 1.138,20.024 C0.813,19.527 1.626,19.13 1.544,18.667 C1.544,18.203 0.65,17.872 0.894,17.409 C0.894,17.277 0.813,17.111 1.219,17.045 C1.382,17.012 1.463,16.912 1.3,16.813 C1.057,16.582 1.219,16.416 1.788,16.35 C1.138,16.151 1.057,15.92 1.463,15.655 C1.707,15.556 1.707,15.39 1.463,15.291 C0.894,14.993 0.894,14.761 1.382,14.496 C1.382,14.463 1.544,14.463 1.463,14.397 C1.057,13.768 1.626,13.106 1.707,12.478 C1.707,11.915 0.731,11.518 1.057,10.988 C1.057,10.922 0.975,10.856 0.731,10.856 C0.569,10.856 0.406,10.756 0.488,10.723 C1.057,10.624 0.894,10.293 1.3,10.194 C2.113,9.962 2.194,9.797 1.544,9.499 C1.3,9.366 0.975,9.333 1.626,9.201 C2.357,9.035 2.357,8.407 1.788,8.109 C1.463,7.943 1.544,7.745 1.707,7.612 C2.032,7.414 2.113,7.281 1.707,7.116 C1.626,7.116 1.544,7.083 1.463,7.05 C2.194,6.719 0.894,6.355 1.626,5.991 C2.194,5.693 1.382,5.163 2.032,4.799 C2.276,4.634 1.869,4.667 1.788,4.567 C1.626,4.369 1.382,4.17 2.113,4.104 C2.276,4.104 2.601,4.203 2.601,4.005 C2.601,3.872 2.601,3.707 2.357,3.707 C1.869,3.674 1.951,3.508 1.951,3.376 C2.032,3.177 2.438,3.243 2.601,3.243 C3.495,3.277 4.226,3.277 4.389,2.747 C4.389,2.582 4.714,2.648 4.958,2.648 C7.884,2.582 10.81,2.548 13.736,2.548 C14.793,2.582 15.768,2.383 16.824,2.515 C17.15,2.548 17.556,2.217 18.206,2.317 C18.531,2.35 18.856,2.217 19.1,2.482 C19.263,2.648 20.238,2.615 20.319,2.515 C20.644,2.284 21.051,2.383 21.457,2.383 C22.351,2.383 23.245,2.383 24.221,2.383 C24.383,2.416 24.465,2.416 24.546,2.317 C24.79,2.151 25.196,2.118 25.44,2.35 C25.602,2.515 25.846,2.515 26.009,2.482 C27.228,2.151 28.61,2.383 29.91,2.317 C30.479,2.284 31.129,2.383 31.617,2.151 C31.779,2.085 32.105,2.085 32.43,2.184 C32.755,2.317 33.08,2.482 33.405,2.184 C33.486,2.052 33.893,2.118 33.974,2.251 C34.218,2.449 34.543,2.449 34.787,2.284 C35.031,2.184 35.193,2.217 35.437,2.217 C36.331,2.217 37.225,2.217 38.119,2.217 C38.363,2.217 38.607,2.35 38.688,2.118 C38.688,2.019 39.013,2.019 39.094,2.052 C40.151,2.416 41.126,1.887 42.102,2.019 C43.24,2.184 44.459,2.151 45.597,2.118 C46.084,2.118 46.653,2.217 47.06,1.953 C47.141,1.92 47.466,1.92 47.547,1.953 C48.279,2.317 49.01,1.853 49.823,1.953 C50.636,2.052 51.611,2.019 52.505,2.052 C52.83,2.052 53.237,2.118 53.399,1.853 C53.399,1.721 55.025,1.82 55.269,1.953 C55.594,2.151 56.407,2.151 56.813,1.953 C56.976,1.853 57.219,1.887 57.382,1.887 C58.195,1.887 59.007,1.853 59.82,1.92 C60.308,1.986 60.877,2.118 61.283,1.754 C61.527,1.589 62.34,1.655 62.909,1.721 C63.315,1.754 63.64,1.787 64.047,1.787 C66.404,1.721 68.761,1.92 71.118,1.853 C71.443,1.853 71.849,1.953 71.931,1.688 C72.012,1.589 72.174,1.589 72.337,1.688 C72.825,1.953 73.068,1.953 73.475,1.721 C73.637,1.655 73.637,1.688 73.8,1.688 C74.45,1.754 75.344,1.522 75.913,1.953 C75.913,1.986 76.076,2.019 76.157,1.986 C76.97,1.522 78.108,1.853 79.164,1.787 C79.814,1.754 80.79,1.92 81.196,1.787 C81.846,1.655 82.09,1.688 82.578,1.853 C82.659,1.887 82.903,1.887 82.984,1.82 C83.228,1.589 83.391,1.456 83.635,1.82 C83.635,1.853 83.96,1.887 83.96,1.853 C84.61,1.357 85.666,1.589 86.398,1.622 C88.918,1.754 91.356,1.754 93.876,1.82 C94.688,1.853 95.582,1.887 96.395,1.853 C96.801,1.853 97.208,1.953 97.452,1.655 C97.452,1.589 98.021,1.489 98.264,1.754 C98.427,1.953 98.833,1.953 99.159,1.82 C99.809,1.589 99.971,1.589 100.459,1.953 C100.865,1.787 101.272,1.39 101.841,1.92 C101.841,1.986 102.247,2.052 102.572,1.953 C103.222,1.721 103.954,1.622 104.523,1.986 C104.685,2.052 104.848,2.052 104.929,2.019 C105.823,1.688 107.205,1.688 107.937,1.986 C108.18,2.052 108.343,2.019 108.587,1.953 C108.912,1.853 109.481,1.721 109.643,2.085 C109.643,2.251 109.968,2.217 110.212,2.217 C112.244,2.217 114.357,2.217 116.389,2.217 C116.471,2.217 116.552,2.217 116.552,2.217 C117.121,1.953 117.69,2.118 118.259,2.118 C118.503,2.085 118.99,2.217 118.99,1.92 C118.99,1.853 119.559,1.82 119.722,1.887 C120.372,2.085 120.941,2.019 121.51,1.787 C121.591,1.754 121.754,1.688 121.916,1.787 C122.241,1.953 122.729,1.92 122.973,1.721 C122.973,1.655 123.786,1.589 123.867,1.655 C123.948,1.787 124.273,1.787 124.273,1.82 C125.005,2.416 125.167,1.787 125.249,1.688 C125.574,1.324 126.224,1.456 126.63,1.522 C127.037,1.622 127.443,1.655 127.687,1.556 C128.906,1.158 130.288,1.258 131.588,1.191 C134.352,1.092 137.034,0.927 139.797,0.827 C139.96,0.827 140.204,0.794 140.204,0.794 C140.448,0.199 141.504,0.563 142.154,0.43 C142.479,0.331 142.886,0.265 143.292,0.199 C144.349,0 145.649,0.033 146.868,0.066 C147.194,0.066 146.95,0.265 147.031,0.364 C147.194,0.463 147.519,0.232 147.519,0.43 C147.6,0.596 147.112,0.53 147.112,0.596 C147.031,0.96 146.218,1.092 146.056,1.357 C145.974,1.622 145.893,2.019 146.299,2.251 C146.868,2.548 146.787,2.879 146.543,3.144 C146.462,3.343 146.543,3.475 146.462,3.674 C146.381,4.005 146.868,4.369 146.299,4.733 C146.218,4.799 145.731,5.262 146.787,5.295 C146.787,5.295 146.868,5.428 146.787,5.494 C146.625,5.792 146.543,6.123 146.462,6.421 C146.299,6.917 146.137,6.95 146.787,7.381 C146.868,7.447 146.95,7.612 146.787,7.678 C146.381,7.877 146.706,8.208 146.137,8.407 C146.137,8.407 146.137,8.407 146.137,8.44 C145.731,8.671 146.787,8.738 146.462,8.969 C146.218,9.168 146.137,9.366 146.706,9.499 C146.787,9.532 146.868,9.598 146.787,9.631 C146.056,9.797 146.868,10.26 146.218,10.459 C146.137,10.459 146.137,10.525 146.137,10.558 C147.031,10.624 146.462,11.021 146.706,11.22 C146.787,11.319 146.787,11.452 146.462,11.518 C146.299,11.551 146.218,11.683 146.299,11.782 C146.462,12.213 146.625,12.61 146.462,13.04 C146.381,13.173 146.706,13.437 146.787,13.636 C147.031,14.165 146.787,14.695 146.706,15.225 C146.625,15.556 146.787,15.92 146.868,16.251 C146.95,16.449 147.194,16.615 146.543,16.681 C146.381,16.714 146.462,16.879 146.462,16.979 C146.706,17.641 147.194,18.369 146.95,19.031 C146.706,19.494 147.275,20.057 146.462,20.487 C146.381,20.553 146.299,20.884 146.625,20.95 C147.031,21.017 146.95,21.116 147.031,21.248 C147.194,21.513 147.112,21.745 146.543,21.91 C146.299,21.976 146.056,22.043 146.543,22.109 C146.868,22.142 146.787,22.208 146.868,22.274 C146.868,22.539 146.299,22.837 147.031,23.102 C147.112,23.102 147.112,23.3 146.868,23.3 C146.056,23.333 146.218,23.565 146.299,23.83 C146.299,23.995 146.299,24.161 146.056,24.326 C145.812,24.492 145.812,24.756 146.218,24.988 C146.706,25.187 146.787,25.584 146.543,25.849 C146.381,26.014 146.299,26.18 146.299,26.312 C146.299,26.775 145.893,27.206 146.218,27.669 C146.218,27.735 145.405,27.934 145.162,27.868 C144.674,27.735 143.861,27.834 143.78,28 C143.617,28.298 143.373,28.232 142.967,28.265 C141.992,28.265 141.098,28.265 140.122,28.331 C139.147,28.397 138.09,28.199 137.034,28.232 C136.465,28.265 135.815,28.265 135.327,28.496 C135.327,28.496 134.921,28.662 134.677,28.463 C134.514,28.331 132.889,28.364 132.564,28.463 C131.832,28.761 131.67,28.728 130.613,28.563 C130.288,28.529 129.475,28.43 129.231,28.761 C129.15,28.894 128.987,28.96 128.662,28.927 C128.175,28.927 127.687,28.993 127.281,29.026 C126.874,29.026 126.549,29.092 126.224,28.827 C125.899,28.596 125.086,28.827 124.598,28.894 C123.948,28.96 123.704,28.993 123.704,28.629 C123.704,28.43 123.379,28.43 123.054,28.43 C119.884,28.43 116.796,28.43 113.707,28.43 C113.463,28.43 113.057,28.397 113.057,28.596 C113.138,28.894 112.732,28.86 112.325,28.86 C108.262,28.761 104.116,28.761 99.971,28.596 C99.646,28.596 99.159,28.463 98.833,28.794 C98.752,28.927 98.021,28.927 97.696,28.794 C97.208,28.496 96.639,28.563 95.989,28.695 C95.907,28.728 95.826,28.761 95.745,28.728 C94.688,28.43 94.688,28.463 93.55,28.728 C93.144,28.827 92.575,28.86 92.25,28.662 C92.006,28.463 91.6,28.463 91.437,28.529 C90.706,28.827 89.893,28.596 89.161,28.695 C88.674,28.728 88.918,28.43 88.592,28.397 C88.105,28.364 87.78,28.596 87.455,28.596 C85.829,28.596 84.203,28.596 82.578,28.563 C82.172,28.563 81.765,28.596 81.44,28.364 C81.196,28.199 81.034,28.496 80.709,28.496 C79.652,28.496 78.514,28.563 77.539,28.496 C76.157,28.364 74.775,28.397 73.475,28.463 C72.906,28.496 72.256,28.596 71.605,28.596 C71.28,28.596 70.874,28.629 70.711,28.43 C70.63,28.298 70.224,28.331 70.224,28.397 C70.305,28.794 69.574,28.596 69.411,28.563 C68.354,28.496 67.379,28.529 66.322,28.496 C66.16,28.496 65.916,28.463 65.753,28.596 C65.591,28.728 65.428,28.761 65.347,28.563 C65.347,28.529 65.103,28.496 65.022,28.529 C63.965,28.86 62.828,28.596 61.771,28.662 C61.608,28.695 61.446,28.695 61.365,28.563 C61.365,28.364 60.877,28.397 60.796,28.43 C59.983,28.728 59.17,28.563 58.357,28.596 C58.032,28.596 57.626,28.529 57.463,28.761 C57.382,28.794 57.219,28.827 57.219,28.827 C56,28.43 54.7,28.96 53.481,28.761 C53.318,28.728 53.155,28.794 53.155,28.894 C52.993,29.092 52.83,29.026 52.668,28.927 C52.261,28.662 52.18,28.662 52.018,28.894 C51.936,29.092 51.692,28.993 51.449,28.993 C49.823,28.894 48.198,28.827 46.653,28.993 C45.515,29.092 44.459,29.158 43.402,29.092 C42.102,29.026 40.801,28.96 39.582,29.059 C38.526,29.158 37.469,29.191 36.412,29.191 C35.925,29.191 35.356,29.258 34.787,29.125 C34.705,29.092 34.38,29.125 34.218,29.158 C33.486,29.589 32.511,29.324 31.617,29.357 C31.129,29.39 30.56,29.191 30.154,29.423 C29.991,29.489 29.829,29.489 29.748,29.423 C29.504,29.225 29.26,29.225 29.097,29.423 C29.016,29.456 28.854,29.489 28.772,29.456 C27.716,29.258 26.74,29.555 25.684,29.456 C24.79,29.357 23.977,29.125 22.92,29.225 C20.726,29.39 18.531,29.555 16.255,29.555 C15.036,30.019 13.411,29.82 12.029,29.92 C10.566,30.052 9.022,30.019 7.559,30.052 C7.152,30.085 6.096,29.754 5.771,29.589 C5.364,29.39 4.714,29.39 4.145,29.324 C3.739,29.258 3.657,29.158 3.414,28.993 Z" fill-rule="evenodd"/></g></g></svg></div><div id="svg15_2142493" style="left:8.294px;top:1.322px;"><svg width="140" height="14" viewBox="0 0 140 14"><path fill="none" d="M0,0 h139.306 v13.163 h-139.306 Z"/></svg></div><div id="spr10_2142493" style="left:8.294px;top:1.322px;"><div style="width:0px;"><span id="txt27_2142493" data-width="117.533203" style="left:7.199px;top:5.315px;">Click to advance text</span></div></div></div></div></div>');})();