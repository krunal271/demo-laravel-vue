webpackJsonp([4],{

/***/ "./node_modules/babel-loader/lib/index.js?{\"cacheDirectory\":true,\"presets\":[[\"env\",{\"modules\":false,\"targets\":{\"browsers\":[\"> 2%\"],\"uglify\":true}}]],\"plugins\":[\"transform-object-rest-spread\",[\"transform-runtime\",{\"polyfill\":false,\"helpers\":false}]]}!./node_modules/vue-loader/lib/selector.js?type=script&index=0!./resources/js/views/Front/components/slider.vue":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__services_ls__ = __webpack_require__("./resources/js/services/ls.js");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//


/* harmony default export */ __webpack_exports__["default"] = ({
    data: function data() {
        return {
            roleType: __WEBPACK_IMPORTED_MODULE_0__services_ls__["a" /* default */].get('role')
        };
    },
    mounted: function mounted() {},

    methods: {
        getStarted: function getStarted() {
            var vm = this;
            vm.$router.push({ 'name': 'childSafeguarding' });
        }
    }
});

/***/ }),

/***/ "./node_modules/babel-loader/lib/index.js?{\"cacheDirectory\":true,\"presets\":[[\"env\",{\"modules\":false,\"targets\":{\"browsers\":[\"> 2%\"],\"uglify\":true}}]],\"plugins\":[\"transform-object-rest-spread\",[\"transform-runtime\",{\"polyfill\":false,\"helpers\":false}]]}!./node_modules/vue-loader/lib/selector.js?type=script&index=0!./resources/js/views/Front/innerlayout.vue":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__components_header__ = __webpack_require__("./resources/js/views/Front/components/header.vue");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__components_header___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_0__components_header__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__components_slider__ = __webpack_require__("./resources/js/views/Front/components/slider.vue");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_1__components_slider___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_1__components_slider__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__components_breadcrumb__ = __webpack_require__("./resources/js/views/Front/components/breadcrumb.vue");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_2__components_breadcrumb___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_2__components_breadcrumb__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__components_footer__ = __webpack_require__("./resources/js/views/Front/components/footer.vue");
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_3__components_footer___default = __webpack_require__.n(__WEBPACK_IMPORTED_MODULE_3__components_footer__);
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_4__services_auth__ = __webpack_require__("./resources/js/services/auth.js");
//
//
//
//
//
//
//
//
//
//
//
//







/* harmony default export */ __webpack_exports__["default"] = ({
  name: 'innerlayout',

  components: {
    Header: __WEBPACK_IMPORTED_MODULE_0__components_header___default.a,
    Breadcrumb: __WEBPACK_IMPORTED_MODULE_2__components_breadcrumb___default.a,
    Footer: __WEBPACK_IMPORTED_MODULE_3__components_footer___default.a,
    slider: __WEBPACK_IMPORTED_MODULE_1__components_slider___default.a
  },
  created: function created() {

    this.$root.$on('logout', this.logout);
  },
  computed: {
    home_page: function home_page() {
      if (this.$store.state.home_page === 'Home') {
        this.breadcrumbStatus = false;
        return true;
      } else {
        this.breadcrumbStatus = true;
        return false;
      }
    },
    strategy_page: function strategy_page() {
      if (this.$store.state.strategy_page === 'globalstrategy') {
        return true;
      } else {
        return false;
      }
    }
  },
  data: function data() {
    return {
      'breadcrumbStatus': true,
      'routeName': this.$router.currentRoute.name

    };
  },
  mounted: function mounted() {},

  methods: {
    logout: function logout() {
      var msg = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : '';

      var vm = this;
      __WEBPACK_IMPORTED_MODULE_4__services_auth__["a" /* default */].logout().then(function () {
        if (msg != '') {
          //console.log('fdsfds');
          toastr.error(msg, '', { timeOut: 5000 });
        } else {
          toastr.success('Logged out!', '', { timeOut: 5000 });
        }
        vm.$router.push({ 'name': 'frontLogin' });
      });
    }
  },
  destroyed: function destroyed() {

    this.$root.$off('logout', this.logout);
  }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/template-compiler/index.js?{\"id\":\"data-v-190f9876\",\"hasScoped\":false,\"buble\":{\"transforms\":{}}}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./resources/js/views/Front/components/slider.vue":
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", [
    _c(
      "div",
      {
        staticClass: "carousel slide",
        attrs: { id: "myCarousel", "data-ride": "carousel" }
      },
      [
        _c("div", { staticClass: "carousel-inner" }, [
          _c("div", { staticClass: "carousel-item active" }, [
            _c("img", {
              staticClass: "first-slide",
              attrs: {
                src: __webpack_require__("./resources/assets/images/banner.jpg"),
                alt: "Deep"
              }
            }),
            _vm._v(" "),
            _c("div", { staticClass: "container" }, [
              _c("div", { staticClass: "carousel-caption" }, [
                _c("h1", [_vm._v("Welcome into our Community")]),
                _vm._v(" "),
                _vm.roleType == 4
                  ? _c("div", { staticClass: "row" }, [
                      _c("div", { staticClass: "col-sm-4 offset-sm-4" }, [
                        _c(
                          "button",
                          {
                            staticClass:
                              "btn btn-primary btn-lg mt-4 btn-block py-3",
                            attrs: { type: "submit" },
                            on: { click: _vm.getStarted }
                          },
                          [_vm._v("Get Started")]
                        )
                      ])
                    ])
                  : _vm._e()
              ])
            ])
          ])
        ])
      ]
    )
  ])
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-190f9876", module.exports)
  }
}

/***/ }),

/***/ "./node_modules/vue-loader/lib/template-compiler/index.js?{\"id\":\"data-v-50bcfbea\",\"hasScoped\":false,\"buble\":{\"transforms\":{}}}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./resources/js/views/Front/innerlayout.vue":
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c(
    "div",
    { staticClass: "innerpage" },
    [
      _vm.home_page ? _c("slider") : _vm._e(),
      _vm._v(" "),
      _c("Header"),
      _vm._v(" "),
      _vm.breadcrumbStatus
        ? _c("Breadcrumb", { staticClass: "wow animated fadeIn" })
        : _vm._e(),
      _vm._v(" "),
      _c(
        "div",
        {
          class: _vm.home_page
            ? ""
            : "container pt-3 pb-5" && _vm.strategy_page
            ? ""
            : "container pt-3 pb-5"
        },
        [_c("router-view")],
        1
      ),
      _vm._v(" "),
      _c("Footer")
    ],
    1
  )
}
var staticRenderFns = []
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-50bcfbea", module.exports)
  }
}

/***/ }),

/***/ "./resources/assets/images/banner.jpg":
/***/ (function(module, exports) {

module.exports = "/images/banner.jpg?81503bcb9f92e9e38f4f41e851067a17";

/***/ }),

/***/ "./resources/js/views/Front/components/slider.vue":
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
var normalizeComponent = __webpack_require__("./node_modules/vue-loader/lib/component-normalizer.js")
/* script */
var __vue_script__ = __webpack_require__("./node_modules/babel-loader/lib/index.js?{\"cacheDirectory\":true,\"presets\":[[\"env\",{\"modules\":false,\"targets\":{\"browsers\":[\"> 2%\"],\"uglify\":true}}]],\"plugins\":[\"transform-object-rest-spread\",[\"transform-runtime\",{\"polyfill\":false,\"helpers\":false}]]}!./node_modules/vue-loader/lib/selector.js?type=script&index=0!./resources/js/views/Front/components/slider.vue")
/* template */
var __vue_template__ = __webpack_require__("./node_modules/vue-loader/lib/template-compiler/index.js?{\"id\":\"data-v-190f9876\",\"hasScoped\":false,\"buble\":{\"transforms\":{}}}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./resources/js/views/Front/components/slider.vue")
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = null
/* scopeId */
var __vue_scopeId__ = null
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/js/views/Front/components/slider.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-190f9876", Component.options)
  } else {
    hotAPI.reload("data-v-190f9876", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ }),

/***/ "./resources/js/views/Front/innerlayout.vue":
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
var normalizeComponent = __webpack_require__("./node_modules/vue-loader/lib/component-normalizer.js")
/* script */
var __vue_script__ = __webpack_require__("./node_modules/babel-loader/lib/index.js?{\"cacheDirectory\":true,\"presets\":[[\"env\",{\"modules\":false,\"targets\":{\"browsers\":[\"> 2%\"],\"uglify\":true}}]],\"plugins\":[\"transform-object-rest-spread\",[\"transform-runtime\",{\"polyfill\":false,\"helpers\":false}]]}!./node_modules/vue-loader/lib/selector.js?type=script&index=0!./resources/js/views/Front/innerlayout.vue")
/* template */
var __vue_template__ = __webpack_require__("./node_modules/vue-loader/lib/template-compiler/index.js?{\"id\":\"data-v-50bcfbea\",\"hasScoped\":false,\"buble\":{\"transforms\":{}}}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./resources/js/views/Front/innerlayout.vue")
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = null
/* scopeId */
var __vue_scopeId__ = null
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/js/views/Front/innerlayout.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-50bcfbea", Component.options)
  } else {
    hotAPI.reload("data-v-50bcfbea", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ })

});