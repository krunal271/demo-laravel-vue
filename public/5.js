webpackJsonp([5],{

/***/ "./node_modules/babel-loader/lib/index.js?{\"cacheDirectory\":true,\"presets\":[[\"env\",{\"modules\":false,\"targets\":{\"browsers\":[\"> 2%\"],\"uglify\":true}}]],\"plugins\":[\"transform-object-rest-spread\",[\"transform-runtime\",{\"polyfill\":false,\"helpers\":false}]]}!./node_modules/vue-loader/lib/selector.js?type=script&index=0!./resources/js/views/Admin/pages/forgot_password.vue":
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
Object.defineProperty(__webpack_exports__, "__esModule", { value: true });
/* harmony import */ var __WEBPACK_IMPORTED_MODULE_0__api_apiService_js__ = __webpack_require__("./resources/js/api/apiService.js");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//

// import magnify from "../../vendors/bootstrap-magnify/js/bootstrap-magnify.min.js"


/* harmony default export */ __webpack_exports__["default"] = ({
    name: "admin_forgot_password",
    data: function data() {
        return {
            loginData: {
                email: '',
                password: '',
                remember: '',
                forgotpassword: 0
            }
        };
    },

    mounted: function mounted() {
        "use strict";
    },
    destroyed: function destroyed() {},
    methods: {
        forgot_password: function forgot_password() {
            var _this = this;

            var vm = this;
            this.$validator.validateAll().then(function (response) {
                // if (vm.loginData.email != "") {
                if (!_this.errors.any()) {
                    _this.$store.commit("routeChange", "start");

                    __WEBPACK_IMPORTED_MODULE_0__api_apiService_js__["a" /* default */].sendResetLinkInMail(vm.loginData.email).then(function (response) {

                        _this.$store.commit("routeChange", "end");

                        $('#responceMessage').text('');
                        var resetLink = '';
                        if (response.data.status == 200) {
                            var resetLink = 'password/reset/' + response.data.token + '?email=' + response.data.email;
                            toastr.success('Reset pasword link has been sent successfully', 'Forgot password', { timeOut: 5000 });
                            $('#responceMessage').text(resetLink);
                            $(".enter_email").addClass("hidden");
                            $(".check_email").removeClass("hidden");
                            $('#email, .signup-signin').addClass('hidden');
                            $('.submit-btn').addClass('animated fadeInUp');
                            $('.submit-btn').html("Reset Password").removeClass("btn-primary btn-block").addClass("btn-success");
                            vm.$router.push({ 'name': 'adminLogin' });
                        } else if (response.data.status == 404) {
                            var resetLink = 'Record not found';
                            toastr.error('Record not found', 'Error', { timeOut: 5000 });
                            $('#responceMessage').text(resetLink);
                            // vm.$router.push({'name':'adminLogin'});
                        } else {
                            var resetLink = '';
                            toastr.error('Something goes wrong', 'Error', { timeOut: 5000 });
                            $('#responceMessage').text(resetLink);
                            // vm.$router.push({'name':'adminLogin'});
                        }
                    });
                }
            });
            // } else {
            //     var error_msg = "<p>Sorry, Enter Your Registered email</p>";
            //     $(".enter_email").addClass("err-text animated fadeInUp").html(error_msg);

            // }
        }
    }
});

/***/ }),

/***/ "./node_modules/vue-loader/lib/template-compiler/index.js?{\"id\":\"data-v-44d92cdd\",\"hasScoped\":false,\"buble\":{\"transforms\":{}}}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./resources/js/views/Admin/pages/forgot_password.vue":
/***/ (function(module, exports, __webpack_require__) {

var render = function() {
  var _vm = this
  var _h = _vm.$createElement
  var _c = _vm._self._c || _h
  return _c("div", { staticClass: "forgot" }, [
    _c("div", { staticClass: "container" }, [
      _vm._m(0),
      _vm._v(" "),
      _c("div", { staticClass: "row mt-5" }, [
        _c(
          "div",
          {
            staticClass:
              "col-lg-6 col-10 col-sm-8 m-auto login-form box animated fadeInUp"
          },
          [
            _c("h2", { staticClass: "text-center" }, [
              _vm._v("Forgot Password")
            ]),
            _vm._v(" "),
            _c("div", { staticClass: "form-group" }, [
              _c("label", { attrs: { for: "" } }, [_vm._v("Email *")]),
              _vm._v(" "),
              _c("input", {
                directives: [
                  {
                    name: "model",
                    rawName: "v-model",
                    value: _vm.loginData.email,
                    expression: "loginData.email"
                  },
                  {
                    name: "validate",
                    rawName: "v-validate",
                    value: "required|email",
                    expression: "'required|email'"
                  }
                ],
                staticClass: "form-control email pl-3",
                attrs: {
                  type: "email",
                  name: "email",
                  id: "email",
                  placeholder: "Email"
                },
                domProps: { value: _vm.loginData.email },
                on: {
                  keyup: function($event) {
                    if (
                      !$event.type.indexOf("key") &&
                      _vm._k($event.keyCode, "enter", 13, $event.key, "Enter")
                    ) {
                      return null
                    }
                    return _vm.forgot_password($event)
                  },
                  input: function($event) {
                    if ($event.target.composing) {
                      return
                    }
                    _vm.$set(_vm.loginData, "email", $event.target.value)
                  }
                }
              }),
              _vm._v(" "),
              _c(
                "span",
                {
                  directives: [
                    {
                      name: "show",
                      rawName: "v-show",
                      value: _vm.errors.has("email"),
                      expression: "errors.has('email')"
                    }
                  ],
                  staticClass: "help is-danger"
                },
                [_vm._v(_vm._s(_vm.errors.first("email")))]
              )
            ]),
            _vm._v(" "),
            _vm._m(1),
            _vm._v(" "),
            _c("div", { staticClass: "row mt-4 align-items-center" }, [
              _c("div", { staticClass: "col-md-6" }, [
                _c(
                  "button",
                  {
                    staticClass: "btn btn-primary",
                    attrs: { type: "button", value: "Reset Your Password" },
                    on: {
                      click: function($event) {
                        return _vm.forgot_password()
                      }
                    }
                  },
                  [
                    _vm._v(
                      "\n                                Retrieve Password\n                            "
                    )
                  ]
                )
              ]),
              _vm._v(" "),
              _vm._m(2)
            ])
          ]
        )
      ])
    ])
  ])
}
var staticRenderFns = [
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "text-center " }, [
      _c("img", {
        staticClass: "hidden-sm-down",
        attrs: {
          src: __webpack_require__("./resources/assets/images/logo.png"),
          id: "logo-desk",
          alt: "Deep"
        }
      })
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", [
      _c("p", { staticClass: "check_email hidden" }, [
        _vm._v(
          "\n                            Check your email for Reset link\n                        "
        )
      ])
    ])
  },
  function() {
    var _vm = this
    var _h = _vm.$createElement
    var _c = _vm._self._c || _h
    return _c("div", { staticClass: "col-md-6 text-right" }, [
      _c(
        "a",
        { staticClass: "reset-link", attrs: { href: "javascript:void(0)" } },
        [_vm._v("Resend the link")]
      )
    ])
  }
]
render._withStripped = true
module.exports = { render: render, staticRenderFns: staticRenderFns }
if (false) {
  module.hot.accept()
  if (module.hot.data) {
    require("vue-hot-reload-api")      .rerender("data-v-44d92cdd", module.exports)
  }
}

/***/ }),

/***/ "./resources/js/views/Admin/pages/forgot_password.vue":
/***/ (function(module, exports, __webpack_require__) {

var disposed = false
var normalizeComponent = __webpack_require__("./node_modules/vue-loader/lib/component-normalizer.js")
/* script */
var __vue_script__ = __webpack_require__("./node_modules/babel-loader/lib/index.js?{\"cacheDirectory\":true,\"presets\":[[\"env\",{\"modules\":false,\"targets\":{\"browsers\":[\"> 2%\"],\"uglify\":true}}]],\"plugins\":[\"transform-object-rest-spread\",[\"transform-runtime\",{\"polyfill\":false,\"helpers\":false}]]}!./node_modules/vue-loader/lib/selector.js?type=script&index=0!./resources/js/views/Admin/pages/forgot_password.vue")
/* template */
var __vue_template__ = __webpack_require__("./node_modules/vue-loader/lib/template-compiler/index.js?{\"id\":\"data-v-44d92cdd\",\"hasScoped\":false,\"buble\":{\"transforms\":{}}}!./node_modules/vue-loader/lib/selector.js?type=template&index=0!./resources/js/views/Admin/pages/forgot_password.vue")
/* template functional */
var __vue_template_functional__ = false
/* styles */
var __vue_styles__ = null
/* scopeId */
var __vue_scopeId__ = null
/* moduleIdentifier (server only) */
var __vue_module_identifier__ = null
var Component = normalizeComponent(
  __vue_script__,
  __vue_template__,
  __vue_template_functional__,
  __vue_styles__,
  __vue_scopeId__,
  __vue_module_identifier__
)
Component.options.__file = "resources/js/views/Admin/pages/forgot_password.vue"

/* hot reload */
if (false) {(function () {
  var hotAPI = require("vue-hot-reload-api")
  hotAPI.install(require("vue"), false)
  if (!hotAPI.compatible) return
  module.hot.accept()
  if (!module.hot.data) {
    hotAPI.createRecord("data-v-44d92cdd", Component.options)
  } else {
    hotAPI.reload("data-v-44d92cdd", Component.options)
  }
  module.hot.dispose(function (data) {
    disposed = true
  })
})()}

module.exports = Component.exports


/***/ })

});